# Códigos de registro

Es posible acceder a IsardVDI a través de una cuenta de Google, para poder acceder es necesario un código de registro, este se puede obtener accediendo a la interfaz avanzada.

Una vez se accede a la interfaz avanzada, en el menú de la izquierda ir a la opción "Users"![](./create_enrollment_codes.es.images/google12.png)

A continuación desplazarse hasta la opción de "Groups".

![](./create_enrollment_codes.es.images/google5.png)

Pulsar el icono ![](./create_enrollment_codes.es.images/google13.png) sobre el grupo en el que tenemos a los usuarios.

Pulsar sobre el icono ![](./create_enrollment_codes.es.images/google7.png) y se abrirá una nueva ventana donde se puede escoger sobre qué roles se crea un código de registro.

![](./create_enrollment_codes.es.images/google8.png)

Una vez obtenidos, se puede proporcionar el código al usuario para que acceda mediante Google, y solo deberá utilizar el código la primera vez que se [autentique con Google](../user/google_account.es).

