# Guia instalación Windows 10

La intención de esta guía es mostar paso a paso como crear una imagen base de windows 10 para crear una plantilla adaptada al entorno de virtualización de escritorios de IsardVDI. La idea de este documento es aportar y recoger ideas sobre que elementos del windows modificar y personalizar para un mejor rendimiento.

## Pre-instalación windows 10

Esta primera parte explica donde hemos de ir a buscar las imágenes iso, los drivers de virtio y como configurar el hardware del escritorio virtual.

### ISO de windows

En función del tipo de licencia y del idioma podemos descargar una iso genérica de windows 10. En nuestro  caso queremos licenciar el windows con una licencia Pro. Las versiones de las iso de windows van evolucionando, y desde la página web de microsoft esta disponible solo la última. También existe la posibilidad de personalizar tu propia iso de windows o crear versiones LTSC (en este caso la licencia es enterprise) con herramientas de microsoft.

Vamos a la [página web de microsfot](https://www.microsoft.com/es-es/software-download/windows10ISO) para descargar la iso y seleccionamos la edición y el idioma

![](./win10_install_guide_img.es/select_iso_win10.png)



Llegamos a una página que nos permite descargar la versión de 64 bits. En cuanto empiece a bajar la iso cancelaremos la descarga, pues solo nos interesa la URL de la descarga para podérsela pasar a isard para que la descargue dentro de los directorios de isard donde se almacenan las iso. Y copiamos el enlace de descarga, este enlace es válido solo temporalmente.

![](./win10_install_guide_img.es/copy_download_link.png)

Ahora vamos a Isard a menú de administración "Media" y hacemos clie cn add new

![](./win10_install_guide_img.es/screenshot_1608082166.png)

Aparece un cuadro de diálogo donde podemos copiar la url de descarga que previamente habíamos copiado, darle un nombre y decirle que es de tipo ISO. También aprovechamos para dar permisos para que esta iso la puedan usar los usuarios administradores desplegando la zona de "Allows":

![](./win10_install_guide_img.es/screenshot_1608082438.png)

La imagen iso se queda en Status "Downloading" y podemos ir viendo el progreso de la descarga:

![](./win10_install_guide_img.es/screenshot_1608082485.png)

### Windows VirtIO Drivers

Mientras se descarga podemos ir a buscar la iso con los drivers virtio que necesitaremos. Los drivers virtio permite usar dispositivos paravirtualizados o simulados que forman parte del hardware optimizado para mejorar el rendimiento de las máquinas virtuales de KVM. Destacan los drivers virtio de disco, más eficientes que los simuladores de SATA o IDE, así como los drivers de red virtio, más eficiente que las simulaciones de otras tarjetas. 

La comunidad de Fedora, con su proyecto oVirt como referencia, preparan y mantienen una imagen ISO con un par de instaladores de windows que facilitan la instalación de mucho software necesario. Sería algo parecido a los discos de drivers de las placas base, que vienen con un instalador para facilitarte la instalación manula de todos los drivers disponibles. Tenemos dos versiones disponibles

* Stable: https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/virtio-win.iso
* Latest: https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso

Recomendamos usar la versión latest y en caso de dar algún problema, ir a la versión stable. 

Pasamos a descargar también esta imagen iso en isard, siguiendo los mismos passo que se han realizado anteriormente para la iso de windows.

![](./win10_install_guide_img.es/screenshot_1608083261.png)

## Crear escritorio para instalar desde iso

Vamos al listado de "Media" en isard y hacemos clic en el botón verde ![](./win10_install_guide_img.es/screenshot_1608083394.png) desde la iso de windows 10:

![](./win10_install_guide_img.es/screenshot_1608083421.png)



Seleccionamos como plantilla de sistema operativo (la que decide que hardware es el aconsejable para esta distribucion) "windows 10 UEFI and Virtio devices". Damos un nombre al desktop, y definimos sus capacidades: 2vcpus, 4GB y disco de 120GB por ejemplo... 

![](./win10_install_guide_img.es/screenshot_1608083706.png)

Una vez le hemos dado a Create desktop, vamos al menú Desktops a ver si ya se ha creado. Observamos que solo tiene un icono de media ![](./win10_install_guide_img.es/screenshot_1608083783.png)

Desplegamos con el botón ![](./win10_install_guide_img.es/screenshot_1608083814.png)para ver más opciones, y hacemos clic en el botón de edita: ![](./win10_install_guide_img.es/screenshot_1608083869.png) 

Apaerece un cuadro de diálogo donde podemos desplegar Media con la flechita ![](./win10_install_guide_img.es/screenshot_1608083922.png)

En la parte de CD/DVD iso empezamos a escribir "virt" y seleccionamos la iso de virtio-win

![](./win10_install_guide_img.es/select_iso_drivers.png)

Quedan las dos iso seleccionadas en Media 

![](./win10_install_guide_img.es/screenshot_1608084090.png)

Vemos que la opción de boot está en cd/dvd

Ahora cambiamos el tipo de red de Default a **Intel PRO/1000 isolated**. Este tipo de tarjeta de red Intel Pro, conectada a una red isolated (puede salir a internet pero no puede ver a otros escritorios) permitirá que el instalador de windows reconozca esa tarjeta y pueda salir a internet durante la instalación. En caso de que dejemos la red default la instalación se realiza igualmente pero no realiza ciertas actualizaciones ni ofrece ciertas opciones durante el proceso de instalación. Recomendamos cambiar a este tipo de hardware y volver a modificar posteriormente a la red **"Default"** una vez haya finalizado la instlaación, ya que con la red default se utiliza un hardware "**virtio**" que es más eficiente y da más rendimiento que la simulación de tarjeta Intel Pro 1000. 

![](./win10_install_guide_img.es/screenshot_1608792451.png)

Ahora clic en ![](./win10_install_guide_img.es/screenshot_1608792482.png)y si todo ha ido bien aparece el icono de Start y dos iconos de media en el listado:

![](./win10_install_guide_img.es/screenshot_1608084156.png)

Al final internamente el escritorio virtual corre en los hypervisores a partir de un xml que describe esa máquina virtual. Desde la parte de administración podemos acceder a este fichero xml y verificar que ciertas opciones están seleccionadas. Pimero hay que ir a visualizar los dominios como ADMINISTRATOR, seleccionamos Domains y Desktops. 

![](./win10_install_guide_img.es/screenshot_1608084314.png)

Desplegamos las opciones del desktop y aparece el icono para editar el XML. Recomendamos solo visualizar y tener un conocimiento claro de lo que se quiere hacer si tocamos este xml. Importantes las siguientes secciones:

* Tipo de máquina **q35** , es un tipo de simulación de hardware moderna y con mejor rendimiento para el windows 10

```xml
    <type arch="x86_64" machine="q35">hvm</type> 
```

* opciones especiales para simular y gestionar interrupciones y interacción simulando al hypervisor de Microsoft HyperV, mejorando el rendimiento general de la máquinas de windows. Con el tipo de máquina q35 son necesarias estas opciones

```xml
      <hyperv>
        <relaxed state="on"/>
        <vapic state="on"/>
        <spinlocks state="on" retries="8191"/>
      </hyperv>
```
  
* Arranque UEFI. Necesario para una correcta instalación de windows 10 y detección de los drivers virtio de disco durante el inicio de la instalación. Se usa para simular UEFI el loader de UEFI, y hay que indicarle en que path tiene ese código de arranque del UEFI. La línea del xml que activa el UEFI es:

```xml
        <loader readonly="yes" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
```

    Integrado en la zona de definición de os en el xml:

    ```xml
      <os>
        <type arch="x86_64" machine="q35">hvm</type>
        <boot dev="cdrom"/>
        <loader readonly="yes" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
      </os>
    ```



La primera vez que arrancamos con uefi, como no hay nada instalado y el menú de arranque por cd casi no tiene timeout, no hay sistema operativo y se queda en este punto:

![](./win10_install_guide_img.es/screenshot_1608085306.png)

Forzamos el reinicio enviando la combianación ctrl+alt+supr Esta parte de enviar esta combinación lo podemos hacer dentro del visor remote -viewer en el menú "Send key":

![](./win10_install_guide_img.es/reboot_with_send_key.png)

Y si somos rápidos podemos ver el mensaje que nos pide arrancar por cd. Al hacer clic dentro del remote-viewer, pulsar una tecla

![](./win10_install_guide_img.es/screenshot_1608085502.png)

Aparece el icono en pantalla del tiano core:

![](./win10_install_guide_img.es/screenshot_1608085705.png)

Y enseguida la primera pantalla de instlaación del windows:

![](./win10_install_guide_img.es/screenshot_1608085767.png)

## Instalación de windows:

Le damos a siguiente de esta primera pantaa de sección de idiomas:

![](./win10_install_guide_img.es/screenshot_1608085877.png)

Instalar ahora:

![](./win10_install_guide_img.es/screenshot_1608085905.png)

En el siguiente diálogo le decimos que **"No tengo clave de producto"**

![](./win10_install_guide_img.es/screenshot_1608085941.png)

Seleccionamos el tipo de windows que queremos instalar, en nuestro caso el **Windows 10 Pro**

![](./win10_install_guide_img.es/screenshot_1608086038.png)

Acepatmos la licencia:

![](./win10_install_guide_img.es/screenshot_1608086068.png)

En el tipo de instalación seleccionamos personalizada:

![](./win10_install_guide_img.es/screenshot_1608086157.png)

Ahora hay que cargar el controlado de virtio en el botón **"Cargar contr."**:

![](./win10_install_guide_img.es/screenshot_1608086519.png)

Le damos a examinar y seleccionamos el cdrom de virtio, directorio viostor, directorio w10 y amd64:

![](./win10_install_guide_img.es/screenshot_1608086347.png)

Encuentra el controlador:

![](./win10_install_guide_img.es/screenshot_1608086404.png)

Y detecta el disco para que podamos instalar:

![](./win10_install_guide_img.es/screenshot_1608086459.png)

Le damos a Siguiente y inicia la instalación del sistema operativo:

![](./win10_install_guide_img.es/screenshot_1608086569.png)

Una vez que acaba se reinicia el escritorio virtual:

![](./win10_install_guide_img.es/screenshot_1608086729.png)

Intenta arrancar por cdrom y luego UEFI da un error:

![](./win10_install_guide_img.es/screenshot_1608086804.png)

Ahora desde la aplicación web de Isard paramos la máquina y cambiamos el dispositivo de arranque. Clic en stop:

![](./win10_install_guide_img.es/screenshot_1608086861.png)

Desplegar opciones y clic en botón de **Edit** 



![](./win10_install_guide_img.es/screenshot_1608086960.png)

Cambiamos el dispositivo de **boot** a **Hard Disk** y modificamos el desktop

![](./win10_install_guide_img.es/screenshot_1608087008.png)

Volvemos a arrancar el escritorio con botón de Start:

![](./win10_install_guide_img.es/screenshot_1608087090.png)

Me conecto al escritorio y arranca bien desde disco, aparecen mensajes por pantalla como este:

![](./win10_install_guide_img.es/screenshot_1608087135.png)

Acaba con un nuevo reinicio y aparece un conjunto de diálogos donde seleccionamos habitualmente NO a lo que nos propone:

![](./win10_install_guide_img.es/screenshot_1608087281.png)

![](./win10_install_guide_img.es/screenshot_1608087314.png)

Si habíamos seleccionado la red con modelo de tarjeta Intel aparecerá una pantalla como esta que busca actualizaciones por internet:

![](./win10_install_guide_img.es/screenshot_1608790597.png)

Si no seleccionamos la tarjeta intel, como no tiene los drivers de red no puede acceder a internet,  le decimos que no tenemos internet cuando nos proponga conectarnos a una red:

![](./win10_install_guide_img.es/screenshot_1608087345.png)

![](./win10_install_guide_img.es/screenshot_1608087390.png)

y en el siguinete diálogo le decimos: **Continuar con la configuración limitada**

A continuación si teníamos red nos permite realizar la configuración para uso personal o para una organización. En nuestro caso preferimos configurar para uso personal:

![](./win10_install_guide_img.es/screenshot_1608790908.png)

Y ahora para agregar una cuenta seleccionamos **Cuenta sin conexión**

![](./win10_install_guide_img.es/screenshot_1608790994.png)

Y posteriormente seleccionamos **Experiencia limitada**:

![](./win10_install_guide_img.es/screenshot_1608791071.png)



Al ser una imagen genérica hay que poner un usuario y password que facilmente te aucerdes. En Isard es habitual que las imágenes base vengan con el siguiente usuario y password que configuraremos

* usuario: isard
* passowrd: pirineus

![](./win10_install_guide_img.es/screenshot_1608087603.png)

![](./win10_install_guide_img.es/screenshot_1608087632.png)

Rellenamos las preguntas de seguridad con respuestas aleatorias:

![](./win10_install_guide_img.es/screenshot_1608087697.png)

Ahora optamos por decirle que no a lo que nos va proponiendo:

![](./win10_install_guide_img.es/screenshot_1608087762.png)

![](./win10_install_guide_img.es/screenshot_1608087797.png)

![](./win10_install_guide_img.es/screenshot_1608087821.png)

![](./win10_install_guide_img.es/screenshot_1608087849.png)

![](./win10_install_guide_img.es/screenshot_1608087867.png)

![](./win10_install_guide_img.es/screenshot_1608087891.png)

![](./win10_install_guide_img.es/screenshot_1608087907.png)

No queremos el asistente Cortana:

![](./win10_install_guide_img.es/screenshot_1608087931.png)

![](./win10_install_guide_img.es/screenshot_1608087945.png)

Finalmente nos empieza a dar mensajes de bienvenida:

![](./win10_install_guide_img.es/screenshot_1608087980.png)

Y aparece el escritorio de windows por primera vez:

![](./win10_install_guide_img.es/screenshot_1608088057.png)

Ahora ya tenemos el windows 10 con un escritorio de trabajo. El siguiente paso es instalar los drivers. Para eso es imporante cambiar el tipo de tarjeta de red a virtio, ya que ese hardware está optimizado y es el recomendado al trabajar con una máquina virtual que corre sobre KVM como es nuestro caso. Por lo tanto apagamos, modificamos el hardware y volvemos a arrancar el escritorio. 

![](./win10_install_guide_img.es/screenshot_1608791967.png)

Ahora editamos el desktop desplegando las opciones del escritorio y haciendo clic en **Edit** y selecionamos la red a **Default** para que pille los drivers virtio de red:

![](./win10_install_guide_img.es/screenshot_1608709662.png)



![](./win10_install_guide_img.es/screenshot_1608792047.png)

Volvemos a iniciar el escritorio y nos conectamos por spice con remote-viewer:

Abrimos explorador de ficheros, seleccionamos la unidad E: y arrancamos primero el ejecutable virtio-win-gt-x64 y luego virtio-guest-tools:

![](./win10_install_guide_img.es/screenshot_1608088165.png)

Instalador de drivers para windows Virtio-win:

![](./win10_install_guide_img.es/screenshot_1608088231.png)

No tocamos nada y siguiente siguiente siguiente:

![](./win10_install_guide_img.es/screenshot_1608088292.png)

Le decimos que si queremos permitir que instale:

![](./win10_install_guide_img.es/screenshot_1608088315.png)

Confiamos en que el editor es Red Hat y Instalar:

![](./win10_install_guide_img.es/screenshot_1608088355.png)

Ahora las win-guest-tools que permitirán entre otras cosas, el reescalamiento automático de la resolución de pantalla.



![](./win10_install_guide_img.es/screenshot_1608088430.png)

Al instalar el agente de spice ya para a reescalar automáticamente. 

Hacemos un apagado manual y quitamos los cds del isard con el escritorio apagado:

![](./win10_install_guide_img.es/screenshot_1608088766.png)

Eliminamos haciendo clic en la x las dos isos:

![](./win10_install_guide_img.es/screenshot_1608088806.png)

Quedando en el listado del desktop sin iconos en la columna "Media":

![](./win10_install_guide_img.es/screenshot_1608088895.png)

Volvemos a arrancar el escritorio y a conectarnos con el visor remote-viewer. 

A continucación instalamos actualizaciones, escribiendo **"windows update"** en el buscador de la barra de tareas:

![](./win10_install_guide_img.es/screenshot_1608088558.png)

Haciendo clic en el texto **Buscar actualizaciones**:

![](./win10_install_guide_img.es/screenshot_1608088622.png)

Encuentra algunas actualizaciones y se dipone a instalarlas

![](./win10_install_guide_img.es/screenshot_1608089460.png)

Realizamos un reboot al escritorio desde dentro del propio windows, tarda en reiniciar al aplicar las actualizaciones:

![](./win10_install_guide_img.es/screenshot_1608795160.png)  ![](./win10_install_guide_img.es/screenshot_1608795282.png)



### Crear plantilla de windows actualizado y con drivers virtio instalados.

Una vez tenemos el sistema actualizado y hemos realizado un reinicio para verificar que las actualizaciones han quedado bien instaladas, recomendamos hacer una plantilla para guardar todo el trabajo hecho y poder regresar a este punto de la instalación si lo necesitamos en un futuro. Tener una plantilla con lo realizado hasta el momento nos puede ahorrar mucho tiempo para confeccionar una nueva plantilla con optimizaciones y software en un futuro.

Las plantillas sólo se pueden hacer con el escritorio apagado. Por lo que lo primero es apagar de forma ordenada el windows:

![](./win10_install_guide_img.es/screenshot_1608088766.png)

Ahora desde desktops, con las opciones ampliadas desplegadas en nuestro escritorio:

Una vez haya reiniciado, apagamos de forma ordenada y creamos la plantilla, En la fila del desktop con la máquina apagada le damos al botón "**TEmpleate it**":

![](./win10_install_guide_img.es/screenshot_1608090182.png)

Y creamos la plantilla

![](./win10_install_guide_img.es/screenshot_1608090145.png)

A partir de ahora aparecerá la template en el listdo de templates, y nuestro escritorio derivará de esta template que acabamos de crear. Podemos continuar trabajando con el escritorio en el punto donde lo dejamos y también crear nuevos desktops a partir de esa template.



## Software y optimizaciones de windows

### Modificar el idioma y replicar a todos los usuarios

En caso de que queramos modificar el idioma de windows (por ejemplo a català) es importante realizar el cambio ahora ya que cierto software buscará el idioma del usuario para instlaar la versioń correcta de idioma. 

Ir a **configuración => hora e idioma => idioma**

Ahora Agregar un idioma:

![](./win10_install_guide_img.es/screenshot_1608518330.png)

![](./win10_install_guide_img.es/screenshot_1608518357.png)

Ahora hay que seleccionar **"Establecer como idioma para mostrar de windows"**:

![](./win10_install_guide_img.es/screenshot_1608797950.png)

Y clic en instalar:
![](./win10_install_guide_img.es/screenshot_1608797980.png)

Esperamos a que se descargue, tarda unos minutos en aparecer la barra de descarga:

![](./win10_install_guide_img.es/screenshot_1608798074.png)

Nos pide si queremos cerrar sesión ahora, aceptamos, cerramos sesión y volvemos a iniciar sesión, aparece el catalán en la barra de tareas de windows:

![](./win10_install_guide_img.es/screenshot_1608798225.png)

#### Configurar idioma por defecto para todos los usuarios

Vamos a **Inicio -> configuración -> Hora e idioma -> Idioma**

Ahora podemos quitar el idioma Español si lo deseamos o bien dejar los dos idiomas en el usuario isard. 

Debajo de idiomas preferidos aparece la opción de **Configuración de idioma administrativo**

![](./win10_install_guide_img.es/screenshot_1608798327.png)

Se abre un cuadro de diálogo:
![](./win10_install_guide_img.es/screenshot_1608798491.png)

Hacemos clic en cambiar los parámetros regionales del sistema y cambiamos a català

Y ahora la parte más importante para que los nuevos usuarios también tengan el nuevo idioma por defecto. Clic en **Copia la configuración** y seleccionar **Pantalla de bienvenida y cuentas del sistema** y **Cuentas de usuarios nuevos**. Queda una configuración como esta:

![](./win10_install_guide_img.es/screenshot_1608798681.png)

Aplicamos los cambios y nos pide reiniciar:
![](./win10_install_guide_img.es/screenshot_1608798782.png)

Aun pueden quedar restos del idioma original (español en este caso) en el usuario actual, habríamos de borrar la cuenta de usuario y volverla a crear para que al rehacer de nuevo la cuenta coja bien todas las opciones de idioma. Por eso es imporatnte hacer el cambio de idioma antes de crear los usuarios admin y user que son los que daremos por defecto en la plantilla base de windows.





## Adaptar windows para entorno virtualizado

En un entorno virtualizado nos interesa que los recursos que usa el windows sean mínimos a nivel de escritura en disco, uso de cpu y ram. Cuanto menor impacto en estos factores tengamos, más escritorios simultáneos podremos correr en un servidor. Una disminución de procesos que habitualmente el usuario no usa en un entorno educativo o empresarial también favorecerá un inicio del sistema más rápido y mayor agilidad. El reto está en buscar el equilibrio entre restringir funcionalidades y no penalizar la experiencia de usuario. 

En esta guía se proponen diferentes herramientas y técnicas que permitirán optimizar las instalaciones y mejorar la experiencia de usuario.

### Instalar firefox

Descargar el firefox e instalarlo desde la web de mozilla:

 https://www.mozilla.org/es-ES/firefox/new/

Inicia por primera vez el firefox. Lo apagamos, y lo volvemos a arrancar. Nos pregunta:

![](./win10_install_guide_img.es/screenshot_1608089809.png)

Le decimos que sí queremos y cambiamos el navegador predeterminado:

![](./win10_install_guide_img.es/screenshot_1608089876.png)

Y queda predeterminado:

![](./win10_install_guide_img.es/screenshot_1608089901.png)

### Desinstalar Microsoft Edge

Microsoft Edge está basado en Chromium project, podemos dejarlo en el sistema, pero si deseamos quitarlo del sistema operativo podemos seguir los siguientes pasos. Abres una consola: Presionar tecla **Windows + R** y escribir **cmd**

En la consola:

```bat
cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\8*\Installer
setup --uninstall --force-uninstall --system-level
```

### Autologon

Para facilitar los reinicios y de momento como trabajaremos con un solo usuario administrador para realizar instalaciones y preparar la plantilla proponemos instalar una utilidad para realizar un autologon y que no nos pida usuario y password cuando inicia windows. Hemos de buscar "autologon windows sysinternals" y llegar a la web de sysinternals desde donde descargar la utilidad. En el momento de redactar este manual el enlace donde encontrar la utilidad es: https://docs.microsoft.com/en-us/sysinternals/downloads/autologon 

![](./win10_install_guide_img.es/screenshot_1608453332.png)

A continuación descargamos el fichero zip que nos proporcionan, lo abrimos y descomprimimos **Autologon64**:

![](./win10_install_guide_img.es/screenshot_1608453580.png)

![](./win10_install_guide_img.es/screenshot_1608453731.png)

Ejecutamos Autologon64, introducimos el password de la cuenta isard: **pirineus**

![1608453832735](./win10_install_guide_img.es/1608453832735.png)

Nos informa que el password queda encriptado:

![](./win10_install_guide_img.es/screenshot_1608453853.png)

Reiniciamos para verificar que ya no nos pide usuario y password al entrar:

![](./win10_install_guide_img.es/screenshot_1608453894.png)

### Crear usuario admin

Una cuenta admin nos permitirá poder trabajar como administradores en caso de que queramos convertir la cuenta del usuario "isard" a usuario normal.

Desde powershell como administrador (botón derecho encima de icono de windows):

![](./win10_install_guide_img.es/screenshot_1608804812.png)

```
# Guardar el password en una variable
$Password = Read-Host -AsSecureString
```
Y una vez hemos introducido el password: ***pirineus***
```powershell
# Crear usuario admin del grupo administradores
New-LocalUser "admin" -Password $Password -FullName "admin"
Add-LocalGroupMember -Group "Administradores" -Member "admin"
```

Y el usuario "user" que no será administrador:

```
New-LocalUser "user" -Password $Password -FullName "user"
Add-LocalGroupMember -Group "Usuarios" -Member "user"
```



### Directorio c:\admin

También creamos el directorio c:\admin donde guardaremos los programas o información de administración. Quitamos los permisos de uusarios, para que solo puedan acceder administradores:

![](./win10_install_guide_img.es/screenshot_1608490011.png)

Y dejamos solo el permiso para el grupo administradores:

![](./win10_install_guide_img.es/screenshot_1608490064.png)



### Desinstalar Microsoft Edge

Microsoft Edge está basado en Chromium project, podemos dejarlo en el sistema, pero si deseamos quitarlo del sistema operativo podemos seguir los siguientes pasos. Abres una consola: Presionar tecla **Windows + R** y escribir **cmd**

En la consola:

```bat
cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\8*\Installer
setup --uninstall --force-uninstall --system-level
```

### Desinstalar otras aplicaciones de Microsoft

Abrimos una consola de powershell como administrador:

- botón derecho encima del icono de windows en la barra de inicio y seleccionamos **Windows PowerShell (Administrador)**

![](./win10_install_guide_img.es/screenshot_1608466862.png)

Para mirar todos los programas instalados:

```
Get-AppxPackage | Select Name
```

Y ahora borramos todos estos:

```
Microsoft.Xbox*
Microsoft.Bing*
Microsoft.3DBuilder
Microsoft.Advertising.Xaml
Microsoft.AsyncTextService
Microsoft.BingWeather
Microsoft.BioEnrollment
Microsoft.DesktopAppInstaller
Microsoft.GetHelp
Microsoft.Getstarted
Microsoft.Microsoft3DViewer
Microsoft.MicrosoftEdge
Microsoft.MicrosoftEdgeDevToolsClient
Microsoft.MicrosoftOfficeHub
Microsoft.MicrosoftSolitaireCollection
Microsoft.MicrosoftStickyNotes
Microsoft.MixedReality.Portal
Microsoft.Office.OneNote
Microsoft.People
Microsoft.ScreenSketch
Microsoft.Services.Store.Engagement
Microsoft.Services.Store.Engagement
Microsoft.SkypeApp
Microsoft.StorePurchaseApp
Microsoft.Wallet
Microsoft.WindowsAlarms
Microsoft.WindowsCamera
Microsoft.WindowsFeedbackHub
Microsoft.WindowsMaps
Microsoft.WindowsSoundRecorder
Microsoft.WindowsStore
Microsoft.XboxGameCallableUI
Microsoft.YourPhone
Microsoft.ZuneMusic
Microsoft.ZuneVideo
SpotifyAB.SpotifyMusic
Windows.CBSPreview
microsoft.windowscommunicationsapps
windows.immersivecontrolpanel
```

Ahora copiar y pegar en la terminal de powershell

```
Get-AppxPackage -Name Microsoft.Xbox* | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Bing* | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.3DBuilder | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Advertising.Xaml | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.AsyncTextService | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.BingWeather | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.BioEnrollment | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.DesktopAppInstaller | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.GetHelp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Getstarted | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Microsoft3DViewer | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftEdge | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftEdgeDevToolsClient | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftStickyNotes | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MixedReality.Portal | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Office.OneNote | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.People | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ScreenSketch | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.SkypeApp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.StorePurchaseApp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Wallet | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsAlarms | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsCamera | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsFeedbackHub | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsMaps | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsSoundRecorder | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.XboxGameCallableUI | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.YourPhone | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ZuneMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ZuneVideo | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name SpotifyAB.SpotifyMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Windows.CBSPreview | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name microsoft.windowscommunicationsapps | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name windows.immersivecontrolpanel | Remove-AppxPackage -ErrorAction SilentlyContinue
```

### Barra de programas simple

Podemos quitar todos los elementos de Productividad y Explorar de la barra de inicio.

Botón derecho encima de los elementos y **Desanclar de Inicio**

![](./win10_install_guide_img.es/screenshot_1608469914.png)



### Desactivar servicios

Ejecutar "**msconfig**" y desactivar servicios:

- Administrador de autenticación Xbox Live
- Centro de seguridad
- Firewall de Windows Defender
- Mozilla Maintenance Service
- Servicio de antivirus de Microsoft Defender
- Servicio de Windows Update Medic
- Windows Update

En Inicio de windows -> ir al administrador de tareas y deshabilitar:

- Microsoft OneDrive

- Windows Security notification

  ![](./win10_install_guide_img.es/screenshot_1608514871.png)
  
  Reiniciar para aplicar los cambios
  
  ![](./win10_install_guide_img.es/screenshot_1608805792.png)
  
  

### Repasar escritorios de admin y user

Iniciar sesión y repasar que los iconos de escritorio, iconos de barra de tareas y menú inicio están bien... Volver a quitar los programas de windows...

Quitar las notificaciones:

![](./win10_install_guide_img.es/screenshot_1608806703.png)





### Instalar software libre

Elegimos instalar el siguiente software:

- Libre Office

- Gimp

- Inkscape

- LibreCAD

- Geany

  ![](./win10_install_guide_img.es/screenshot_1608516793.png)

### Aplicaciones no libres

Instalamos las siguientes aplicaciones:

- google chrome
- Adobe Acrobat Reader

Desactivar Update Service:

![](./win10_install_guide_img.es/screenshot_1608517594.png)

Desactivar Google Update:

![](./win10_install_guide_img.es/screenshot_1608517628.png)

Una herramienta muy útil para preparar plantillas es **VMware OS Optimization Tool** 

La podemos descargar desde https://flings.vmware.com/vmware-os-optimization-tool

La dejamos en c:\admin\VMwareOSOptimizationTool_b2000_17205301.zip

Antes de pasar esta herramienta recomendamos hacer una plantilla. Si la herramienta de optimización realiza demasiadas modificaciones que interfieren con alguna de las aplicaciones que instalemos podemos volver a este punto y realizar la optimización posteriormente.

## Vmware OS optimization tool

Descomprimimos el fichero zip que tenemos almacenado en c:\admin y arrancamos la utilidad: VMwareOSOptimizationTool

![](./win10_install_guide_img.es/screenshot_1608455307.png)

Una vez arranca la aplicación le damos al botón **Analyze**:

![](./win10_install_guide_img.es/screenshot_1608490804.png)

Y después a common options, donde en Visual Effect seleccionaremos "Best quality" y atención al check de Disable hardware acceleration:

![](./win10_install_guide_img.es/screenshot_1608516016.png)



En Store Apps dejamos Calculator

![](./win10_install_guide_img.es/screenshot_1608515977.png)

Y le damos a Optimize, cuando finaliza guardamos el resultado con el botón "export" en c:\admin y luego reiniciar
