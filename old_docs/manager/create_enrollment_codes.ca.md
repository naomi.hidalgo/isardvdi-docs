# Codis de registre

És possible accedir a IsardVDI a través d'un compte de Google, per poder accedir cal un codi de registre, aquest el podem obtenir accedint a la interfície avançada.

Un cop accedit a la interfície avançada, al menú de l'esquerra anem a l'opció “Users”. ![](./create_enrollment_codes.es.images/google12.png)

A continuació, desplaçar-se fins a l'opció de "Groups".

![](./create_enrollment_codes.es.images/google5.png)

Premer la icona ![](./create_enrollment_codes.es.images/google13.png) sobre el grup d'usuaris.

Premer la icona ![](./create_enrollment_codes.es.images/google7.png) i s'obrirà una finestra nova on es pot triar per quins rols es crea un codi de registre.

![](./create_enrollment_codes.es.images/google8.png)

Un cop obtinguts, es pot proporcionar el codi a l'usuari perquè hi accedeixi mitjançant Google, i només haurà d'utilitzar el codi la primera vegada que [autentiqueu amb Google](../user/google_account.ca).