# Quick Start

Get the docker-compose file and bring it up:

```bash
wget https://isardvdi.com/docker-compose.yml
docker-compose pull
docker-compose up -d
```

Please wait a minute the first time as the database it will get populated before the IsardVDI login becomes available.

Browse to <https://localhost/isard-admin>. Default user is **admin** and default password is **IsardVDI**.

You can immediately download preinstalled and optimized Operating System images from *Downloads* menu and use them, create templates and much more.

Please, read the rest of the documentation to enjoy all features.
