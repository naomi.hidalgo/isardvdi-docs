# Despliegues

Los despliegues son escritorios creados para otros usuarios.

## Como crear despliegues

Para crear un despliegue, se debe ir a la barra superior

![](./deployments.es.images/deployment1.png)

Y pulsar en el menú desplegable ![](./deployments.es.images/deployment2.png)

Ir a la opción "Gestionar" ![](./deployments.es.images/deployment3.png)

Una vez pulsado, se redirigirá a esta página:

![](./deployments.es.images/deployment4.png)

Pulsar en el botón ![](./deployments.es.images/deployment12.png) para crear un nuevo despliegue.

Una vez se haya pulsado, aparecerá un pop-up para que se pueda asignar un nombre, seleccionar una plantilla y los usuarios al que crear estos escritorios:

![](./deployments.es.images/deployment5.png)

Y aquí se verán los despliegues que se hayan creado:

![](./deployments.es.images/deployment6.png)



## Como borrar despliegues

Para eliminar un despliegue, se debe ir a la opción de "Gestionar" en la barra superior. Una vez ahí:

![deployment16](./deployments.es.images/deployment16.png)

Se selecciona el despliegue que se quiera eliminar:

![deployment15](./deployments.es.images/deployment15.png)

Pulsar sobre el botón ![](./deployments.es.images/deployment14.png) para eliminar. 



## Como ver despliegues

En la ventana de despliegues:

![](./deployments.es.images/deployment6.png)

Se selecciona la opción de "Toggle visible to user (will stop desktops running)" para que los escritorios estén visibles y ejecutados.

![](./deployments.es.images/deployment7.png)

Se pulsa en el menú desplegable ![](./deployments.es.images/deployment3.png) y se selecciona la opción de "Ver".

Te redirigirá a esta página donde se puede encontrar todas los despliegues que se han creado.

![](./deployments.es.images/deployment8.png)

Cuando se selecciona uno de los despliegues, se verán los escritorios que hayan sido creados para los usuarios.

![](./deployments.es.images/deployment9.png)

Para interactuar con un escritorio en concreto se pulsa en el icono ![](./deployments.es.images/deployment10.png):

![](./deployments.es.images/deployment11.png) 



## Como recrear un despliegue

Para recrear un despliegue, se debe ir a la opción de Gestionar en la barra superior. Una vez ahí:

![deployment16](./deployments.es.images/deployment16.png)

Se selecciona el despliegue que se quiera recrear 

![deployment15](./deployments.es.images/deployment15.png)

Pulsar sobre el botón ![](./deployments.es.images/deployment13.png) para recrear.



