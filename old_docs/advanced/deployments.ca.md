# Desplegaments

Els desplegaments són escriptoris creats per a altres usuaris.

## Com crear desplegaments

Per crear un desplegament, cal anar a la barra superior

![](./deployments.ca.images/deployment1.png)

I prémer el menú desplegable ![](./deployments.ca.images/deployment2.png)

Anar a l'opció "Gestionar" ![](./deployments.ca.images/deployment3.png)

Un cop polsat, es redirigirà a aquesta pàgina:

![](./deployments.ca.images/deployment4.png)

Es polsa en el botó ![](./deployments.ca.images/deployment12.png) per crear un nou desplegament.

Un cop hagueu premut, apareixerà un pop-up perquè es pugui assignar un nom, seleccionar una plantilla i els usuaris al qual crear aquests escriptoris:

![](./deployments.ca.images/deployment5.png)

I aquí es veuran els desplegaments que s'hagin creat:

![](./deployments.ca.images/deployment6.png)



## Com esborrar desplegaments

Per esborrar un desplegament, cal anar a l'opció de "Gestionar" a la barra superior. Un cop aquí:

![deployment16](./deployments.ca.images/deployment16.png)

Se selecciona el desplegament que es vulgui esborrar:

![deployment15](./deployments.ca.images/deployment15.png)

Es polsa sobre el botó ![](./deployments.ca.images/deployment14.png) per esborrar.



## Com veure desplegaments

En la finestra de desplegaments:

![](./deployments.ca.images/deployment6.png)

Es selecciona l'opció de "Toggle visible to user (will stop desktops running)" perquè els escriptoris estiguin visibles i executats.

![](./deployments.ca.images/deployment7.png)

Es polsa en el menú desplegable ![](./deployments.ca.images/deployment3.png) i es selecciona l'opció de "Veure".

Et redirigirà a aquesta pàgina on es poden trobar tots els desplegament que s'han creat.

![](./deployments.ca.images/deployment8.png)

Quan se selecciona un dels desplegaments, es veuran els escriptoris que hagin estat creats per als usuaris.

![](./deployments.ca.images/deployment9.png)

Per interactuar amb un escriptori en concret es polsa a la icona ![](./deployments.ca.images/deployment10.png) : 

![](./deployments.ca.images/deployment11.png) 



## Com recrear un desplegament

Per recrear un desplegament, cal anar a l'opció de "Gestionar" a la barra superior. Un cop aquí:

![deployment16](./deployments.ca.images/deployment16.png)

Es selecciona el desplegament que es vulgui recrear

![deployment15](./deployments.ca.images/deployment15.png)

Es polsa sobre el botó ![](./deployments.ca.images/deployment13.png) per recrear.



