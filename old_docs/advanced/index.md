# Introduction

Users of IsardVDI are classified in [roles](../manager/users.md#roles). This section describes features that can be used byadvanced role users.

Users with **advanced role** have the features of all [users](../user/index.md) and those described in this section.
