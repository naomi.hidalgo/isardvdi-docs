# Deployments

Deployments are desktops created for other users.

## How to create deployments

To create a deployment, you need to go to the top bar

![](./deployments.en.images/deployment1.png)

And click on the menu option ![](./deployments.en.images/deployment2.png)

Go to the option "Manage" ![](./deployments.en.images/deployment3.png)

Once you click, you will be redirected to this page:

![](./deployments.en.images/deployment4.png)

Click on the button ![](./deployments.en.images/deployment12.png) to create a new deployment



Once clicked, this will pop up so you can assign a name, select a template and the users to create this desktops to:

![](./deployments.en.images/deployment5.png)

And here you will see that the deployments have been created: 

![](./deployments.en.images/deployment6.png)



## How to delete deployments

To delete a deployment, you need to be in the Manage option in the top bar. Once there:

![deployment16](./deployments.en.images/deployment16.png)

Select the deployment you want to delete:

![deployment15](./deployments.en.images/deployment15.png)

Click the button ![](./deployments.en.images/deployment14.png) to delete.



## How to view deployments

In the deployments manage page:

![](./deployments.en.images/deployment6.png)

Select the option "Toggle visible to user (will stop desktops running)" to make the desktops visible and running.

![](./deployments.en.images/deployment7.png)



Click on the menu option ![](./deployments.en.images/deployment3.png) and select the option "View".

It will redirect you to this page where you can find all the deployments you have created:

![](./deployments.en.images/deployment8.png)

When you click on one of the deployments, you will see the desktops you have created for the users.

![](./deployments.en.images/deployment9.png)

To interact with a specific desktop, click on the icon ![](./deployments.en.images/deployment10.png):

![](./deployments.en.images/deployment11.png) 



## How to recreate a deployment

To recreate a deployment, you need to be in the Manage option in the top bar. Once there:

![deployment16](./deployments.en.images/deployment16.png)

Select the deployment that you want to recreate:

![deployment15](./deployments.en.images/deployment15.png)

Click on the button ![](./deployments.en.images/deployment13.png) to recreate. 



