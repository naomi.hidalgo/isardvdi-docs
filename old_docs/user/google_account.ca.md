# Accés mitjançant Google

Des de la pàgina principal és possible accedir-hi mitjançant un compte de correu de Google.

![](./google_account.ca.images/google_ca1.png)

Polsar sobre la icona ![](./google_account.es.images/google9.png) de la pantalla principal d'IsardVDI

Demanarà un compte de correu de Google

![](./google_account.ca.images/google_ca2.png)

Afegeix l'adreça de correu i premeu sobre la icona següent i sol·licitarà la contrasenya del compte de Google.

![](./google_account.ca.images/google_ca3.png)

Un cop introduïda, polsar sobre la icona següent. Si és la primera vegada apareixerà el formulari de registre.   

![](./google_account.ca.images/google_ca4.png)

El codi de registre ens ho han de proporcionar els gestors de l'organització.

Introduir el codi que ens han proporcionat i polsar sobre la icona ![](./google_account.ca.images/google_ca5.png)

Accedint a la interfície bàsica autenticats amb el compte de Google.

![](./basic_interface.ca.images/interface7.png)

Un cop autenticats, ja no cal repetir el procés de registre.
Quan es vulgui tornar a accedir mitjançant Google, només cal prémer sobre la icona ![](./google_account.es.images/google9.png) i utilitzar les credencials de Google.