# Google Access

It is possible to access through your Google email account from the homepage. 

![](./google_account.en.images/google_en1.PNG)

Click on the botton:
![](./google_account.es.images/google9.png)
on the homepage of IsardVDI  

It will ask for a Google email account.

![](./google_account.en.images/google_en2.PNG)



Add the email address, click on the "Next" icon and it will ask for your Google account password.

![](./google_account.en.images/google_en3.PNG)

Once inserted, click on the "Next" icon. If it's the first time, the registration form will appear. 

![](./google_account.en.images/google_en4.PNG)



The sign up code must be provided by the managers of the organization.

Enter the code that they have provided us and click on the icon
![](./google_account.en.images/google_en5.PNG)

Accessing the basic interface authenticated with the Google account.

![](./basic_interface.en.images/interface3.png)

Once you are authenticated, you will no longer require to repeat the registration process.
When we want to access again through Google, we only have to click on the icon
![](./google_account.es.images/google9.png)
and use the Google credentials.

