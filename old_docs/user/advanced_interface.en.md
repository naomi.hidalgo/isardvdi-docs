# Advanced Interface

Description of the options available in the advanced IsardVDI interface.

## How to log in
To log in, you must access the IsardVDI URL, and access using the available credentials or authenticating through a Google account.

![](./advanced_interface.en.images/interface1.png)

To access the advanced interface, click on the icon:

![](./advanced_interface.en.images/interface5.png)

## How to log out
To log out from the advanced interface there are two options, by clicking on the icon:

![](./advanced_interface.en.images/interface12.png)

or by clicking on the icon:

![](./advanced_interface.en.images/interface30.png)

and then click on Log Out

![](./advanced_interface.en.images/interface31.png)

## How to see the guide
To see the guide, click on the icon:

![](./advanced_interface.en.images/interface32.png)

and then on the "Read documentation" icon

![](./advanced_interface.en.images/interface33.png)

## How to start a temporary desktop
From the advanced interface it is not possible to start a temporary desktop, but it is possible to connect to it if it has been started from the basic interface.
The following screenshot shows the appearance of the advanced interface, and the temporary desktops do not appear:

![](./advanced_interface.en.images/interface11.png)

If we go back to the basic one by clicking on the Home icon:

![](./advanced_interface.en.images/interface12.png)

We start one or more of the temporary desktops, by clicking on the create icon:

![](./advanced_interface.en.images/interface9.png)

And we return to the advanced interface from the following icon:

![](./advanced_interface.en.images/interface5.png)

We check that now if the temporary desktops appear so that we can connect to them.

![](./advanced_interface.en.images/interface13.png)

## How to select a viewer
From the advanced interface, you have to click on the icon:

![](./advanced_interface.en.images/interface18.png)

As in the basic interface, the options of the SPICE viewer or in the browser appear

![](./advanced_interface.en.images/interface19.png)

## How to delete a temporary desktop
From the advanced interface the option to delete the temporary desktop does not appear directly, you have to click on the icon

![](./advanced_interface.en.images/interface23.png)

Here we see the option to remove it, but it is disabled:

![](./advanced_interface.en.images/interface24.png)

In order to activate it, you have to click on the icon

![](./advanced_interface.en.images/interface21.png)

And once the desktop is stopped, we click on the icon again:

![](./advanced_interface.en.images/interface23.png)

and the option to eliminate the temporary desktop appears active.

![](./advanced_interface.en.images/interface25.png)

## How to start a persistent desktop
To start a persistent desktop, click on the icon:

![](./advanced_interface.en.images/interface28.png)

## How to stop a persistent desktop
To stop a persistent desktop, click on the icon:

![](./advanced_interface.en.images/interface21.png)

You can also force the desktop to stop after clicking the Stop icon with:

 ![](./advanced_interface.en.images/interface22.png)

