# Desktops

Desktops menú will allow you to manage your own desktops.

## Add new desktop

You have a button on the top right corner that will open a modal form where you can choose a template desktop as a base for your new desktop and set the hardware within your user [quota](quotas.md) limits.

NOTE: If there are no templates to choose from you will need to [create one from an existing desktop](desktops.md#convert-to-template) or, as [advanced user](../advanced/index.md), [create new one installing it from a downloaded media ISO](../advanced/media.md#create-new-desktop-from-uploaded-media).

You shoul fill the form an click on create desktop:

- **Name**: Fill in your desired desktop name
- **Description**: Give it a description (optional)
- **Search an select template**: Find a template using the search input box provided that will filter template table list and click on the template you want to create your desktop from.
- **Set hardware**: After selecting a template you will be shown with a button that opens a selection of hardware that you can choose for your new desktop, always within your user [quota](quotas.md) limits.

After creating the desktop you will find it in stopped status with a green button to start it. If you start it, a modal viewer form will be shown where you can choose your connection type.

### Connect to viewer

The first time you start a desktop you will be presented with a viewer selection form with all available types of connections to your desktop:

![](../images/desktops/viewer-form.png)

| CONNECTION TYPE                                 | **CLIENT NEEDED**                                            | **SECURE** |
| ----------------------------------------------- | ------------------------------------------------------------ | ---------- |
| **Spice client**<br /><u>(PREFERRED CLIENT)</u> | **Linux**: virt-viewer (debian based), remote-viewer (RH based) <br />**Win**: [virt-viewer](https://virt-manager.org/download/sources/virt-viewer)<br />**Mac**: No client tested to be working<br />**Android**: [aSpice viewer](https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE)<br />**iOS**: [FlexVDI](https://itunes.apple.com/us/app/flexvdi-client/id1051361263) | YES        |
| **Spice browser**                               | Any modern browser: Firefox, Chromium, ...                   | YES        |
| **VNC browser**                                 | Any modern browser: Firefox, Chromium, ...                   | YES        |
| **VNC client**                                  | **Linux**: vinagre -F console.vv <br />**Win**: [RealPlayer](https://www.realvnc.com/en/connect/download/viewer/linux/) ***1**<br />**Mac**: Default VNC client in Mac works<br />**Android**: Not tested<br />**iOS**: Not tested | NO***2**   |

***1**: When opening file in RealPlayer it will ask for password. You can copy&paste password from desktop details.

***2**: VNC connections in KVM through a VNC client are not being encrypted. To connect securely with VNC it should be used a previously created encrypted tunnel or VPN connection.

## Desktop details

When you click on the **+** sign to the left of each desktop a details view will open. There you will find actions and information about desktop hardware being used and system status information.

![](../images/desktops/desktop-detail.png)

### Edit desktop

When desktop is stopped you will be able to edit them in the edit form that will open:

![](../images/desktops/desktop-edit-form.png)

- **Description**: This is optional and can be updated.
- **Options**:
  - Viewer fullscreen: If checked it will set that paraemeter in client viewer files so it will automatically open the guest in fullscreen.
- **Hardware**: Here you can modify the hardware, networks and boot for that desktop within the [quota](quotas.md) and [alloweds](../advanced/allows.md) for your user.
- **Media**: Here you can add or remove ISO and Floppy images already uploaded into the system and shared with your user, group, category or role.

When you finish and click on **Modify desktop** button a bunch of operations will be held by IsardVDI engine to ensure that the new desktop configuration can be started in the system. If it finishes in a *Fail* state you should review your modifications.

### Delete desktop

When the desktop is stopped you can delete it. This action will ask for confirmation as it is **NOT REVERSIBLE**. You will lose anything that was related to that desktop.

