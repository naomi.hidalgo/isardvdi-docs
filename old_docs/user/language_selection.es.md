# Selección de idioma

Existen varios idiomas disponibles para IsardVDI, el idioma se tiene que seleccionar antes de iniciar sesión desde la pagina principal.

![](./language_selection.es.images/language1.png)

Para seleccionar el idioma hay que pulsar sobre el icono:

![](./language_selection.es.images/language2.png)

En el desplegable eleccionamos el idioma:

![](./language_selection.es.images/language3.png)
