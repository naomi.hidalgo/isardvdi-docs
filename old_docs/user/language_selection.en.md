# Language selection

There are several languages available for IsardVDI, the language has to be selected before logging in from the main page.

![](./language_selection.en.images/language1.png)

To select the language, click on the following icon:

![](./language_selection.en.images/language2.png)

In the drop-down menu we choose the language:

![](./language_selection.en.images/language3.png)
