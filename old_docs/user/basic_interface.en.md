# Basic Interface

Description of the options available in the basic IsardVDI interface.

## How to log in
To log in, you must access the IsardVDI URL, and access using the available credentials or authenticating through a Google account.

![](./basic_interface.en.images/interface1.png)

## How to log out
To log out, click on the icon
![](./basic_interface.es.images/interface2.png)
from the IsardVDI main screen

![](./basic_interface.en.images/interface3.png)

## How to view the guide
To see the guide, click on the icon
![](./basic_interface.en.images/interface4.png)

## How to switch to the advanced interface
To be able to see the advanced interface from the basic one, click on the icon
![](./basic_interface.en.images/interface5.png)


## How to switch between card and table views
To switch between card and table views, click on the icon
![](./basic_interface.es.images/interface6.png)
By default the card option is checked, in the following screenshot you can see the initial view:

![](./basic_interface.en.images/interface3.png)

If we click on the previous icon, the view remains as in the following screenshot:

![](./basic_interface.en.images/interface8.png)

## How to start a temporary desktop
To start a temporary desktop, click on the following icon:
![](./basic_interface.en.images/interface9.png)
from one of the options available in the basic interface

![](./basic_interface.en.images/interface7.png)


## How to select a viewer
From the basic interface, you have to click on the icon:
![](./basic_interface.en.images/interface14.png)
and choose between two options SPICE Viewer or Viewer in the browser
For the SPICE Viewer, a "Virtual Machine Viewer" application must be installed, for the second option the desktop will open in a new browser window.

![](./basic_interface.en.images/interface15.png)

Capture with the SPICE viewer:

![](./basic_interface.es.images/interface16.png)

Capture with the browser:

![](./basic_interface.es.images/interface17.png)

## How to delete a temporary desktop
To delete a temporary desktop, click on the icon:
![](./basic_interface.en.images/interface20.png)

## How to start a persistent desktop
To start a persistent desktop, click on the icon:
![](./basic_interface.en.images/interface26.png)

![](./basic_interface.en.images/interface27.png)

## How to stop a persistent desktop
To stop a persistent desktop, click on the icon:
![](./basic_interface.en.images/interface29.png)
