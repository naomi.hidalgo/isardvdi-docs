# Selecció d'idioma

Hi ha diversos idiomes disponibles per a IsardVDI, l'idioma s'ha de seleccionar abans d'iniciar sessió des de la pàgina principal.

![](./language_selection.ca.images/language1.png)

Per seleccionar l'idioma, feu clic a la següent icona:

![](./language_selection.ca.images/language2.png)

Al menú desplegable escollim l'idioma:

![](./language_selection.ca.images/language3.png)
