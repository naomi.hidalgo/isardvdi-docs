# Interfaz básica

Descripción de las opciones disponibles en la interfaz básica de IsardVDI.

## Como iniciar sesión
Para iniciar sesión hay que acceder a la URL de IsardVDI, y acceder mediante las credenciales disponibles o autenticando mediante una cuenta de Google.

![](./basic_interface.es.images/interface1.png)

## Como cerrar sesión
Para cerrar sesión hay que pulsar sobre el icono ![](./basic_interface.es.images/interface2.png) desde la pantalla principal de IsardVDI

![](./basic_interface.es.images/interface3.png)

## Como ver la guía
Para ver la guía hay que pulsar sobre el icono ![](./basic_interface.es.images/interface4.png)

## Como cambiar a la interfaz avanzada
Para poder ver la interfaz avanzada desde la básica, hay que pulsar sobre el icono ![](./basic_interface.es.images/interface5.png)


## Como cambiar entre las vistas de tarjeta y de tabla
Para cambiar entre las vistas de tarjeta y de tabla hay que pulsar sobre el icono 
![](./basic_interface.es.images/interface6.png)
Por defecto esta marcada la opción de tarjeta, en la siguiente captura se ve la vista inicial:

![](./basic_interface.es.images/interface7.png)

Si pulsamos sobre el icono anterior la vista queda como en la siguiente captura:

![](./basic_interface.es.images/interface8.png)

## Como iniciar un escritorio temporal
Para iniciar un escritorio temporal hay que pulsar sobre el siguiente icono:
![](./basic_interface.es.images/interface9.png)
desde una de las opciones disponibles en la interfaz básica

![](./basic_interface.es.images/interface10.png)


## Como seleccionar un visor
Desde la interfaz básica, hay que pulsar sobre el icono visores:
![](./basic_interface.es.images/interface14.png)
y elegir entre dos opciones Visor SPICE o Visor en el navegador
Para el Visor SPICE, hace falta tener instalada una aplicación "Virtual Machine Viewer", para la segunda opción el escritorio se abrirá en una nueva ventana del navegador.

![](./basic_interface.es.images/interface15.png)

Captura con el visor SPICE:

![](./basic_interface.es.images/interface16.png)

Captura con el navegador:

![](./basic_interface.es.images/interface17.png)

## Como eliminar un escritorio temporal
Para eliminar un escritorio temporal hay que pulsar sobre el icono Eliminar:
![](./basic_interface.es.images/interface20.png)

## Como iniciar un escritorio persistente
Para iniciar un escritorio persistente hay que pulsar sobre el icono Iniciar:
![](./basic_interface.es.images/interface26.png)

![](./basic_interface.es.images/interface27.png)

## Como detener un escritorio persistente
Para detener un escritorio persistente hay que pulsar sobre el icono:
![](./basic_interface.es.images/interface29.png)
