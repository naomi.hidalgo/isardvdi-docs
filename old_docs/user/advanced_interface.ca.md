# Interfície avançada

Descripció de les opcions disponibles a la interfície avançada de IsardVDI.

## Com iniciar sessió
Per iniciar sessió cal accedir a l'URL de IsardVDI, i accedir mitjançant les credencials disponibles o autenticant mitjançant un compte de Google.

![](./advanced_interface.ca.images/interface1.png)

Per accedir a la interfície avançada cal prémer sobre la icona:

![](./advanced_interface.ca.images/interface5.png)

## Com tancar sessió
Per tancar sessió des de la interfície avançada hi ha dues opcions, prement sobre la icona:

![](./advanced_interface.en.images/interface12.png)

o prement sobre la icona:

![](./advanced_interface.en.images/interface30.png)

i tot seguit prémer sobre "Log Out"

![](./advanced_interface.en.images/interface31.png)

## Com veure la guia
Per veure la guia cal prémer sobre la icona:

![](./advanced_interface.en.images/interface32.png)

i tot seguit sobre l'icona "Read documentation"

![](./advanced_interface.en.images/interface33.png)

## Com iniciar un escriptori temporal
Des de la Interfície avançada no és possible iniciar un escriptori temporal, però si connectar-se a ell si s'ha iniciat des de la interfície bàsica. En la següent captura es veu l'aspecte de la interfície avançada, i no apareixen els escriptoris temporals:

![](./advanced_interface.en.images/interface11.png)

Si tornem a la bàsica prement sobre la icona Home:

![](./advanced_interface.en.images/interface12.png)

Iniciem un o diversos dels escriptoris temporals, prement sobre la icona de crear:

![](./advanced_interface.ca.images/interface9.png)

I tornem a la Interfície avançada des del següent icona:

![](./advanced_interface.ca.images/interface5.png)

Comprovem que ara si apareixen els escriptoris temporals perquè ens puguem connectar a ells.

![](./advanced_interface.en.images/interface13.png)

## Com seleccionar un visor
Des de la interfície avançada, cal prémer sobre la icona:

![](./advanced_interface.en.images/interface18.png)

Igual que a la interfície bàsica ens apareixen les opcions del visor SPICE o al navegador:

![](./advanced_interface.en.images/interface19.png)

## Com suprimir un escriptori temporal
Des de la interfície avançada no apareix directament l'opció d'eliminar l'escriptori temporal, cal prémer sobre la icona

![](./advanced_interface.en.images/interface23.png)

Aquí ens apareix l'opció per eliminar-lo, però està desactivada:

![](./advanced_interface.en.images/interface24.png)

Per poder activar-lo, cal prémer sobre la icona

![](./advanced_interface.en.images/interface21.png)

I un cop aturat l'escriptori, tornem a prémer sobre la icona:

![](./advanced_interface.en.images/interface23.png)

i ja ens apareix activa l'opció per eliminar l'escriptori temporal.

![](./advanced_interface.en.images/interface25.png)

## Com iniciar un escriptori persistent
Per iniciar un escriptori persistent cal prémer sobre la icona:

![](./advanced_interface.en.images/interface28.png)

## Com aturar un escriptori persistent
Per aturar un escriptori persistent cal prémer sobre la icona:

![](./advanced_interface.en.images/interface21.png)

També es pot forçar la detenció de l'escriptori un cop premuda la icona Stop amb: 

![](./advanced_interface.en.images/interface22.png)
