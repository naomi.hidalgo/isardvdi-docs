# Interfícies web

Hi ha interfícies d'arbre que donen diferents tipus d'accés a IsardVDI, l'[enllaç directe](#enllac-directe), la [*interfície d'administració completa*]() i una [*interfície d'usuari simplificada*](). Això permet als [usuaris avançats](../advanced/index.ca.md) i als [usuaris mànager](../manager/index.ca.md) administrar plantilles, límits i quotes al mateix temps que deixa una interfície simplificada als usuaris.

## Enllaç directe 
Un l'[usuari avançat](../advanced/index.ca.md) us podria proporcionar un enllaç directe a un escriptori virtual. Si obriu l'enllaç amb un navegador podreu seleccionar accedir-hi a través del navegador o amb un [client local (seguiu l'enllaç per a instruccions d'instal·lació)](local-client.md).

## Interfície web d'usuari simplificada

Aquesta interfície simplificada és la interfície principal quan l'usuari accedeix a l'arrel de l'URL del domini, Ex. <https://desktops.isardvdi.com>. Es pretén proporcionar una interfície d'usuari simplificada per accedir a escriptoris no persistents creats des de les plantilles que te accés. Ho ampliarem per a permetre gestionar els escriptoris persistents en el futur.

![nonpersistent_login](../images/first-steps/nonpersistent_login.png)

Aquesta interfície permet: 

- Accés amb usuaris IsardVDI amb un nom d'usuari i contrasenya.
- Accés utilitzant comptes de Google i Github. El primer cop necessitareu un testimoni proporcionat per un [mànager](../manager/index.ca.md). Potser l'administrador del sistema no permetrà aquest tipus d'accés.
- Accés a escriptoris no persistents vius, destruïts en tancar la sessió. 
- Opció de múltiples plantilles

![nonpersistent_login](../images/first-steps/nonpersistent_access.png)

Amb aquesta interfície simple, l'usuari té dos tipus de visors: 

- Client local: Ús del client Spice. Les instruccions d'instal·lació són al [Client Local](local-client.md).
- Navegador: Ús del mateix navegador amb HTML5.

Si l'usuari té accés a més d'un escriptori no persistent (té accés a més d'una plantilla) el sistema preguntarà quin escriptori no persistent vol crear i accedir en iniciar la sessió: 

![nonpersistent_login](../images/first-steps/enrollment_templates.png)

## Interfície web d'administració completa

Aquesta interfície està destinada a funcions d'[administrador](../admin/index.ca.md), [mànager](../manager/index.ca.md) i [avançat](../advanced/index.ca.md), ja que els aporta un control complet. Els [usuaris](../user/index.ca.md) normals també poden accedir a aquesta interfície d'administració si se'ls permet tenir escriptoris persistents.

L'URL per accedir a aquesta interfície web per a la categoria *Per defecte* és a **/isard-admin** si no creeu una configuració multitenanciària. Per exemple <https://desktops.isardvdi.com/isard-admin>. 

![nonpersistent_login](../images/first-steps/login_default.png)

Els [administradors](../admin/index.ca.md) poden crear un acces **multitenant** creant categories. Per a configuracions de multitenància cada usuari ha d'accedir a través del seu propi portal de categoria a **/isard-admin/login/[categoria]**. 

![nonpersistent_login](../images/first-steps/login_acme.png)
