# Interfaz avanzada

Descripción de las opciones disponibles en la interfaz avanzada de IsardVDI.

## Como iniciar sesión
Para iniciar sesión hay que acceder a la URL de IsardVDI, y acceder mediante las credenciales disponibles o autenticando mediante una cuenta de Google.

![](./advanced_interface.es.images/interface1.png)

Para acceder a la interfaz avanzada hay que pulsar sobre el icono:

![](./advanced_interface.es.images/interface5.png)

## Como cerrar sesión
Para cerrar sesión desde la interfaz avanzada hay dos opciones, pulsando sobre el icono:

![](./advanced_interface.en.images/interface12.png)

o pulsando sobre el icono:

![](./advanced_interface.en.images/interface30.png)

y a continuación pulsar sobre "Log Out"

![](./advanced_interface.en.images/interface31.png)

## Como ver la guia
Para ver la guía hay que pulsar sobre el icono: 

![](./advanced_interface.en.images/interface32.png)

y a continuación sobre el icono "Read documentation"

![](./advanced_interface.en.images/interface33.png)

## Como iniciar un escritorio temporal
Desde la Interfaz avanzada no en posible iniciar un escritorio temporal, pero si conectarse a el si se ha iniciado desde la interfaz básica.
En la siguiente captura se ve el aspecto de la interfaz avanzada, y no aparecen los escritorios temporales:

![](./advanced_interface.en.images/interface11.png)

Si volvemos a la básica pulsando sobre el icono Home:

![](./advanced_interface.en.images/interface12.png)

Iniciamos uno o varios de los escritorios temporales, pulsando sobre el icono de crear:

![](./advanced_interface.es.images/interface9.png)

Y volvemos a la Interfaz avanzada desde el siguiente icono:

![](./advanced_interface.es.images/interface5.png)

Comprobamos que ahora si aparecen los escritorios temporales para que nos podamos conectar a ellos.

![](./advanced_interface.en.images/interface13.png)

## Como seleccionar un visor
Desde la interfaz avanzada, hay que pulsar sobre el icono:

![](./advanced_interface.en.images/interface18.png)

Igual que en la interfaz básica nos aparecen las opciones del visor SPICE o en el navegador:

![](./advanced_interface.en.images/interface19.png)

## Como eliminar un escritorio temporal
Desde la interfaz avanzada no aparece directamente la opción de eliminar el escritorio temporal, hay que pulsar sobre el icono 

![](./advanced_interface.en.images/interface23.png)

Aqui nos aparece la opcion para eliminarlo, pero esta desactivada:

![](./advanced_interface.en.images/interface24.png)

Para poder activarlo, hay que pulsar sobre el icono

![](./advanced_interface.en.images/interface21.png)

Y una vez parado el escritorio, volvemos a pulsar sobre el icono: 

![](./advanced_interface.en.images/interface23.png)

y ya nos aparece activa la opción para eliminar el escritorio temporal.

![](./advanced_interface.en.images/interface25.png)

## Como iniciar un escritorio persistente
Para iniciar un escritorio persistente hay que pulsar sobre el icono:

![](./advanced_interface.en.images/interface28.png)

## Como detener un escritorio persistente
Para detener un escritorio persistente hay que pulsar sobre el icono:

![](./advanced_interface.en.images/interface21.png)

También se puede forzar la detención del escritorio una vez pulsado el icono Stop con:

 ![](./advanced_interface.en.images/interface22.png)

