# Interfície bàsica

Descripció de les opcions disponibles a la interfície bàsica de IsardVDI.

## Com iniciar sessió
Per iniciar sessió cal accedir a l'URL de IsardVDI, i accedir mitjançant les credencials disponibles o autenticant mitjançant un compte de Google.

![](./basic_interface.ca.images/interface1.png)

## Com tancar sessió
Per tancar sessió cal prémer sobre la icona ![](./basic_interface.es.images/interface2.png) des de la pantalla principal de IsardVDI

![](./basic_interface.ca.images/interface3.png)

## Com veure la guia
Per veure la guia cal prémer sobre la icona
![](./basic_interface.ca.images/interface4.png)

## Com canviar a la interfície avançada
Per poder veure la interfície avançada des de la bàsica, cal prémer sobre la icona
![](./basic_interface.ca.images/interface5.png)


## Com canviar entre les vistes de targeta i de taula
Per canviar entre les vistes de targeta i de taula cal prémer sobre la icona
![](./basic_interface.es.images/interface6.png)
Per defecte està marcada l'opció de targeta, en la següent captura es veu la vista inicial:

![](./basic_interface.ca.images/interface7.png)

Si premem sobre la icona anterior la vista queda com en la següent captura:

![](./basic_interface.ca.images/interface8.png)

## Com iniciar un escriptori temporal
Per iniciar un escriptori temporal cal prémer sobre la següent icona:
![](./basic_interface.ca.images/interface9.png)
des d'una de les opcions disponibles a la interfície bàsica

![](./basic_interface.ca.images/interface10.png)


## Com seleccionar un visor
Des de la interfície bàsica, cal prémer sobre la icona:
![](./basic_interface.ca.images/interface14.png)
i triar entre dues opcions Visor SPICE o Visor al navegador.
Pel Visor SPICE, cal tenir instal·lada una aplicació "Virtual Machine Viewer", per a la segona opció l'escriptori s'obrirà en una nova finestra de navegador.

![](./basic_interface.ca.images/interface15.png)

Captura amb el visor SPICE:

![](./basic_interface.ca.images/interface16.png)

Captura amb el navegador:

![](./basic_interface.ca.images/interface17.png)

## Com suprimir un escriptori temporal
Per suprimir un escriptori temporal cal prémer sobre la icona Esborrar:
![](./basic_interface.ca.images/interface20.png)

## Com iniciar un escriptori persistent
Per iniciar un escriptori persistent cal prémer sobre la icona:
![](./basic_interface.ca.images/interface26.png)

![](./basic_interface.ca.images/interface27.png)

## Com aturar un escriptori persistent
Per aturar un escriptori persistent cal prémer sobre la icona:
![](./basic_interface.ca.images/interface29.png)
