# Manage users

To manage users you have to go to the Administration panel, press the button ![](./manage_user.images/manage_user26.png)

![](./manage_user.images/manage_user27.png)

Press the button ![](./manage_user.images/manage_user24.png)

![](./manage_user.images/manage_user25.png)

## Group Creation

To create groups you have to go to the "Administration" panel in the "Users" section, look for where it says "Groups" and press the button ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user2.png)

And fill the fields in the form

![](./manage_user.images/manage_user3.png)

* Auto desktops creation: A desktop will be created when a user logs in with the chosen template

* Ephimeral desktops: To be able to put a limited time of use to a temporary desktop. (For this you also have to configure, being Admin, in Config the "Job Scheduler")

## Add Users

To add users you have to go to the "Administration" panel under the "Users" section

### Individually

Press the button ![](./manage_user.images/manage_user4.png)

![](./manage_user.images/manage_user5.png)

And a dialog box will appear with the following form: 

![](./manage_user.images/manage_user6.png)

And fill the fields in the form

### Mass Creation

Press the button ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png)

And a dialog box will appear with the following form: 

![](./manage_user.images/manage_user9.png)

An example file can be downloaded by clicking the button ![](./manage_user.images/manage_user10.png). A form will appear to fill out:

![](./manage_user.images/manage_user11.png)

Once filled out, it can be uploaded on

![](./manage_user.images/manage_user12.png)


## Enrollment keys

In the "Groups" section and in the name of the group that you want to obtain the code, press the button ![](./manage_user.images/manage_user13.png)

![](./manage_user.images/manage_user14.png)

The group is displayed with some options. Press the button ![](./manage_user.images/manage_user15.png)

![](./manage_user.images/manage_user16.png)

A dialog box will appear where you can copy the code you want to give. Teachers, for example, will be given the "Advanced" code and students the "Users" code.

![](./manage_user.images/manage_user17.png)


## Quota and Limits

### Quota

Quotas define the amount of resources that can be used based on the group to which the user belongs to.

To manage the quotas, press the button ![](./manage_user.images/manage_user21.png)

![](./manage_user.images/manage_user22.png)

![](./manage_user.images/manage_user23.png)


### Limits

Limits define the amount of resources that can be used at the same time based on the group to which the user belongs to.

To manage the limits, press the button ![](./manage_user.images/manage_user18.png)

![](./manage_user.images/manage_user19.png)

Therefore if the "Unlimited" button is pressed, all users will have access to create unlimited desktops, with unlimited CPU, memory, disk size, templates, and isos. But if you uncheck the box, you can choose a limit to each resource. For example, if you want to give a desktop creation limit of 20, only 20 can be created in this group.

If the "Propagate to related" box is checked, the configuration chosen will be inherited for all users of this group/category

![](./manage_user.images/manage_user20.png)