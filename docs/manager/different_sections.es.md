# Diferentes apartados

1. **User**: En este apartado se puede ver todo el que el propio usuario tiene/ha creado.

2. **Manager**: En este apartado se puede ver todo el que el resto de usuarios de la misma categoría hayan creado.

![](./different_sections.images/different_sections1.png)