# Esborrar Plantilles

**COMPTE: Hi ha dependències, i si s'esborra una plantilla, s'esborrarà l'escriptori "pare"**

Per a poder esborrar una plantilla s'ha d'anar al panell de "Administració". 

Es prem el botó ![](./delete_templates.ca.images/delete_templates1.png)

![](./delete_templates.ca.images/delete_templates2.png)

Una vegada allí es va a "Templates" sota l'apartat de "Manager". 

Es prem el botó ![](./delete_templates.images/delete_templates3.png) 

![](./delete_templates.images/delete_templates4.png) 

Es prem el botó ![](./delete_templates.images/delete_templates5.png) de la plantilla que es vulgui esborrar.

![](./delete_templates.images/delete_templates6.png) 

Es prem el botó ![](./delete_templates.images/delete_templates7.png) 

![](./delete_templates.images/delete_templates8.png) 

I sortirà una finestra de diàleg indicant la plantilla a eliminar. 

![](./delete_templates.images/delete_templates9.png) 


