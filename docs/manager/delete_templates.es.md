# Borrar Plantillas

**CUIDADO: Hay dependencias, y si se borra una plantilla, se borrará el escritorio "padre"**

Para poder borrar una plantilla se tiene que ir al panel de "Administración". 

Se pulsa el botón ![](./delete_templates.es.images/delete_templates1.png)

![](./delete_templates.es.images/delete_templates2.png)

Una vez allí se va a "Templates" bajo el apartado de "Manager".

Se pulsa el botón ![](./delete_templates.images/delete_templates3.png) 

![](./delete_templates.images/delete_templates4.png) 

Se pulsa el botón ![](./delete_templates.images/delete_templates5.png) de la plantilla que se quiera borrar.

![](./delete_templates.images/delete_templates6.png) 

Se pulsa el botón ![](./delete_templates.images/delete_templates7.png) 

![](./delete_templates.images/delete_templates8.png) 

Y saldrá una ventana de diálogo indicando la plantilla a eliminar. 

![](./delete_templates.images/delete_templates9.png) 