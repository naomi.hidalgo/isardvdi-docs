# Delete Templates

**WARNING: There are dependencies, and if a template is deleted, the "parent" desktop will be deleted**

In order to delete a template you have to go to the "Administration" panel.

Press the button ![](./delete_templates.images/delete_templates1.png)

![](./delete_templates.images/delete_templates2.png)

Once there, go to "Templates" under the "Manager" section.

Press the button ![](./delete_templates.images/delete_templates3.png) 

![](./delete_templates.images/delete_templates4.png) 

Press the button ![](./delete_templates.images/delete_templates5.png) of the template you want to delete.

![](./delete_templates.images/delete_templates6.png) 

Press the button ![](./delete_templates.images/delete_templates7.png) 

![](./delete_templates.images/delete_templates8.png) 

And a dialog box will appear indicating the template to be deleted. 

![](./delete_templates.images/delete_templates9.png) 