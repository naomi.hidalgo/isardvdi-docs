# Different sections

1. **User**: In this section you can see everything that the user has/has created.

2. **Manager**: In this section you can see everything that other users in the same category have created.

![](./different_sections.images/different_sections1.png)