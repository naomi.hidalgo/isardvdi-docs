# Plantilla Server

Para poder generar un server a partir de una plantilla se tiene que ir al panel de "Administración".

Se pulsa el botón ![](./template_server.es.images/template_server1.png)

![](./template_server.es.images/template_server2.png)

Una vez allí se va a "Templates" bajo el apartado de "Manager".

Se pulsa el botón ![](./template_server.images/template_server3.png)

![](./template_server.images/template_server4.png)

Se pulsa el botón ![](./template_server.images/template_server5.png) de la plantilla que se quiera convertir en server.

![](./template_server.images/template_server6.png)

Se pulsa el botón ![](./template_server.images/template_server7.png)

![](./template_server.images/template_server8.png)

Y aparecerá una ventana de diálogo. Se tiene que marcar la casilla "Set it as server"

![](./template_server.images/template_server9.png)