# Plantilla Server

Per a poder generar un server a partir d'una plantilla s'ha d'anar al panell de "Administració".

Es prem el botó ![](./template_server.ca.images/template_server1.png)

![](./template_server.ca.images/template_server2.png)

Una vegada allí es va a "Templates" sota l'apartat de "Manager".

Es prem el botó ![](./template_server.images/template_server3.png)

![](./template_server.images/template_server4.png)

Es prem el botó ![](./template_server.images/template_server5.png) de la plantilla que es vulgui convertir en server.

![](./template_server.images/template_server6.png)

Es prem el botó ![](./template_server.images/template_server7.png)

![](./template_server.images/template_server8.png)

I apareixerà una finestra de diàleg. S'ha de marcar la casella "Set it as server"

![](./template_server.images/template_server9.png)