# Gestionar usuarios

Para gestionar los usuarios se tiene que ir al panel de Administración, se pulsa el botón ![](./manage_user.es.images/manage_user1.png)

![](./manage_user.es.images/manage_user2.png)

Se pulsa el botón ![](./manage_user.images/manage_user24.png)

![](./manage_user.images/manage_user25.png)

## Creación de Grupos

Para crear grupos en el apartado de "Users", se busca donde ponga "Groups" y se pulsa el botón ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user2.png)

Y se rellenan los campos

![](./manage_user.images/manage_user3.png)

* Auto desktops creation: Se creará un escritorio cuando inicie sesión un usuario con la plantilla que se escoja 

* Ephimeral desktops: Para poder poner un tiempo limitado de uso a un escritorio temporal. (Para esto también se tiene que configurar, siendo Admin, en Config el "Job Scheduler")


## Agregar Usuarios

### Individualmente

Se pulsa el botón ![](./manage_user.images/manage_user4.png)

![](./manage_user.images/manage_user5.png)

Y saldrá una ventana de diálogo con el siguiente formulario: 

![](./manage_user.images/manage_user6.png)

Y se rellena los campos.

### Creación masiva

Se pulsa de botón ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png)

Y saldrá una ventana de diálogo con el siguiente formulario: 

![](./manage_user.images/manage_user9.png)

Se puede descargar un archivo de ejemplo pulsando el botón ![](./manage_user.images/manage_user10.png). Saldrá un formulario a rellenar:

![](./manage_user.images/manage_user11.png)

Una vez rellenado, se puede subir en 

![](./manage_user.images/manage_user12.png)


## Clave de autoregistro

En el apartado de "Grupos" y en el nombre del grupo que se quiera obtener el codigo se pulsa el botón ![](./manage_user.images/manage_user13.png)

![](./manage_user.images/manage_user14.png)

El grupo se despliega con unas opciones. Se pulsa el botón ![](./manage_user.images/manage_user15.png)

![](./manage_user.images/manage_user16.png)

Aparecerá una ventana de diálogo donde se puede copiar el código que se quira dar. A los profesores, por ejemplo, se les dará el código "Advanced" y a los alumnos el de "Users".

![](./manage_user.images/manage_user17.png)


## Cuotas y Límites

### Cuotas

Las cuotas definen la cantidad de recursos que se pueden utilizar basándose en el grupo al que pertenece el usuario.

Para administrar las cuotas se pulsa el botón ![](./manage_user.images/manage_user21.png)

![](./manage_user.images/manage_user22.png)

![](./manage_user.images/manage_user23.png)


### Límites

Los límites definen la cantidad de recursos que se pueden utilizar a la vez basándose en el grupo al que pertenece el usuario.

Para administrar los límites se pulsa el botón ![](./manage_user.images/manage_user18.png)

![](./manage_user.images/manage_user19.png)

Es decir, si se pulsa el botón de "Unlimited" todos los usuarios tendrán acceso a crear ilimitados escritorios, con CPU, memoria, tamaño de disco, plantillas, y isos ilimitados. Pero si se desmarca la casilla, se puede elegir un límite a cada recurso. Por ejemplo si se quiere dar un límite de creación de escritorios de 20, solo se podrán crear 20 en este grupo.

Si se marca la casilla de "Propagate to related" la configuración que se haya elegido, se heredará para todos los usuarios de este grupo/categoría

![](./manage_user.images/manage_user20.png)