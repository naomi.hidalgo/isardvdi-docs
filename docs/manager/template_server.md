# Template Server

To be able to generate a server from a template you have to go to the "Administration" panel.

Press the button ![](./template_server.images/template_server1.png)

![](./template_server.images/template_server2.png)

Once there, go to "Templates" under the "Manager" section.

Press the button ![](./template_server.images/template_server3.png)

![](./template_server.images/template_server4.png)

Press the button ![](./template_server.images/template_server5.png) of the template that you want to convert into a server.

![](./template_server.images/template_server6.png)

Press the button ![](./template_server.images/template_server7.png)

![](./template_server.images/template_server8.png)

And a dialog window will appear. You have to check the box "Set it as server"

![](./template_server.images/template_server9.png)