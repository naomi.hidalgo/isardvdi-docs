# Media

Se pulsa el botón ![](./media.es.images/media1.png)

![](./media.es.images/media2.png)

## User

Se pulsa el botón ![](./media.images/media3.png) bajo el apartado de "User"

![](./media.images/media4.png)

Aquí se pueden ver dos tipos de "Medios":

1. **Media files**: En este apartado se ven los "Medios" creados por el propio usuario.

1. **Media shared with you**: En este apartado se ven los "Medios" compartidos con el propio usuario.

![](./media.images/media5.png)

## Manager

Se pulsa el botón ![](./media.images/media3.png) bajo el apartado de "Manager"

![](./media.images/media6.png)

Aquí se pueden ver todos los "Medios" compartidos con la misma categoría que tiene el usuario.

![](./media.images/media7.png)