# Media

Es prem el botó ![](./media.ca.images/media1.png)

![](./media.ca.images/media2.png)

## User

Es prem el botó ![](./media.images/media3.png) sota l'apartat de "User"

![](./media.images/media4.png)

Aquí es poden veure dos tipus de "Mitjans":

1. **Media files**: En aquest apartat es veuen els "Mitjans" creats pel propi usuari.

1. **Media shared with you**: En aquest apartat es veuen els "Mitjans" compartits amb el propi usuari.

![](./media.images/media5.png)

## Manager

Es prem el botó ![](./media.images/media3.png) sota l'apartat de "Manager"

![](./media.images/media6.png)

Aquí es poden veure tots els "Mitjans" compartits amb la mateixa categoria que té l'usuari.

![](./media.images/media7.png)