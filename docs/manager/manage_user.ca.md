# Gestionar usuaris

Per a gestionar els usuaris s'ha d'anar al panell d'Administració, es prem el botó ![](./manage_user.ca.images/manage_user1.png)

![](./manage_user.ca.images/manage_user2.png)

Es prem el botó ![](./manage_user.images/manage_user24.png)

![](./manage_user.images/manage_user25.png)

## Creació de Grups

Per a crear grups s'ha d'anar al panell de "Administració" en l'apartat de "Users", es busca on posi "Groups" i es prem el botó ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user2.png)

I s'emplenen els camps

![](./manage_user.images/manage_user3.png)

* Auto desktops creation: Es crearà un escriptori quan iniciï sessió un usuari amb la plantilla que es triï

* Ephimeral desktops: Per a poder posar un temps limitat d'ús a un escriptori temporal. (Per a això també s'ha de configurar, sent Admin, en Config el "Job Scheduler")


## Afegir Usuaris

### Individualment

Es prem el botó ![](./manage_user.images/manage_user4.png)

![](./manage_user.images/manage_user5.png)

I sortirà una finestra de diàleg amb el següent formulari:

![](./manage_user.images/manage_user6.png)

I s'emplena els camps.

### Creació massiva

Es prem el botó ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png)

I sortirà una finestra de diàleg amb el següent formulari:

![](./manage_user.images/manage_user9.png)

Es pot descarregar un arxiu d'exemple prement el botó ![](./manage_user.images/manage_user10.png). Sortirà un formulari a emplenar:

![](./manage_user.images/manage_user11.png)

Una vegada emplenat, es pot pujar en 

![](./manage_user.images/manage_user12.png)


## Clau d'autoregistre

En l'apartat de "Grups" i en el nom del grup que es vulgui obtenir el codi es prem el botó ![](./manage_user.images/manage_user13.png)

![](./manage_user.images/manage_user14.png)

El grup es desplega amb unes opcions. Es prem el botó ![](./manage_user.images/manage_user15.png)

![](./manage_user.images/manage_user16.png)

Apareixerà una finestra de diàleg on es pot copiar el codi que es vulgui donar. Als professors, per exemple, se'ls donarà el codi "Advanced" i als alumnes el de "Users".

![](./manage_user.images/manage_user17.png)


## Quotas i Límits

### Quotas

Les quotes defineixen la quantitat de recursos que es poden utilitzar basant-se en el grup al qual pertany l'usuari. 

Per a administrar les quotes es prem el botó ![](./manage_user.images/manage_user21.png)

![](./manage_user.images/manage_user22.png)

![](./manage_user.images/manage_user23.png)


### Límits

Els límits defineixen la quantitat de recursos que es poden utilitzar alhora basant-se en el grup al qual pertany l'usuari.

Per a administrar els límits es prem el botó ![](./manage_user.images/manage_user18.png)

![](./manage_user.images/manage_user19.png)

És a dir, si es prem el botó de "Unlimited" tots els usuaris tindran accés a crear il·limitats escriptoris, amb CPU, memòria, grandària de disc, plantilles, i isos il·limitats. Però si es desmarca la casella, es pot triar un límit a cada recurs. Per exemple si es vol donar un límit de creació d'escriptoris de 20, només es podran crear 20 en aquest grup.

Si es marca la casella de "Propagate to related" la configuració que s'hagi triat, s'heretarà per a tots els usuaris d'aquest grup/categoria

![](./manage_user.images/manage_user20.png)


