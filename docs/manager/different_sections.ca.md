# Diferents apartats

1. **User**: En aquest apartat es pot veure tot el que el propi usuari té/ha creat.

2. **Manager**: En aquest apartat es pot veure tot el que la resta d'usuaris de la mateixa categoria hagin creat.

![](./different_sections.images/different_sections1.png)