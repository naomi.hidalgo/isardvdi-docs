# Quick Start

Follow these instructions to clone the repository and bring IsardVDI up.

```bash
git clone https://gitlab.com/isard/isardvdi
cd isardvdi
cp isardvdi.cfg.example isardvdi.cfg
./build.sh
docker-compose pull
docker-compose up -d
```

Please wait a minute the first time as the database will get populated before the IsardVDI login becomes available.

Browse to <https://localhost>. Default user is **admin** and default password is **IsardVDI**.

You can immediately download preinstalled and optimized Operating System images from *Downloads* menu and use them, create templates and much more.

Please, read the rest of the documentation to enjoy all features.

