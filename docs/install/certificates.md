# Certificates

SSL certificates should be used to secure web access and connections to virtual desktops with viewers. 

IsardVDI will generate a default self signed generic certificate when installing from the first time if you don't configure letsencrypt parameters. That's why browsers will ask for certificate acceptance on first access to IsardVDI web...

If you configure letsencrypt domain parameters in isardvdi.cfg then it will generate one for you and autorenew it (remind that you should keep your server ports 80 and 443 open for external access through this domain). Also, if no certificate present it will generate a new self signed to make use of it by default. 

You can also use your own certificate (fullchain concatenated) by putting it in **/opt/isard/certs/defaul/custom-portal-chain.pem** or, in case of hypervisor flavour, at **/opt/isard/certs/defaul/custom-video-chain.pem**.

Spice certificates needs to be self-signed and are autogenerated and distributed automatically to auto-registered hypervisors. They are kept at **/opt/isard/certs/video** folder.

## Manage certificates

Certificates are stored in path **/opt/isard/certs/** where always will be the one self-signed or letsencrypt with name chain.pem.

If you are using self-signed or auto generated letsencrypt certificates you don't need to do anything else.

You can set your own certificate as said before. Follow this guide to generate your custom-xxxxx-chain.pem from your certificates.

### Custom Web certificate

To create your **custom-portal-chain.pem** (or custom-video-chain.pem if hypervisor flavour) you have to concatenate (in this order) the **server certificate** and **server-key**:

```
cat server-cert.pem server-key.pem > /opt/isard/certs/default/custom-portal-chain.pem
```

#### Custom commercial Web certificate

In case of a commercial certificate you should include de intermediate chain also in the fullchain custom-portal-chain.pem:

```
# domain.crt:       domain certificate
# intermediate.pem: intermediate certificate
# domain.key:       private key

cat domain.crt intermediate.pem domain.key > /opt/isard/certs/default/custom-portal-chain.pem
```

#### Custom letsencrypt Web certificate
In case of a letsencrypt certificate (not the one autogenerated by IsardVDI, one that you requested your own):

```
cat fullchain.pem privkey.pem > /opt/isard/certs/default/custom-portal-chain.pem
```

### Custom Video certificate

They are only needed if you have an infrastructure with hypervisor flavour servers.

Follow the same as for custom web certificates but issuing the **custom-video-chain.pem** in the same **/opt/isard/certs/default/** folder.


#### Examples with certificates from vendors

##### Commercial certificate

Put your concatenated certificate with correct name in **/opt/isard/certs/default/** (replace everythig that it is already in that folder).

Then restart portal (or video) container.

```
docker restart isard-portal
```

Now you may connect to IsardVDI server using the qualified CN as provided with your certificate.

NOTE: Wilcard certificates have been also validated with this procedure to be working as expected. See example below:

##### Example wildcard SSL Certificate

For example you have a wildcard commercial certificate from a company (let's say you bought *.isardvdi.com). You will get this files from your certificate provider:

- **wildcard_isardvdi_com.crt.pem**: Your wilcard certificate.
- **GandiStandardSSLCA2.pem**: This is the intermediate certificate from certificate authority (in this example is gandi.net the provider). You can always get this certificate by copying or exporting from your browser certificate settings or download it from their webpage (i.e. ```wget -q -O - https://www.gandi.net/static/CAs/GandiStandardSSLCA2.pem```)
- **wildcard_isardvdi_com.key.pem**: Your certificate private key.

We will need to transform this files into the chain:

```
cat wildcard_isardvdi_com.crt.pem GandiStandardSSLCA2.pem wildcard_isardvdi_com.key.pem > /opt/isard/certs/default/chain.pem
```

Then restart portal (or video) container.

```
docker restart isard-portal
```

## Reset certificates

If you replaced certificates and nothing worked it is recommended to start the proccess again by resetting certificates. If you manipulate certificates in the folder it could confuss IsardVDI certificate processing code.

You can always get your IsardVDI working again with self signed certificates by removing /opt/isard/certs/default folder. IsardVDI will generate and set up a new self signed certificate again. Procedure will be:

```bash
docker-compose down
rm -rf /opt/isard/certs/*
docker-compose up -d
```

You may have done a backup of your previously working self signed certificates and you could now also copy those ones in default certs folder instead of generating new ones.

## Troubleshoot certificates

Please refer to the  [admin faq about certificates](../admin/faq.md#certificates) section.
