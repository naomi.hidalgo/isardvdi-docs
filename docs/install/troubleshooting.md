# Troubleshooting Install

## docker-compose up -d refuses to start hypervisor

The may be two possible sources for this problem. One is the use of a service in your host that is on a port in the range of default ports used by IsardVDI and viewers. Those ports are:

- 80: Spice proxy SSL tunnel viewer port
- 443: Web browser and HTML4 viewer port.

You can check your listening ports by issuing the command **netstat -tulpn** and checking if any of your listening ports overlaps with IsardVDI port range. 

There is no easy solution to this without shutting down your service before starting IsardVDI.

## The hypervisor details say there is no virtualization available

Some CPUs (mostly old ones) don't have hardware virtualization, others have it but it is disabled in BIOS. In the first case there is nothing that can be done. If it is disabled in BIOS then you should check for VT-X or Virtualization or SVM and activate it.

[More info in admin faqs](../admin/faq.md#after-finishing-install-the-default-isard-hypervisor-is-disabled)