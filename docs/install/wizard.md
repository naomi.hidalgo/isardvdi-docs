# Wizard

Now in IsardVDI version 2 it is so simple and automated that there is even no wizard. Just bring up the project and wait one minute to let the database and webapp ready.

Connect with your browser to your IsardVDI server on  `https://<ip|dns>` and wait to be ready. (Please let a while to allow the system to populate initial database)

Note: If you didn't set up a letsencrypt certificate (in isardvdi.cfg) the browser will ask to to accept the self-signed generated certificate. Add an exception and add the certificate to trusted certificates and it will open IsardVDI webapp.

![](../images/wizard/firefox-untrusted.jpg)

The main login IsardVDI page will come up. Login with **admin** username and the password you set up in isardvdi.cfg (by default is **IsardVDI**).

![](../images/wizard/login.png)

Go now to [first steps](first-steps.md)
