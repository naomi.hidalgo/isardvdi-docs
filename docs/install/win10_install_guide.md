# Windows 10 installation guide

The intention of this guide is to show step by step how to create a windows 10 base image to create a template adapted to the IsardVDI desktop virtualization environment. 

The idea of ​​this document is to contribute and collect ideas about which elements of the windows to modify and customize for better performance.

## Pre-install windows 10

This first part explains where to find the iso images, the virtio drivers and how to configure the virtual desktop hardware.

### Windows ISO

Depending on the type of license and the language, we can download a generic windows 10 iso. In our case we want to license the windows with a Pro license. The versions of the windows iso are evolving, and from the microsoft website it is only available the last one. There is also the possibility of customizing your own windows iso or creating LTSC versions (in this case the license is enterprise) with microsoft tools.

Go to the [microsoft website](https://www.microsoft.com/es-es/software-download/windows10ISO) to download the iso and select the edition and language

![](./win10_install_guide.es.images/win17.png)

The page allows you to download the 64-bit version. As soon as the iso starts to download, cancel the download, since it only interests the download URL so that it can be downloaded within the isard directories where the iso are stored. Copy the download link, this link is valid only temporarily.

![](./win10_install_guide.es.images/win18.png)

Press the button ![](./win10_install_guide.es.images/win1.png) to go to the administration panel

![](./win10_install_guide.es.images/win2.png)

In the section ![](./win10_install_guide.es.images/win4.png)

![](./win10_install_guide.es.images/win3.png)

Press the button ![](./win10_install_guide.es.images/win5.png)

![](./win10_install_guide.es.images/win6.png)

A dialog box appears where you can copy the download url that has previously been copied, give it a name and select the ISO type. It can also be used to give permissions so that this iso can be used by users by displaying the "Allows" area:

![](./win10_install_guide.es.images/win7.png)

La imagen iso se queda en Status "Downloading" y se puede ir viendo el progreso de la descarga:

![](./win10_install_guide.es.images/win8.png)

### Windows VirtIO Drivers

While it is downloading, you can get the iso with the virtio drivers that we will need. The virtio drivers allow you to use paravirtualized or simulated devices that are part of the optimized hardware to improve the performance of KVM virtual machines. The virtio disk drivers stand out, more efficient than the SATA or IDE simulators, as well as the virtio network drivers, more efficient than the simulations of other cards.

The Fedora community, with their oVirt project as a reference, prepare and maintain an ISO image with a couple of windows installers that make it easy to install necessary software. It would be something similar to motherboard driver discs, which come with an installer to make it easy for you to manually install all available drivers. There are two versions available:

* [Stable](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/virtio-win.iso)
* [Latest](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso)

It is recommended to use the latest version and in case of any problem, use the stable version.

Also download this iso image in isard, following the same steps that have been carried out previously for the windows iso.

![](./win10_install_guide.es.images/win9.png)

## Create desktop to install from iso

To create a desktop from a "Media", press the button ![](./win10_install_guide.es.images/win10.png) from the iso:

![](./win10_install_guide.es.images/win11.png)

"Windows 10 UEFI and Virtio devices" is selected as the operating system template (which decides which hardware is recommended for this distribution). Give the desktop a name and define the capacities: 2vcpus, 6GB and 120GB disk for example...

![](./win10_install_guide.es.images/win12.png)

Once you have pressed the button "Create desktop", we go to the Desktops menu to see if it has already been created. Note, it only has one media icon ![](./win10_install_guide.es.images/win13.png)

![](./win10_install_guide.es.images/win19.png)

To display the options, press the icon ![](./win10_install_guide.es.images/win14.png), and press the button ![](./win10_install_guide.es.images/win15.png)

A dialog appears where Media can be displayed with the icon ![](./win10_install_guide.es.images/win16.png)

In the CD/DVD part, search for the word "virt" and select the virtio-win iso

![](./win10_install_guide.es.images/win20.png)

The two iso are selected in Media

![](./win10_install_guide.es.images/win21.png)

Verify that the boot option is on cd/dvd

Now we change the network type from **Default** to **Intel PRO/1000 isolated**. This type of Intel Pro network card, connected to an isolated network (can go to the internet but cannot see other desktops) will allow the windows installer to recognize that card and be able to access to the internet during installation. In case that we leave the default network, the installation is carried out in the same way, but it does not carry out certain updates or offer certain options during the installation process. We recommend changing to this type of hardware and later changing back to the **"Default"** network once the installation is complete, since with the default network a **"virtio"** hardware is used, which is more efficient and gives more performance than the Intel Pro 1000 card simulation.

![](./win10_install_guide.es.images/win22.png)

![](./win10_install_guide.es.images/win24.png)

Press the button ![](./win10_install_guide.es.images/win23.png) and if everything has gone well, the Start icon and two media icons appear in the list:

![](./win10_install_guide.es.images/win25.png)

Internally, the virtual desktop runs on the hypervisors from an xml that describes that virtual machine.
From the administration panel you can access this xml file and verify that certain options are selected. First, press the button ![](./win10_install_guide.es.images/win26.png) in the "Domains" section

![](./win10_install_guide.es.images/win27.png)

The desktop options are displayed by pressing the "-" icon and then press the button ![](./win10_install_guide.es.images/win28.png) 

![](./win10_install_guide.es.images/win29.png)

![](./win10_install_guide.es.images/win30.png)

**It is recommended to only visualize and have a clear knowledge of what you want to do if we touch this xml.**

The following sections are important:

* Machine type **q35** , is a better performing and modern hardware simulation type for windows 10

```xml
    <type arch="x86_64" machine="q35">hvm</type> 
```

* special options to simulate and manage interruptions, and interaction simulating the Microsoft HyperV hypervisor, improving the general performance of windows machines. With machine type q35 these options are required

```xml
      <hyperv>
        <relaxed state="on"/>
        <vapic state="on"/>
        <spinlocks state="on" retries="8191"/>
      </hyperv>
```
  
* UEFI boot. Necessary for a correct installation of windows 10 and detection of the virtio disk drivers during the start of the installation. The UEFI loader is used to simulate UEFI, and you have to tell it in which path the UEFI boot code is located. The line of the xml that activates the UEFI is:

```xml
        <loader readonly="yes" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
```

Integrated in the os definition area in the xml:

```xml
      <os>
        <type arch="x86_64" machine="q35">hvm</type>
        <boot dev="cdrom"/>
        <loader readonly="yes" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
      </os>
```

The first time you boot with uefi, since there is nothing installed and the cd boot menu has almost no timeout, there is no operating system and it stays in this point:

![](./win10_install_guide.es.images/win31.png)

Force the restart by sending the combination CTRL+ALT+DEL. This part of sending this combination can be done within the remote-viewer in the "Send key" menu:

![](./win10_install_guide.es.images/win32.png)

You can see the message that asks us to boot from cd. Within the remote-viewer, press any key

![](./win10_install_guide.es.images/win33.png)

The icon of the tiano core appears on the screen:

![](./win10_install_guide.es.images/win34.png)

And the first windows installation screen:

![](./win10_install_guide.es.images/win35.png)


## Windows Installation

Next press the "Next" button on this first screen of the language section:

![](./win10_install_guide.es.images/win36.png)

Install now:

![](./win10_install_guide.es.images/win37.png)

In the next dialog press **"I don't have a product key"**

![](./win10_install_guide.es.images/win38.png)

Select the type of windows that you want to install, in our case **Windows 10 Pro**

![](./win10_install_guide.es.images/win39.png)

Accept the license terms:

![](./win10_install_guide.es.images/win40.png)

In the type of installation, select the custom option:

![](./win10_install_guide.es.images/win41.png)

The virtio driver is loaded on the **"Load driver**" button:

![](./win10_install_guide.es.images/win42.png)

Press browse and select the virtio cdrom, viostor directory, w10 directory and amd64:

![](./win10_install_guide.es.images/win43.png)

Select the controller:

![](./win10_install_guide.es.images/win44.png)

And the disk is detected so that it can be installed:

![](./win10_install_guide.es.images/win45.png)

Press the button "Next" and the installation of the operating system starts:

![](./win10_install_guide.es.images/win46.png)

Once it finishes, the virtual desktop is restarted:

![](./win10_install_guide.es.images/win47.png)

Try to boot from cdrom and then UEFI gives an error:

![](./win10_install_guide.es.images/win48.png)

Now from the Isard web application the stop the desktop and change the boot device. Press the button ![](./win10_install_guide.es.images/win49.png)

![](./win10_install_guide.es.images/win50.png)

The options are displayed when pressing the icon "-" and then press the **Edit** button 

![](./win10_install_guide.es.images/win51.png)

Change the device from **Boot** to **Hard Disk** and modify the desktop

![](./win10_install_guide.es.images/win52.png)

Restart the desktop, press the "Start" button:

![](./win10_install_guide.es.images/win53.png)

Connect to the desktop and it boots fine from disk, messages appear on the screen like this:

![](./win10_install_guide.es.images/win54.png)

It ends with a new restart and a set of dialogs appears where you usually select NO to what it proposes:

![](./win10_install_guide.es.images/win55.png)

![](./win10_install_guide.es.images/win56.png)

If the network with Intel card model has been selected, a screen like this will appear, searching for updates over the internet:

![](./win10_install_guide.es.images/win57.png)

If the intel card is not selected, since it does not have the network drivers it cannot access the internet, it tells us that we do not have internet when it proposes to connect to a network:

![](./win10_install_guide.es.images/win58.png)

![](./win10_install_guide.es.images/win59.png)

and in the following dialog select: **Continue with limited configuration**

Then, if you had a network, you can configure it for personal use or for an organization. In our case it has been configured for personal use:

![](./win10_install_guide.es.images/win60.png)

And now to add an account select **Offline account**

![](./win10_install_guide.es.images/win61.png)

And then select **Limited experience**:

![](./win10_install_guide.es.images/win62.png)

Being a generic image, you have to choose a username and password that is easy to remember. In Isard it is usual for the base images to come with the following username and password that we will configure

* user: isard
* passowrd: pirineus

![](./win10_install_guide.es.images/win63.png)

![](./win10_install_guide.es.images/win64.png)

Fill out the security questions with random answers:

![](./win10_install_guide.es.images/win65.png)

Now choose to say no to what is being proposed:

![](./win10_install_guide.es.images/win66.png)

![](./win10_install_guide.es.images/win67.png)

![](./win10_install_guide.es.images/win68.png)

![](./win10_install_guide.es.images/win69.png)

![](./win10_install_guide.es.images/win70.png)

![](./win10_install_guide.es.images/win71.png)

![](./win10_install_guide.es.images/win72.png)

Cortana assistant is not wanted:

![](./win10_install_guide.es.images/win73.png)

![](./win10_install_guide.es.images/win74.png)

Finally, it starts giving welcome messages:

![](./win10_install_guide.es.images/win75.png)

And the windows desktop appears for the first time:

![](./win10_install_guide.es.images/win76.png)

Now you have windows 10 with a working desktop. The next step is to install the drivers. For this, it is important to change the type of network card to virtio, since this hardware is optimized and is recommended when working with a virtual machine that runs on KVM, as it is our case. For this, you have to shut down the desktop, modify the hardware, and reboot the desktop.

![](./win10_install_guide.es.images/win77.png)

Now edit the desktop by pressing the icon "-", press the **Edit** button and select the **Default** network so that it catches the virtio network drivers:

![](./win10_install_guide.es.images/win78.png)

![](./win10_install_guide.es.images/win79.png)

Start the desktop again and connect via spice with remote-viewer:

Open the file explorer, select the drive E: and start the executable virtio-win-gt-x64 first and then virtio-guest-tools:

![](./win10_install_guide.es.images/win80.png)

Driver installer for windows Virtio-win:

![](./win10_install_guide.es.images/win81.png)

Press the "Next" button:

![](./win10_install_guide.es.images/win82.png)

Press on the "Yes" button that you want to allow to install:

![](./win10_install_guide.es.images/win83.png)

Trust the publisher to be Red Hat and "Install":

![](./win10_install_guide.es.images/win84.png)

Now the win-guest-tools, that will allow among other things, the automatic rescaling of the screen resolution.

![](./win10_install_guide.es.images/win85.png)

When you install the spice agent it automatically rescales.

Manual shutdown the desktop and remove the cds from the isard with the desktop turned off:

![](./win10_install_guide.es.images/win77.png)

To delete the isos, press the "X" icon of the two:

![](./win10_install_guide.es.images/win86.png)

Remaining in the desktop list without icons in the "Media" column:

![](./win10_install_guide.es.images/win87.png)

Restart the desktop and connect with the remote-viewer viewer.

Updates are then installed by typing **"windows update"** in the search bar on the taskbar:

![](./win10_install_guide.es.images/win88.png)

Press **Check for updates**:

![](./win10_install_guide.es.images/win89.png)

It finds some updates and gets ready to install them

![](./win10_install_guide.es.images/win90.png)

A reboot to the desktop is performed from within windows itself, it takes time to restart when applying the updates:

![](./win10_install_guide.es.images/win91.png)  ![](./win10_install_guide.es.images/win92.png)

### Create updated windows template with virtio drivers installed.

Once the system is updated and a reboot has been performed to verify that the updates have been correctly installed, it is recommended to make a template to save all the work done and to be able to return to this point of the installation if we need it in the future. Having a template with what has been done so far can save a lot of time to build a new template with optimizations and software in the future.

Templates can only be made with the desktop turned off. So the first thing is to shut down the windows in an orderly way:

![](./win10_install_guide.es.images/win93.png)

Now from desktops, with the expanded options displayed by pressing the "-" icon on our desktop:

Once it has restarted, it shuts down and the template can be created. In the desktop row with the machine turned off, press the "**Template it**" button:

![](./win10_install_guide.es.images/win94.png)

Fill the camps such as "Template name" and the template is created

![](./win10_install_guide.es.images/win95.png)

From now on, the template will appear in the list of templates, and our desktop will derive from this template that we just created. We can continue working with the desktop at the point where it has been left off and also create new desktops from that template.


## Software and Windows optimizations

### Modify language and replicate to all users

In case you want to change the windows language (for example to Catalan) it is important to make the change now since certain software will look for the user's language to install the correct language version.

Go to **settings => time and language => language**

Now add a language:

![](./win10_install_guide.es.images/win96.png)

![](./win10_install_guide.es.images/win97.png)

Select **"Set as windows display language"** and press the "Install" button:

![](./win10_install_guide.es.images/win98.png)

It takes a few minutes for the download bar to appear:

![](./win10_install_guide.es.images/win99.png)

It asks if you want to close the session now,press accept, the session is closed and the session is started again, Catalan appears in the windows task bar:

![](./win10_install_guide.es.images/win100.png)

#### Set default language for all users

Go to **Start -> settings -> Time and language -> Language**

Now you can remove the Spanish language if you wish or leave both languages ​​in the isard user.

Under preferred languages ​​there is the option of **Administrative language settings**

![](./win10_install_guide.es.images/win101.png)

A dialogue window opens:

![](./win10_install_guide.es.images/win102.png)

Press on "change the regional parameters of the system" and change to "Català"

And now the most important part so that new users also have the new default language. Press on **Copy settings** and select **Welcome screen and system accounts** and **New user accounts**. There is a configuration like this:

![](./win10_install_guide.es.images/win103.png)

Apply the changes and restart:

![](./win10_install_guide.es.images/win104.png)

There may still be traces of the original language (Spanish in this case) in the current user, the user account would have to be deleted and recreated so that when the account is remade again it takes all the language options correctly. That is why it is important to change the language before creating the admin and user users, which are the ones that will be given by default in the windows base template.


## Adapt windows for virtualized environment

In a virtualized environment, we want the resources used by windows to be minimal at the level of disk writing, cpu and ram use. The less impact on these factors we have, the more simultaneous desktops can be run on a server. Reducing processes that are not typically used by the user in an educational or business environment will also promote faster system startup and greater agility. The challenge is to find the balance between restricting functionalities and not penalizing the user experience.

This guide proposes different tools and techniques to optimize installations and improve the user experience.


### Install firefox

Firefox is downloaded and installed from the [mozilla website](https://www.mozilla.org/es-ES/firefox/new/)

Firefox starts for the first time. It shuts down and restarts. It will ask us if we want it to be our default browser:

![](./win10_install_guide.es.images/win105.png)

We tell it that it is wanted and the default browser is changed:

![](./win10_install_guide.es.images/win106.png)

And it is default:

![](./win10_install_guide.es.images/win107.png)

### Uninstall Microsoft Edge

Microsoft Edge is based on the Chromium project, it can be left in the system, but if you want to remove it from the operating system, you can follow the steps below. Open a console: press the keys **Windows + R** and type **cmd**

In the console:

```bat
cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\8*\Installer
setup --uninstall --force-uninstall --system-level
```

### Autologon

To facilitate restarts and for the time being, as we will work with a single administrator user to carry out installations and prepare the template, it is recommended to install a utility to carry out an autologon so it does not ask us for a username and password when Windows starts. You have to search for "autologon windows sysinternals" and reach the sysinternals website from where you download the utility. The link where to find the utility is:[Autologon](https://docs.microsoft.com/en-us/sysinternals/downloads/autologon)

![](./win10_install_guide.es.images/win108.png)

Next, download the zip file that they provide us, open and decompress the file **Autologon64**:

![](./win10_install_guide.es.images/win109.png) 

![](./win10_install_guide.es.images/win110.png)

Execute Autologon64 and enter the password of the isard account: **pirineus**

![](./win10_install_guide.es.images/win111.png)

It informs us that the password is encrypted:

![](./win10_install_guide.es.images/win112.png)

Restart to verify that it no longer asks for a username and password when entering:

![](./win10_install_guide.es.images/win113.png)


### Create admin user

An admin account will allow us to work as administrators in case we want to convert the "isard" user account to a normal user.

From powershell as administrator (right click on the windows icon):

![](./win10_install_guide.es.images/win114.png)

```
# Save the password in a variable
$Password = Read-Host -AsSecureString
```
And once the password has been entered: ***pirineus***

```powershell
# Create admin user from administrators group
New-LocalUser "admin" -Password $Password -FullName "admin"
Add-LocalGroupMember -Group "Administradores" -Member "admin"
```

And the user "user" that will not be an administrator:

```
New-LocalUser "user" -Password $Password -FullName "user"
Add-LocalGroupMember -Group "Usuarios" -Member "user"
```


### Directory c:\admin

The directory c:\admin is also created where programs or administration information will be saved. Remove user permissions, so that only administrators can access:

![](./win10_install_guide.es.images/win115.png)

And only the permission for the administrators group is left:

![](./win10_install_guide.es.images/win116.png)


### Uninstall Microsoft Edge

Microsoft Edge is based on the Chromium project, it can be left in the system, but if you want to remove it from the operating system, you can follow the steps below. Open a console: Press the keys **Windows + R** and type **cmd**

In the console:

```bat
cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\8*\Installer
setup --uninstall --force-uninstall --system-level
```

### Uninstall other Microsoft applications

Open a powershell console as administrator:

- Right click on the windows icon in the start bar and select **Windows PowerShell (Admin)**

![](./win10_install_guide.es.images/win117.png)

To check all installed programs:

```
Get-AppxPackage | Select Name
```

And now delete all these:

```
Microsoft.Xbox*
Microsoft.Bing*
Microsoft.3DBuilder
Microsoft.Advertising.Xaml
Microsoft.AsyncTextService
Microsoft.BingWeather
Microsoft.BioEnrollment
Microsoft.DesktopAppInstaller
Microsoft.GetHelp
Microsoft.Getstarted
Microsoft.Microsoft3DViewer
Microsoft.MicrosoftEdge
Microsoft.MicrosoftEdgeDevToolsClient
Microsoft.MicrosoftOfficeHub
Microsoft.MicrosoftSolitaireCollection
Microsoft.MicrosoftStickyNotes
Microsoft.MixedReality.Portal
Microsoft.Office.OneNote
Microsoft.People
Microsoft.ScreenSketch
Microsoft.Services.Store.Engagement
Microsoft.Services.Store.Engagement
Microsoft.SkypeApp
Microsoft.StorePurchaseApp
Microsoft.Wallet
Microsoft.WindowsAlarms
Microsoft.WindowsCamera
Microsoft.WindowsFeedbackHub
Microsoft.WindowsMaps
Microsoft.WindowsSoundRecorder
Microsoft.WindowsStore
Microsoft.XboxGameCallableUI
Microsoft.YourPhone
Microsoft.ZuneMusic
Microsoft.ZuneVideo
SpotifyAB.SpotifyMusic
Windows.CBSPreview
microsoft.windowscommunicationsapps
windows.immersivecontrolpanel
```

Now copy and paste into powershell terminal:

```
Get-AppxPackage -Name Microsoft.Xbox* | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Bing* | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.3DBuilder | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Advertising.Xaml | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.AsyncTextService | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.BingWeather | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.BioEnrollment | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.DesktopAppInstaller | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.GetHelp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Getstarted | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Microsoft3DViewer | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftEdge | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftEdgeDevToolsClient | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftStickyNotes | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MixedReality.Portal | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Office.OneNote | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.People | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ScreenSketch | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.SkypeApp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.StorePurchaseApp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Wallet | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsAlarms | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsCamera | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsFeedbackHub | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsMaps | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsSoundRecorder | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.XboxGameCallableUI | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.YourPhone | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ZuneMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ZuneVideo | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name SpotifyAB.SpotifyMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Windows.CBSPreview | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name microsoft.windowscommunicationsapps | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name windows.immersivecontrolpanel | Remove-AppxPackage -ErrorAction SilentlyContinue
```

### Simple program bar

All Productivity and Explore items can be removed from the launch bar.

Right click on items and **Unpin from Start**

![](./win10_install_guide.es.images/win118.png)


### Disable services

Run "**msconfig**" and disable services:

- Xbox Live Authentication Manager
- Security Center
- Windows Defender Firewall
- Mozilla Maintenance Service
- Microsoft Defender Antivirus Service
- Windows Update Medic service
- Windows Update

In Windows Start -> go to task manager and disable:

- Microsoft OneDrive

- Windows Security notification

  ![](./win10_install_guide.es.images/win119.png)
  
  Se reiniciar para aplicar los cambios
  
  ![](./win10_install_guide.es.images/win120.png)
  

### Review admin and user desktops

Log in and check that the desktop icons, taskbar icons and start menu are fine... Remove windows programs again...

To remove notifications:

![](./win10_install_guide.es.images/win121.png)


### Install free software

W choose to install the following software:

- Libre Office

- Gimp

- Inkscape

- LibreCAD

- Geany

![](./win10_install_guide.es.images/win122.png)

### Non-free software applications

The following applications are installed:

- Google Chrome
- Adobe Acrobat Reader

Update Service is disabled:

![](./win10_install_guide.es.images/win123.png)

Google Update is disabled:

![](./win10_install_guide.es.images/win124.png)

A very useful tool for preparing templates is the **VMware OS Optimization Tool**

It can be downloaded from: [VMware OS Optimization tool](https://flings.vmware.com/vmware-os-optimization-tool)

It is left in c:\admin\VMwareOSOptimizationTool_b2000_17205301.zip

Before passing this tool it is recommended to make a template. If the optimization tool makes too many modifications that interfere with any of the applications that you have installed, you can return to this point and perform the optimization later.

## Vmware OS optimization tool

Decompress the zip file that has been stored in c:\admin and start the utility: VMwareOSOptimizationTool

![](./win10_install_guide.es.images/win125.png)

Once the application starts, press the **Analyze** button:

![](./win10_install_guide.es.images/win126.png)

And then to "Common options", where in Visual Effect select "Best quality" and pay attention to the "Disable hardware acceleration" check:

![](./win10_install_guide.es.images/win127.png)

In Store Apps leave "Calculator"

![](./win10_install_guide.es.images/win128.png)

And press the "Optimize" button, when it is finished, save the result with the "export" button in c:\admin and then restart it.
