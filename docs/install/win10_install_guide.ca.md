# Guia d'instal·lació Windows 10

La intenció d'aquesta guia és mostrar pas a pas com crear una imatge base de windows 10 per a crear una plantilla adaptada a l'entorn de virtualització d'escriptoris de IsardVDI.

La idea d'aquest document és aportar i recollir idees sobre quins elements del windows modificar i personalitzar per a un millor rendiment.

## Pre-instal·lació windows 10

Aquesta primera part explica on s'ha de buscar les imatges iso, els drivers de virtio i com configurar el maquinari de l'escriptori virtual.

### ISO de windows

En funció del tipus de llicència i de l'idioma podem descarregar una iso genèrica de windows 10. En el nostre cas volem llicenciar el windows amb una llicència Pro. Les versions de les iso de windows van evolucionant, i des de la pàgina web de microsoft aquesta disponible solo l'última. També existeix la possibilitat de personalitzar la teva pròpia iso de windows o crear versions LTSC (en aquest cas la llicència és enterprise) amb eines de microsoft.

Es va a la [pàgina web de microsoft](https://www.microsoft.com/es-es/software-download/windows10iso) per a descarregar la iso i se selecciona l'edició i l'idioma

![](./win10_install_guide.es.images/win17.png)

La pàgina permet descarregar la versió de 64 bits. Quan es comenci a descarregar la iso, es cancel·la la descàrrega, ja que només interessa l'enllaç de la descàrrega perquè la descarregui dins dels directoris de isard on s'emmagatzemen les iso. Es copia l'enllaç de descàrrega, aquest enllaç és vàlid solo temporalment.

![](./win10_install_guide.es.images/win18.png)

Es prem el botó ![](./win10_install_guide.es.images/win1.png) per a anar al panell d'administració

![](./win10_install_guide.es.images/win2.png)

En l'apartat de ![](./win10_install_guide.es.images/win4.png)

![](./win10_install_guide.es.images/win3.png)

Es prem el botó ![](./win10_install_guide.es.images/win5.png)

![](./win10_install_guide.es.images/win6.png)

Apareix un quadre de diàleg on es pot copiar l'enllaç de descàrrega que prèviament s'ha copiat, se li dona un nom i se selecciona el tipus ISO. També es pot aprofitar per a donar permisos perquè aquesta iso la puguin usar els usuaris desplegant la zona de "Allows":

![](./win10_install_guide.es.images/win7.png)

La imatge iso es queda en Estatus "Downloading" i es pot anar veient el progrés de la descàrrega:

![](./win10_install_guide.es.images/win8.png)

### Windows VirtIO Drivers

Mentre es descarrega, es pot anar a buscar la iso amb els drivers virtio que necessitarem. Els drivers virtio permeten utilitzar dispositius paravirtualitzats o simulats que formen part del maquinari optimitzat per a millorar el rendiment de les màquines virtuals de KVM. Destaquen els drivers virtio de disc, més eficients que els simuladors de SATA o IDE, així com els drivers de xarxa virtio, més eficient que les simulacions d'altres targetes.

La comunitat de Fedora, amb el seu projecte oVirt com a referència, preparen i mantenen una imatge ISO amb un parell d'instal·ladors de windows que faciliten la instal·lació de programari necessari. Seria alguna cosa semblant als discos de drivers de les plaques base, que venen amb un instal·lador per a facilitar-te la instal·lació manual de tots els drivers disponibles. Hi ha dues versions disponibles:

* [Stable](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/virtio-win.iso)
* [Latest](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso)

Es recomana utilitzar la versió latest i en cas de donar algun problema, anar a la versió stable.

Es descarrega també aquesta imatge iso en isard, seguint els mateixos passos que s'han realitzat anteriorment per a la iso de windows.

![](./win10_install_guide.es.images/win9.png)

## Crear escriptori per a instal·lar des de iso

Per a crear un escriptori des d'un "Mitjana", es prem el botó ![](./win10_install_guide.es.images/win10.png) des de la iso:

![](./win10_install_guide.es.images/win11.png)

Se selecciona com a plantilla de sistema operatiu (la que decideix que maquinari és l'aconsellable per a aquesta distribució) "windows 10 UEFI and Virtio devices". Se li dona un nom a l'escriptori, i es defineix les seves capacitats: 2vcpus, 6GB i disc de 120GB per exemple...

![](./win10_install_guide.es.images/win12.png)

Una vegada s'ha premut en "Create desktop", anem al menú Desktops a veure si ja s'ha creat. Observem que només té una icona de mitjana ![](./win10_install_guide.es.images/win13.png)

![](./win10_install_guide.es.images/win19.png)

Per a desplegar les opcions, es prem la icona ![](./win10_install_guide.es.images/win14.png), i es prem el botó ![](./win10_install_guide.es.images/win15.png)

Apareix un quadre de diàleg on es pot desplegar Mitjana amb la icona ![](./win10_install_guide.es.images/win16.png)

En la part de CD/DVD, es busca la paraula "virt" i se selecciona la iso de virtio-win

![](./win10_install_guide.es.images/win20.png)

Queden les dues iso seleccionades en Media

![](./win10_install_guide.es.images/win21.png)

Es verifica que l'opció de boot està en cd/dvd

Ara canviem el tipus de xarxa de **Default** a **Intel PRO/1000 isolated**. Aquest tipus de targeta de xarxa Intel Pro, connectada a una xarxa isolated (pot sortir a internet però no pot veure a altres escriptoris) permetrà que l'instal·lador de windows reconegui aquesta targeta i pugui sortir a internet durant la instal·lació. En cas que deixem la xarxa default la instal·lació es realitza igualment però no realitza unes certes actualitzacions ni ofereix unes certes opcions durant el procés d'instal·lació. Recomanem canviar a aquesta mena de maquinari i tornar a modificar posteriorment a la xarxa **"Default"** una vegada hagi finalitzat la instal·lació, ja que amb la xarxa default s'utilitza un maquinari "**virtio**" que és més eficient i dona més rendiment que la simulació de targeta Intel Pro 1000. 

![](./win10_install_guide.es.images/win22.png)

![](./win10_install_guide.es.images/win24.png)

Es prem el botó ![](./win10_install_guide.es.images/win23.png) i si tot ha anat bé apareix la icona de Start i dues icones de mitjana en el llistat:

![](./win10_install_guide.es.images/win25.png)

Internament l'escriptori virtual corre en els hypervisors a partir d'un xml que descriu aquesta màquina virtual.
Des del panell d'administració es pot accedir a aquest fitxer xml i verificar que unes certes opcions estan seleccionades. Primer es prem el botó ![](./win10_install_guide.es.images/win26.png) dins de l'apartat de "Domains"

![](./win10_install_guide.es.images/win27.png)

Es desplega les opcions del desktop i es prem el botó ![](./win10_install_guide.es.images/win28.png).

![](./win10_install_guide.es.images/win29.png)

![](./win10_install_guide.es.images/win30.png)

**Es recomana només visualitzar i tenir un coneixement clar del que es vol fer si toquem aquest xml.**

Importants les següents seccions:

* Tipus de màquina **q35** , és un tipus de simulació de maquinari moderna i amb millor rendiment per al windows 10

```xml
    <type arch="x86_64" machine="q35">hvm</type> 
```

* opcions especials per a simular i gestionar interrupcions i interacció simulant al hypervisor de Microsoft HyperV, millorant el rendiment general de la màquines de windows. Amb la mena de màquina q35 són necessàries aquestes opcions

```xml
      <hyperv>
        <relaxed state="on"/>
        <vapic state="on"/>
        <spinlocks state="on" retries="8191"/>
      </hyperv>
```
  
* Arrencada UEFI. Necessari per a una correcta instal·lació de windows 10 i detecció dels drivers virtio de disc durant l'inici de la instal·lació. S'usa per a simular UEFI el loader de UEFI, i cal indicar-li en què path té aquest codi d'arrencada del UEFI. La línia del xml que activa el UEFI és:

```xml
        <loader readonly="yes" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
```

Integrat en la zona de definició d'us en el xml:

```xml
      <os>
        <type arch="x86_64" machine="q35">hvm</type>
        <boot dev="cdrom"/>
        <loader readonly="yes" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
      </os>
```


La primera vegada que s'arrenca amb uefi, com no hi ha res instal·lat i el menú d'arrencada per cd gairebé no té timeout, no hi ha sistema operatiu i es queda en aquest punt:

![](./win10_install_guide.es.images/win31.png)

Es força el reinici enviant la combinació CTRL+ALT+SUPR. Aquesta part d'enviar aquesta combinació es pot fer dins del visor remote-viewer en el menú "Send key":

![](./win10_install_guide.es.images/win32.png)

Es pot veure el missatge que ens demana arrencar per cd. Dins del remote-viewer, es prem una tecla

![](./win10_install_guide.es.images/win33.png)

Apareix la icona en pantalla del tiano core:

![](./win10_install_guide.es.images/win34.png)

I la primera pantalla d'instal·lació del windows:

![](./win10_install_guide.es.images/win35.png)


## Instal·lació de windows

Es prem "següent" d'aquesta primera pantalla de secció d'idiomes:

![](./win10_install_guide.es.images/win36.png)

Instal·lar ara:

![](./win10_install_guide.es.images/win37.png)

En el següent diàleg es prem **"No tinc clau de producte"**

![](./win10_install_guide.es.images/win38.png)

Se selecciona el tipus de windows que es vol instal·lar, en el nostre cas el **Windows 10 Pro**

![](./win10_install_guide.es.images/win39.png)

S'accepta els termes de llicència:

![](./win10_install_guide.es.images/win40.png)

En el tipus d'instal·lació se selecciona l'opció personalitzada:

![](./win10_install_guide.es.images/win41.png)

Es carrega el controlador de virtio en el botó **"Carregar contr."**:

![](./win10_install_guide.es.images/win42.png)

Es prem examinar i se selecciona el cd-rom de virtio, directori viostor, directori w10 i amd64:

![](./win10_install_guide.es.images/win43.png)

Se selecciona el controlador:

![](./win10_install_guide.es.images/win44.png)

I es detecta el disc perquè es pugui instal·lar:

![](./win10_install_guide.es.images/win45.png)

Es prem "Següent" i s'inicia la instal·lació del sistema operatiu:

![](./win10_install_guide.es.images/win46.png)

Una vegada que acaba, es reinicia l'escriptori virtual:

![](./win10_install_guide.es.images/win47.png)

Intenta arrencar per cd-rom i després UEFI dona un error:

![](./win10_install_guide.es.images/win48.png)

Ara des de l'aplicació web de Isard es deté la màquina i es canvia el dispositiu d'arrencada. Es prem el botó ![](./win10_install_guide.es.images/win49.png)

![](./win10_install_guide.es.images/win50.png)

Es desplega les opcions i es prem el botó de **Edit** 

![](./win10_install_guide.es.images/win51.png)

Es canvia el dispositiu de **Boot** a **Hard Disk** i es modifica el escriptori

![](./win10_install_guide.es.images/win52.png)

Es torna a arrencar l'escriptori, es prem el botó "Start":

![](./win10_install_guide.es.images/win53.png)

Em connecto a l'escriptori i arrenca bé des de disc, apareixen missatges per pantalla com aquest:

![](./win10_install_guide.es.images/win54.png)

S'acaba amb un nou reinici i apareix un conjunt de diàlegs on se selecciona habitualment NO al que proposa:

![](./win10_install_guide.es.images/win55.png)

![](./win10_install_guide.es.images/win56.png)

Si s'ha seleccionat la xarxa amb model de targeta Intel, apareixerà una pantalla com aquesta que busca actualitzacions per internet:

![](./win10_install_guide.es.images/win57.png)

Si no se selecciona la targeta intel, com no té els drivers de xarxa no pot accedir a internet, se li diu que no tenim internet quan proposi connectar-nos a una xarxa:

![](./win10_install_guide.es.images/win58.png)

![](./win10_install_guide.es.images/win59.png)

i en el següent diàleg se selecciona: **Continuar amb la configuració limitada**

A continuació, si es tenia xarxa, permet realitzar la configuració per a ús personal o per a una organització. En el nostre cas s'ha configurat per a ús personal:

![](./win10_install_guide.es.images/win60.png)

I ara per a afegir un compte se selecciona **Compte sense connexió**

![](./win10_install_guide.es.images/win61.png)

I posteriorment se selecciona **Experiència limitada**:

![](./win10_install_guide.es.images/win62.png)

Al ser una imatge genèrica s'ha de posar un usuari i contrasenya que sigui fàcil de recordar. En Isard és habitual que les imatges basi vinguin amb el següent usuari i contrasenya que configurarem

* usuari: isard
* contrasenya: pirineus

![](./win10_install_guide.es.images/win63.png)

![](./win10_install_guide.es.images/win64.png)

S'emplena les preguntes de seguretat amb respostes aleatòries:

![](./win10_install_guide.es.images/win65.png)

Ara s'opta per dir-li que no al que es vagi proposant:

![](./win10_install_guide.es.images/win66.png)

![](./win10_install_guide.es.images/win67.png)

![](./win10_install_guide.es.images/win68.png)

![](./win10_install_guide.es.images/win69.png)

![](./win10_install_guide.es.images/win70.png)

![](./win10_install_guide.es.images/win71.png)

![](./win10_install_guide.es.images/win72.png)

No es vol l'assistent Cortana:

![](./win10_install_guide.es.images/win73.png)

![](./win10_install_guide.es.images/win74.png)

Finalment, comença a donar missatges de benvinguda:

![](./win10_install_guide.es.images/win75.png)

I apareix l'escriptori de windows per primera vegada:

![](./win10_install_guide.es.images/win76.png)

Ara ja es té el windows 10 amb un escriptori de treball. El següent pas és instal·lar els drivers. Per a això és important canviar el tipus de targeta de xarxa a virtio, ja que aquest maquinari està optimitzat i és el recomanat en treballar amb una màquina virtual que corre sobre KVM com és el nostre cas. Per tant s'apaga, es modifica el maquinari i es torna a arrencar l'escriptori.

![](./win10_install_guide.es.images/win77.png)

Ara s'edita l'escriptori desplegant les opcions i es prem el botó **Edit** i se selecciona la xarxa **Default** perquè detecti els drivers virtio de xarxa:

![](./win10_install_guide.es.images/win78.png)

![](./win10_install_guide.es.images/win79.png)

Es torna a iniciar l'escriptori i es connecta per spice amb remote-viewer:

S'obre l'explorador de fitxers, se selecciona la unitat E: i s'arrenca primer l'executable virtio-win-gt-x64 i després virtio-guest-tools:

![](./win10_install_guide.es.images/win80.png)

Instal·lador de drivers per a windows Virtio-win:

![](./win10_install_guide.es.images/win81.png)

Es prem el botó de "Següent":

![](./win10_install_guide.es.images/win82.png)

Es prem el botó de "Si" al fet que es vol permetre que instal·li:

![](./win10_install_guide.es.images/win83.png)

Es confia que l'editor és Red Hat i "Instal·lar":

![](./win10_install_guide.es.images/win84.png)

Ara les win-guest-tools que permetran entre altres coses, el reescalament automàtic de la resolució de pantalla.

![](./win10_install_guide.es.images/win85.png)

En instal·lar l'agent de spice ja reescala automàticament.

Es fa un apagat manual i es treu els cds del isard amb l'escriptori apagat:

![](./win10_install_guide.es.images/win77.png)

Per a eliminar les isos, es prem la icona "X" de les dues:

![](./win10_install_guide.es.images/win86.png)

Quedant en el llistat de l'escriptori sense icones en la columna "Media":

![](./win10_install_guide.es.images/win87.png)

Es torna a arrencar l'escriptori i a connectar-nos amb el visor remote-viewer.

A continuació s'instal·la actualitzacions, s'escriu **"windows update"** en el cercador de la barra de tasques:

![](./win10_install_guide.es.images/win88.png)

Es prem **Buscar actualitzacions**:

![](./win10_install_guide.es.images/win89.png)

Troba algunes actualitzacions i es disposa a instal·lar-les

![](./win10_install_guide.es.images/win90.png)

Es realitza un reboot a l'escriptori des de dins del propi windows, triga a reiniciar en aplicar les actualitzacions:

![](./win10_install_guide.es.images/win91.png)  ![](./win10_install_guide.es.images/win92.png)

### Crear plantilla de windows actualitzat i amb drivers virtio instal·lats.

Una vegada es té el sistema actualitzat i s'ha realitzat un reinici per a verificar que les actualitzacions han quedat ben instal·lades, es recomana fer una plantilla per a guardar tot el treball fet i poder tornar a aquest punt de la instal·lació si el necessitem en un futur. Tenir una plantilla amb el realitzat fins al moment pot estalviar molt de temps per a crear una nova plantilla amb optimitzacions i programari en un futur.

Les plantilles només es poden fer amb l'escriptori apagat. Pel que el primer és apagar de forma ordenada el windows:

![](./win10_install_guide.es.images/win93.png)

Ara des "desktops", amb les opcions ampliades desplegades en el nostre escriptori:

Una vegada s'hagi reiniciat, s'apaga de forma ordenada i es crea la plantilla. En la fila de l'escriptori amb la màquina apagada es prem al botó "**Template it**":

![](./win10_install_guide.es.images/win94.png)

I es crea la plantilla

![](./win10_install_guide.es.images/win95.png)

A partir d'ara, la plantilla apareixerà en el llistat de templates, i el nostre escriptori derivarà d'aquesta plantilla que acabem de crear. Podem continuar treballant amb l'escriptori en el punt on s'ha deixat i també crear nous escriptoris a partir d'aquesta plantilla.


## Programari i optimitzacions de Windows

### Modificar l'idioma i replicar a tots els usuaris

En cas que es vulgui modificar l'idioma de windows (per exemple a català) és important fer el canvi ara ja que cert programari buscarà l'idioma de l'usuari per a instal·lar la versió correcta d'idioma.

Es va a **configuració => hora i idioma => idioma**

Ara s'afegeix un idioma:

![](./win10_install_guide.es.images/win96.png)

![](./win10_install_guide.es.images/win97.png)

Se selecciona **"Establir com a idioma per a mostrar de windows"** i es prem el botó "Instal·lar":

![](./win10_install_guide.es.images/win98.png)

Triga uns minuts a aparèixer la barra de descàrrega:

![](./win10_install_guide.es.images/win99.png)

Demana si es vol tancar sessió ara, s'accepta, es tanca sessió i es torna a iniciar sessió, apareix el català en la barra de tasques de windows:

![](./win10_install_guide.es.images/win100.png)

#### Configurar idioma per defecte per a tots els usuaris

Es va a **Inici -> configuració -> Hora i idioma -> Idioma**

Ara es pot treure l'idioma Espanyol si es desitja o bé deixar els dos idiomes en l'usuari isard.

Sota idiomes preferits apareix l'opció de **Configuració d'idioma administratiu**

![](./win10_install_guide.es.images/win101.png)

S'obre un quadre de diàleg:

![](./win10_install_guide.es.images/win102.png)

Es prem a canviar els paràmetres regionals del sistema i es canvia a "Català"

I ara la part més important perquè els nous usuaris també tinguin el nou idioma per defecte. Es prem en **Còpia la configuració** i se selecciona **Pantalla de benvinguda i comptes del sistema** i **Comptes d'usuaris nous**. Queda una configuració com aquesta:

![](./win10_install_guide.es.images/win103.png)

S'aplica els canvis i es reinicia:

![](./win10_install_guide.es.images/win104.png)

Fins i tot poden quedar restes de l'idioma original (espanyol en aquest cas) en l'usuari actual, s'hauria d'esborrar el compte d'usuari i tornar a crear-la perquè en refer de nou el compte reculli bé totes les opcions d'idioma. Per això és important fer el canvi d'idioma abans de crear els usuaris admin i user que són els que es donaran per defecte en la plantilla bas3 de windows.


## Adaptar windows per a entorn virtualizat

En un entorn virtualizat ens interessa que els recursos que utilitza el windows siguin mínims a nivell d'escriptura en disc, ús de cpu i ram. Quant menor impacte en aquests factors tinguem, més escriptoris simultanis es pot córrer en un servidor. Una disminució de processos que habitualment l'usuari no usa en un entorn educatiu o empresarial també afavorirà un inici del sistema més ràpid i major agilitat. El repte està a buscar l'equilibri entre restringir funcionalitats i no penalitzar l'experiència d'usuari.

En aquesta guia es proposen diferents eines i tècniques que permetran optimitzar les instal·lacions i millorar l'experiència d'usuari.


### Instal·lar firefox

Es descarrega el firefox i s'instal·la des de la [web de mozilla](https://www.mozilla.org/es-es/firefox/new/):

S'inicia per primera vegada el firefox. S'apaga, i es torna a arrencar. Ens pregunta:

![](./win10_install_guide.es.images/win105.png)

Li diem que sí que es vol i es canvia el navegador predeterminat:

![](./win10_install_guide.es.images/win106.png)

I queda predeterminat

![](./win10_install_guide.es.images/win107.png)

### Desinstal·lar Microsoft Edge

Microsoft Edge està basat en Chromium project, es pot deixar en el sistema, però si es desitja treure del sistema operatiu es pot seguir els següents passos. S'obre una consola: es premen les tecles **Windows + R** i s'escriu **cmd**

En la consola:

```bat
cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\8*\Installer
setup --uninstall --force-uninstall --system-level
```

### Autologon

Per a facilitar els reinicis i de moment com es treballarà amb un solo usuari administrador per a realitzar instal·lacions i preparar la plantilla es proposa instal·lar una utilitat per a realitzar un autologon i que no ens demani usuari i password quan inicia windows. S'ha de buscar "autologon windows sysinternals" i arribar a la web de sysinternals des d'on es descarrega la utilitat. L'enllaç on trobar la utilitat és: [Autologon](https://docs.microsoft.com/en-us/sysinternals/downloads/autologon)

![](./win10_install_guide.es.images/win108.png)

A continuació es descarrega el fitxer zip que ens proporcionen, s'obre i es descomprimeix **Autologon64**:

![](./win10_install_guide.es.images/win109.png) 

![](./win10_install_guide.es.images/win110.png)

S'executa Autologon64, s'introdueix la contrasenya del compte isard: **pirineus**

![](./win10_install_guide.es.images/win111.png)

Ens informa que la contrasenya queda encriptat:

![](./win10_install_guide.es.images/win112.png)

Es reinicia per a verificar que ja no ens demana usuari i password en entrar:

![](./win10_install_guide.es.images/win113.png)

### Crear usuari admin

Un compte admin ens permetrà poder treballar com a administradors en cas que es vulgui convertir el compte de l'usuari "isard" a usuari normal.

Des de powershell com a administrador (botó dret damunt de la icona de windows):

![](./win10_install_guide.es.images/win114.png)

```
# Guardar el password en una variable
$Password = Read-Host -AsSecureString
```
I una vegada s'ha introduït el password: ***pirineus***

```powershell
# Crear usuari admin del grup administradors
New-LocalUser "admin" -Password $Password -FullName "admin"
Add-LocalGroupMember -Group "Administradores" -Member "admin"
```

I l'usuari "user" que no serà administrador:

```
New-LocalUser "user" -Password $Password -FullName "user"
Add-LocalGroupMember -Group "Usuarios" -Member "user"
```


### Directori c:\admin

També es crea el directori c:\admin on es guardaran els programes o informació d'administració. Es treuen els permisos d'usuaris, perquè només puguin accedir administradors:

![](./win10_install_guide.es.images/win115.png)

I es deixa només el permís per al grup administradors:

![](./win10_install_guide.es.images/win116.png)


### Desinstal·lar Microsoft Edge

Microsoft Edge està basat en Chromium project, es pot deixar en el sistema, però si es desitja llevar del sistema operatiu es pot seguir els següents passos. S'obre una consola: Es premen les tecles **Windows + R** i s'escriu **cmd**

En la consola:

```bat
cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\8*\Installer
setup --uninstall --force-uninstall --system-level
```

### Desinstal·lar altres aplicacions de Microsoft

S'obre una consola de powershell com a administrador:

- Botó dret damunt de la icona de windows en la barra d'inici i se selecciona **Windows PowerShell (Administrador)**:

![](./win10_install_guide.es.images/win117.png)

Per a mirar tots els programes instal·lats:

```
Get-AppxPackage | Select Name
```

I ara s'esborra tots aquests:

```
Microsoft.Xbox*
Microsoft.Bing*
Microsoft.3DBuilder
Microsoft.Advertising.Xaml
Microsoft.AsyncTextService
Microsoft.BingWeather
Microsoft.BioEnrollment
Microsoft.DesktopAppInstaller
Microsoft.GetHelp
Microsoft.Getstarted
Microsoft.Microsoft3DViewer
Microsoft.MicrosoftEdge
Microsoft.MicrosoftEdgeDevToolsClient
Microsoft.MicrosoftOfficeHub
Microsoft.MicrosoftSolitaireCollection
Microsoft.MicrosoftStickyNotes
Microsoft.MixedReality.Portal
Microsoft.Office.OneNote
Microsoft.People
Microsoft.ScreenSketch
Microsoft.Services.Store.Engagement
Microsoft.Services.Store.Engagement
Microsoft.SkypeApp
Microsoft.StorePurchaseApp
Microsoft.Wallet
Microsoft.WindowsAlarms
Microsoft.WindowsCamera
Microsoft.WindowsFeedbackHub
Microsoft.WindowsMaps
Microsoft.WindowsSoundRecorder
Microsoft.WindowsStore
Microsoft.XboxGameCallableUI
Microsoft.YourPhone
Microsoft.ZuneMusic
Microsoft.ZuneVideo
SpotifyAB.SpotifyMusic
Windows.CBSPreview
microsoft.windowscommunicationsapps
windows.immersivecontrolpanel
```

Ara es copia i es pega en la terminal de powershell

```
Get-AppxPackage -Name Microsoft.Xbox* | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Bing* | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.3DBuilder | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Advertising.Xaml | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.AsyncTextService | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.BingWeather | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.BioEnrollment | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.DesktopAppInstaller | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.GetHelp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Getstarted | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Microsoft3DViewer | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftEdge | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftEdgeDevToolsClient | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MicrosoftStickyNotes | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.MixedReality.Portal | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Office.OneNote | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.People | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ScreenSketch | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.SkypeApp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.StorePurchaseApp | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.Wallet | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsAlarms | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsCamera | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsFeedbackHub | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsMaps | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsSoundRecorder | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.XboxGameCallableUI | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.YourPhone | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ZuneMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Microsoft.ZuneVideo | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name SpotifyAB.SpotifyMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name Windows.CBSPreview | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name microsoft.windowscommunicationsapps | Remove-AppxPackage -ErrorAction SilentlyContinue
Get-AppxPackage -Name windows.immersivecontrolpanel | Remove-AppxPackage -ErrorAction SilentlyContinue
```

### Barra de programes simple

Es pot llevar tots els elements de Productivitat i Explorar de la barra d'inici.

Es prem el botó dret damunt dels elements i **Desanclar d'Inici**

![](./win10_install_guide.es.images/win118.png)


### Desactivar serveis

S'executa "**msconfig**" i es desactiva serveis:

- Administrador d'autenticació Xbox Live
- Centre de seguretat
- Firewall de Windows Defensar
- Mozilla Maintenance Service
- Servei d'antivirus de Microsoft Defensar
- Servei de Windows Update Medic
- Windows Update

En Inici de windows -> anar a l'administrador de tasques i deshabilitar:

- Microsoft OneDrive

- Windows Security notification

  ![](./win10_install_guide.es.images/win119.png)
  
  Es reiniciar per a aplicar els canvis
  
  ![](./win10_install_guide.es.images/win120.png)
  

### Repassar escriptoris de admin i user

S'inicia sessió i es repassa que les icones d'escriptori, icones de barra de tasques i menú inicio estan bé... Tornar a treure els programes de windows...

Per a treure les notificacions:

![](./win10_install_guide.es.images/win121.png)


### Instal·lar programari lliure

Es tria instal·lar el següent programari:

- Libre Office

- Gimp

- Inkscape

- LibreCAD

- Geany

![](./win10_install_guide.es.images/win122.png)

### Aplicacions no lliures

S'instal·len les següents aplicacions:

- Google chrome
- Adobe Acrobat Reader

Es desactiva Update Service:

![](./win10_install_guide.es.images/win123.png)

Es desactiva Google Update:

![](./win10_install_guide.es.images/win124.png)

Una eina molt útil per a preparar plantilles és **VMware US Optimization Tool**

Es pot descarregar des de: [VMware US Optimization tool](https://flings.vmware.com/vmware-os-optimization-tool)

Es deixa en c:\admin\VMwareOSOptimizationTool_b2000_17205301.zip

Abans de passar aquesta eina es recomana fer una plantilla. Si l'eina d'optimització realitza massa modificacions que interfereixen amb alguna de les aplicacions que s'ha instal·lada es pot tornar a aquest punt i realitzar l'optimització posteriorment.

## Vmware US optimization tool

Es descomprimeix el fitxer zip que s'ha emmagatzemat en c:\admin i s'arrenca la utilitat: VMwareOSOptimizationTool

![](./win10_install_guide.es.images/win125.png)

Una vegada arrenca l'aplicació es prem el botó **Analyze**:

![](./win10_install_guide.es.images/win126.png)

I després a "Common options", on en Visual Effect se selecciona "Best quality" i atenció al check de "Disable hardware acceleration":

![](./win10_install_guide.es.images/win127.png)

En Store Apps es deixa "Calculator"

![](./win10_install_guide.es.images/win128.png)

I es prem el botó de "Optimize", quan finalitza es guarda el resultat amb el botó "export" en c:\admin i després es reinicia.
