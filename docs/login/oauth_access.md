# Access via OAuth

To be able to access, for example, from a Google account through a registration code provided by a Manager.

From the main page it is possible to access through a Google email account.

![](./oauth_access.images/login1.png)

Press the button ![](./oauth_access.ca.images/codi3.png) from the IsardVDI main screen

It will ask for a Google email account

![](./oauth_access.images/google1.png)

Once entered the email, click the "Next" button and it will request the password of the Google account.

![](./oauth_access.images/google2.png)

Once entered, click the "Next" button.


## Registration by registration code

If this is the first time you are accessing as a user, a form will appear to enter a registration code.

The registration code is created by the organization manager and how it is generated is explained later in the "Manager" section of the documentation. Each user must belong to a group and have a role, either advanced (teacher), user (student), etc. For each group of users, self-registration codes can be generated for different roles.

Enter the code provided by a Manager and press the button ![](./oauth_access.images/code2.png)

![](./oauth_access.images/code1.png)

And you access the basic interface authenticated with the Google account.

![](./oauth_access.images/home1.png)

Once authenticated, there is no need to repeat the registration process. When you want to access again through Google, just click on the button ![](./oauth_access.ca.images/google1.png) and use Google credentials.