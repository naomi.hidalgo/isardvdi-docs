# Introducción

Para acceder al servidor hay que ir a https://DOMINIO

Es recomendable dar un enlace a los usuarios de tu centro con la URL directa de acceso al centro, de este modo no hay que seleccionar la categoría (la categoría dentro de IsardVDI es el centro educativo).

Cada instituto tiene un código de categoría que lo identifica, este código de categoría os lo pasarán junto con la lista de usuarios y passwords creados. Por ejemplo si somos del Insituto Prueba el identificador de la categoría es INS_PRUEBA y la URL de acceso es: https://DOMINIO/login/ins_prueba (el nombre de la categoría en minúscula).