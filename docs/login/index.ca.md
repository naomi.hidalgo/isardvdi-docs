# Introducció

Per a accedir al servidor cal anar a https://DOMINI

És recomanable donar un enllaç als usuaris del teu centre amb la URL directa d'accés al centre, d'aquesta manera no cal seleccionar la categoria (la categoria dins d'IsardVDI és el centre educatiu).

Cada institut té un codi de categoria que l'identifica, aquest codi de categoria us el passaran juntament amb la llista d'usuaris i contrasenyes creats. Per exemple si som del Insitut Prova l'identificador de la categoria és INS_PROVA i la URL d'accés és: https://DOMINI/login/ins_prueba (el nom de la categoria en minúscula).