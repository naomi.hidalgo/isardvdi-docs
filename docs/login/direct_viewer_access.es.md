# Acceso por Visor Directo

Como usuario advanced (profesor) o manager (gestor del instituto) puedes generar un enlace para que un usuario se pueda conectar al visor de un escritorio sin necesidad de estar autenticado. Este tipo de accesos están pensados para dar un acceso a otro profesor que queremos que pruebe el sistema, compartir un acceso al escritorio entre varios usuarios o por ejemplo para que una persona externa pueda probar o instalar un software o hacer helpdesk remoto sobre un escritorio.

El acceso por visor directo se hace con un código único de acceso y te lleva directamente a la página de visores. Por ejemplo una url del tipo: https://DOMINIO/vw/U50mGaGmyPfoKAa3vLam3jLbFEIlAjB0r6rAr8BqSUg nos llevaría directamente a esta página de visores:

![](./direct_viewer_access.es.images/visor1.png)