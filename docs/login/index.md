# Introduction

To access the server you have to go to https://DOMAIN

It is recommended to give a link to the users of your center with the direct URL to access the center, in this way you do not have to select the category (the category within IsardVDI is the educational center).

Each institute has a category code that identifies it, this category code will be sent to you along with the list of users and passwords created. For example, if we are from the Test Institute, the category identifier is INS_TEST and the access URL is: https://DOMAIN/login/ins_test (the name of the category in lowercase).