# Direct Viewer Access

As an advanced user (teacher) or manager (institute manager) you can generate a link so that a user can connect to a desktop viewer without having to be authenticated. This type of access is designed to give access to another teacher who we want to test the system, share access to the desktop among several users or for example so that an external person can test or install software or do remote helpdesk on a desktop.

Direct viewer access is done with a unique access code and takes you directly to the viewer page. For example a url of the type: https://DOMAIN/vw/U50mGaGmyPfoKAa3vLam3jLbFEIlAjB0r6rAr8BqSUg would take us directly to this page of viewers:

![](./direct_viewer_access.images/visor1.png)