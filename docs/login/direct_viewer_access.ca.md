# Accés per Visor Directe

Com a usuari advanced (professor) o manager (gestor de l'institut) pots generar un enllaç perquè un usuari es pugui connectar al visor d'un escriptori sense necessitat d'estar autenticat. Aquest tipus d'accessos estan pensats per a donar un accés a un altre professor que volem que provi el sistema, compartir un accés a l'escriptori entre diversos usuaris o per exemple perquè una persona externa pugui provar o instal·lar un programari o fer helpdesk remot sobre un escriptori. 

L'accés per visor directe es fa amb un codi únic d'accés i et porta directament a la pàgina de visors. Per exemple una url del tipus: https://DOMINI/vw/u50mgagmypfokaa3vlam3jlbfeilajb0r6rar8bqsug ens portaria directament a aquesta pàgina de visors:

![](./direct_viewer_access.ca.images/visor1.png)