# Acceso Local

Se han pasado una serie de usuarios y contraseñas de usuarios locales (no se validan por Google). Si se accede a https://DOMINIO hay que seleccionar la categoría y el idioma:

![](./local_access.es.images/local_access1.png)

Si se accede tal como se ha recomendado a la URL de login del centro (https://DOMINIO/login/categoría), solo es necesario elegir el idioma:

![](./local_access.es.images/local_access2.png)

La elección del idioma se queda guardada en un cookie en el navegador, y la próxima vez que entres ya aparecerá preseleccionada con el idioma que elegiste la última vez.