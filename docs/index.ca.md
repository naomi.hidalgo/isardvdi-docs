# Introducció

Benvingut a la documentació d'[IsardVDI](https://isardvdi.com).

Aquí podeu trobar informació útil per a una gran varietat d'usuaris. Des d'usuaris d'escriptori virtual a administradors de sistema.

Com a usuari, pots començar a la [secció d'usuari](user/index.ca.md).

Si esteu interessat a instal·lar-lo, aneu a la [secció d'instal·lació](install).

Si no trobeu el que esteu buscant, contacteu amb nosaltres a <mailto:info@isardvdi.com>.

## Què és el sistema IsardVDI?

IsardVDI és un sistema de desplegament d'Infraestructura d'Escriptoris Virtuals (En anglès Virutal Desktop Infrastructure (VDI) de codi obert basat en virtualització de linux KVM i dockers. Es pot instal·lar de forma independent en uns minuts però també pot créixer a mesura que necessiteu amb múltiples hipervisors.

## Funcionalitats

Aquestes són les principals característiques:

- Interfície bàsica per gestionar els escriptoris.
- Registre automàtic d'OAUTH2 i inici de sessió amb Google i Github.
- Configuració multitenant per categories i nou rol de gestor multitenant.
- Es poden establir límits per grup i categoria:
    - Nombre màxim d'escriptoris creats, escriptoris concurrents, plantilles, vCPUS, memòria, ISOS carregats, ...
- Escriptoris efímers amb un ús limitat del temps.
- Creació d'escriptori automàtic persistent en iniciar la sessió de l'usuari.
- Generació automàtica i renovació de certificats Letsencrypt.
- Només s'usen els ports 80 i 443 per a l'accés web i l'accés als visors.
- Estadístiques de sistema i ús d'usuaris amb grafana.

## Downloads

Podeu baixar immediatament les imatges preinstal·lades i optimitzades del sistema operatiu des del menú **Downloads**.

- Inicieu **escriptoris de demostració** i connecteu-vos-hi utilitzant el vostre navegador i protocol spice o vnc.  No cal instaŀlar res i serà assegurat amb certificats.
- Instal·leu virt-viewer i connecteu utilitzant el client d'spice. El connector transparent de **so i USB** estarà disponible.

Creeu el vostre propi escriptori utilitzant ISOs baixats des d'actualitzacions o podeu pujar la vostra opció des del menú **Media**. Quan acabeu d'instal·lar el sistema operatiu i les aplicacions creeu una **plantilla** i decidiu quins usuaris o categories poden crear un escriptori idèntic a aquesta plantilla. Gràcies a la **creació incremental de disc** tot això es pot fer en minuts.

<iframe src="https://drive.google.com/file/d/1tPL12yw3MEV5IEPL5by7z76zVVSNnAng/preview" width="640" height="480"></iframe>

No esteu lligats a una instal·lació «autònoma» en un servidor. Podeu afegir més hipervisors al **pool** i deixar que IsardVDI decideixi on iniciar cada escriptori. Cada hipervisor només necessita el docker-compose d'hipervisor IsardVDI. Tingueu en compte que hauríeu de mantenir l'emmagatzematge compartit entre aquests hipervisors.

## Casos d'ús (coneguts)

### Educatiu

Actualment gestionem una **gran infraestructura d'IsardVDI** a l'Escola del Treball de Barcelona. 3000 estudiants i professors tenen IsardVDI disponible sis hipervisors i un cluster NAS dual fet a mida, que van des de plaques base dual core de servidor intel d'alta gama a gigabyte de gaming.

Tenim experiència en diferents clients prims que utilitzem per reduir els costos de renovació i consum a les aules.

## Administració Pública

L'administració pública local a Catalunya té una instal·lació de tres hipervisors reformats que vam fer en temps rècord fent també una integració sense fissures en la seva autenticació SASL2. Vam fer algunes proves de rendiment amb IsardVDI en aquesta infraestructura aconseguint un resultat impressionant de +2000 escriptoris VDI concurrents.

## Per què triar IsardVDI?

- Llicència AGPLv3 de codi obert.
- Grans implementacions de proves de concepte.
- Optimitzacions d'infraestructures i reduccions de costs.
- Sense llicències.
- BYOD: HTML5 o visors SPICE nadius.
- Instal·lacions per a múltiples clients.
- OAUTHv2, SASL2, autenticació a base de dades amb codi de registre automàtic.
- Sistema de control amb API REST.
- Creació immediata d'escriptoris i plantilles.
- Escriptoris persistents i no persistents.
- Dockeritzat. És fàcil instal·lar i mantenir en qualsevol distribució de Linux.
- Apliqueu límits per als clients i quotes per categories, grups i/o usuaris.
- ...

## Autors de IsardVDI

- Josep Maria Viñolas Auquer
- Alberto Larraz Dalmases
- Néfix Estrada Campañà

## Suport / Contacte

Si us plau envieu-nos un correu electrònic a <mailto:info@isardvdi.com> si teniu cap pregunta o [creeu un tiquet](https://gitlab.com/isard/isardvdi/-/issues/new).
