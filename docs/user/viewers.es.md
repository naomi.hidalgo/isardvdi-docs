# Visores

## Tipos de visores

Podemos diferenciar dos tipos principales de visores en función del lugar desde donde accedemos:

* **Visores integrados en el navegador**: el visor queda integrado dentro de una página web, desde una pestaña del navegador podemos manejar un windows o un linux. La decodificación de la señal de vídeo y el envío de la señal del ratón y el teclado se hace desde una pestaña del navegador. 

  * **Ventajas**: 
    * No necesitas tener un cliente instalado, funciona desde cualquier dispositivo (ordenadores, tablets, móviles) y sistema operativo que tenga un navegador.
  * **Inconvenientes**: 
     * La decodificación no es tan eficiente cómo en un visor dedicado, se nota **más lentitud** en el refresco del escritorio y cuando se mueven elementos dentro del escritorio.
     * No podemos redirigir al escritorio virtual dispositivos locales conectados por usb como un pendrive, una webcam... 

* **Aplicaciones cliente de escritorio**: son aplicaciones dedicadas a realizar de visor, están optimizadas y son la mejor opción para tener la mejor experiencia de usuario

  * **Ventajas**: 
    * Optimizadas para los protocolos de clientes de escritorio, mejor rendimiento que en los visores integrados en el navegador. Si no hay problemas de red (latencia y ancho de banda adecuados) la sensación de trabajo con el escritorio es similar a la de un escritorio real.
    * Aunque depende del tipo de protocolo y de la versión del cliente, en general ofrecen opciones avanzadas como: redirección de puertos USB, copiar y pegar desde el escritorio virtual al escritorio real, subir ficheros por el visor hacia el escritorio virtual... 
  * **Inconvenientes**: 
    * No en todos los casos vienen pre-instalados con el sistema operativo, con lo que es necesario realizar una instalación en el sistema operativo del equipo desde el que nos conectamos. 
    * Funciones avanzadas no disponibles en función de las versiones del cliente y la versión del protocolo

## Protocolos y visores disponibles en Isard

* **SPICE**:  es un protocolo de comunicación para entornos virtuales. Nos permite acceder a la señal de vídeo, ratón y teclado del escritorio como si estuviéramos conectados a la pantalla, ratón y teclado de un ordenador real.  

  * **Ventajas**:
    * Es el propio motor de máquinas virtuales que usa Isard (Qemu-KVM) el que nos da acceso independientemente del sistema operativo que esté corriendo. 
    * Podemos usar este protocolo para ver toda la secuencia de arranque del escritorio, instalar sistemas operativos...
    * No necesitamos instalar ningún componente en el sistema operativo del escritorio virtual para poder interactuar con él. 
    * Es un protocolo que optimiza el ancho de banda de vídeo utilizado comprimiendo la señal y enviando sólo las zonas que varían de un frame a otro.

  * **Inconvenientes**:
      * El cliente no viene instalado por defecto en ningún sistema operativo, la instalación es muy sencilla y la puede hacer cualquier usuario, pero en entornos corporativos o educativos las restricciones de permisos puede que dificulten el poderlo instalar.
      * La instalación en windows requiere de un programa extra para poder redirigir los puertos USB
      * La conexión se hace por un proxy HTTP utlizando un método "CONNECT". Este método, en algunos casos, es filtrado por algún cortafuegos o proxy intermedio. 

* **VNC**: es un protocolo que trabaja en el mismo nivel que SPICE pero que al ser más antiguo la señal de vídeo no está tan optimizada. Es el protocolo que usamos en el cliente integrado en el navegador 

* **RDP**: es el protocolo que se usa por defecto para conectarse remotamente a un sistema operativo windows. Necesita que permitamos el acceso remoto en el sistema operativo. No está disponible desde el arranque del escritorio y hay que esperar a que en la secuencia de arranque del escritorio obtenga una dirección IP. La principal ventaja es que ofrece la mejor experiencia de fluidez para un escritorio de windows, usando el cliente nativo de windows de RDP acual. Es el protocolo recomendado también cuando se usa junto con tarjetas vGPU de Nvidia. 

  * **Ventajas**: 
      * La mejor experiencia de usuario si el sistema operativo del escritorio virtual es windows y del ordenador desde donde se conecta el cliente también es windows
      * En los clientes windows no es necesario instalar software adicional, ya que el cliente de escritorio remoto viene por defecto en todos los windows
      * Es necesario para una buena experiencia de usuario usando vGPUs de Nvidia sobre sistemas operativos windows.
    
  * **Inconvenientes**: 
      * Si hay algún problema en el arranque del sistema operativo no accedes a la señal de pantalla (lo solventas conectándote por spice o vnc)

## Puertos y proxies

En Isard se ha realizao un esfuerzo importante por no tener que abrir puertos extra y encapsular en proxys HTTP las conexiones. Se trata de adaptar a cualquier situación en la que haya un firewall que pueda dificultar la conexión. Por defecto se usan los puertos:

- TCP/80 para el proxy donde se encapsulan las conexiones del protocolo SPICE
- TCP/443 para la web y los visores integrados en el navegador
- TCP/9999 para el proxy por donde se encapsulan las conexioes del protocolo RDP

## Visor VNC integrado en el navegador

Para poder ver un escritorio directamente desde el navegador se selecciona en el menú desplegable la opción ![](viewers.es.images/visor1.png)

![](viewers.es.images/visor2.png)

**Es necesario tener en cuenta si se tiene bloqueado en el navegador que se abran ventanas emergentes**

En Firefox: Aparecerá un mensaje sobre las ventanas emergentes

![](viewers.es.images/visor3.png)

Se pulsa el botón ![](viewers.es.images/visor4.png)

![](viewers.es.images/visor5.png)

Y abrirá el escritorio en una pestaña nueva del navegador

![](viewers.es.images/visor17.png)

## Visor para el protocolo SPICE

**Se recomienda utilizar la [versión 7](https://www.spice-space.org/download.html) de Spice.**

### Spice para Windows

Se descarga el [virt-viewer Windows Installer](http://virt-manager.org/download) y se instala.

Para poder utilizar el visor de Spice, se descarga el cliente del visor:

### Microsoft
#### Spice client app

* [Spice viewer client](https://releases.pagure.org/virt-viewer/virt-viewer-x64-9.0.msi)
    * [Spice viewer USB client para Windows](https://www.spice-space.org/download/windows/usbdk/UsbDk_1.0.22_x64.msi)


### Linux
#### Spice client app

* Distribuciones basadas en Debian/Ubuntu: `sudo apt install virt-viewer -y`
* Distribuciones basadas en RedHat/CentOS/Fedora: `sudo dnf install remote-viewer -y`

Una vez se haya instalado, cuando se arranque un escritorio se puede seleccionar la opción de "Visor Spice" en el menú desplegable

![](viewers.es.images/visor10.png)

Y aparecerá una ventana emergente para poder abrir el archivo Spice

![](viewers.es.images/visor11.png)

Una vez abierto, aparecerá una ventana con el escritorio

![](viewers.es.images/visor12.png)

## Visor RDP

Para que el cliente RDP se pueda conectar **Es necesario tener activada la red Wireguard en el escritorio que es a través de la que nos conectaremos y en dicho escritorio tener configurado el "Escritorio remoto"** 

En los equipos con sistema operativo windows no es necesario instalar ningún software adicional ya que llevan el cliente de escritorio remoto instalado como una herramienta de widows. 

### RDP client app (Windows)

* Por lo general, Windows ya tiene instalada la "Conexión de escritorio remoto". Si no lo tiene, se descarga en [Remote Desktop clients](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-clients)


En los equipos con linux hay que instalar un cliente como remmina y en macOS se dispone de una aplicación oficial, Microsoft Remote Desktop, que se puede descargar desde el [Apple store](https://apps.apple.com/es/app/microsoft-remote-desktop/id1295203466?mt=12)

### RDP client app (Linux)

* En las distribuciones de Linux, la aplicación cliente para RDP se llama `Remmina` y se puede instalar siguiendo [esta guía](https://remmina.org/how-to-install-remmina/)

Como se ha comentado anteriormente el visor RDP necesita de una ip donde conectarnos, por eso cuando arranca un escritorio observamos un parpadeo y aparecen los enlaces a los visores RDP como no seleccionables:

![](viewers.es.images/visor16.png)

Una vez ha obtenido dirección IP, se informa de ella y se activa el acceso a los visores:

![](viewers.es.images/visor13.png)

Al seleccionar el **visor RDP** se descarga un fichero con extensión .rdp:

![](viewers.es.images/visor14.png)

El fichero isard-rdp-gw.rdp contiene la información para poder conectarse con el escritorio.

Es probable que la primera vez en los sistemas operativos windows nos informe con una alerta de seguridad, le podemos confirmar en la casilla "No volver a preguntarme sobre conexiones a este equipo" y en el futuro ya no nos lo pedirá más.

![](viewers.es.images/visor15.png)

Ahora nos pide las credenciales. Es importante notar que este usuario y password es el del escritorio virtual que está corriendo. En las plantillas que ofrecemos por defecto:

* el usuario es: isard
* el password es: pirineus

![](viewers.es.images/visor18.png)

Al acceder al escritorio nos pide confirmación para aceptar el certificado:

![](viewers.es.images/visor19.png)

Y finalmente se abre el cliente y podemos interactuar con el escritorio.

## Descarga de clientes nativos del visor para otras distribuciones

*Para la aplicación Spice Client, puede encontrar más información para cualquier sistema operativo en [spice-space](https://www.spice-space.org/download.html)*

### MacOS

#### Spice client app

* Se puede seguir [esta guía de instalación](https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c)

#### RDP client app

* Existe una aplicación oficial, `Microsoft Remote Desktop`, que se puede descargar desde el [Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12)

### Android

#### Spice client app

* Hay una [versión gratuita](https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE) en la Play Store y una [de pago](https://play.google.com/store/apps/details?id=com.iiordanov.aSPICE) con más funcionalidades.

#### RDP client app

* Microsoft tiene su propio [Escritorio Remoto](https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx) en la Play Store

### IOS

#### Spice client app

* Hay una [versión de pago](https://apps.apple.com/gb/app/aspice-pro/id1560593107) en la Apple Store

#### RDP client app

* Microsoft tiene su propio [Escritorio Remoto](https://apps.apple.com/us/app/remote-desktop-mobile/id714464092) en la Apple Store

## Conectar dispositivos (USB) 

### Visor RDP

Del escritorio que se quiera conectar el USB, se pulsa el botón ![](./viewers.es.images/visor21.png)

![](./viewers.es.images/visor20.png)

Se tiene que descargar el archivo del visor que se quiera usar con rdp

![](./viewers.es.images/visor22.png)

Abrir el fichero donde se tenga la descarga y darle al botón de modificar

![](./viewers.es.images/visor24.png)

Se pulsa el botón de "Recursos locales"

![](./viewers.es.images/visor25.png)

Y se pulsa el botón de "Más" en dispositivos y recursos locales

![](./viewers.es.images/visor26.png)

Una vez allí, en el apartado de "Unidades" seleccionamos el usb que queramos, en mi caso yo he seleccionado el "SUN-USB(D:)"

![](./viewers.es.images/visor27.png)

Se pulsa el botón de "Conectar"

![](./viewers.es.images/visor28.png)

![](./viewers.es.images/visor29.png)

Usuario: isard y contraseña: pirineus -> por defecto

![](./viewers.es.images/visor30.png)

Se pulsa el botón "Si"

![](./viewers.es.images/visor31.png)

Y cuando se entre por el escritorio remoto tendría que salir algo como esto. En este caso es D porque es en el que estaba en mi ordenador.

![](./viewers.es.images/visor32.png)

### Visor Spice

IMPORTANTE: Descargarse el driver de USB para que se pueda detectar en el visor. (UsbDk_1.0.22x64.msi): https://www.spice-space.org/download.html

Una vez descargado el driver, se abre el visor Spice, se pulsa el botón de "File"

![](./viewers.es.images/visor33.png)

Y se selecciona la opción "USB device selection"

![](./viewers.es.images/visor34.png)

Y se selecciona el dispositivo que se quiera utilizar.

![](./viewers.es.images/visor35.png)