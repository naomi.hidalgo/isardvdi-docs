# VPN

Para poder conectarse con VPN, primero se tiene que seleccionar la red "Wireguard" del escritorio que se quiera.

Se pulsa el icono ![](./vpn.images/vpn1.png)

![](./vpn.es.images/vpn2.png)

Y el icono ![](./vpn.images/vpn3.png)

![](./vpn.es.images/vpn4.png)

En el apartado de "Hardware", en "Redes" se selecciona la red "Wireguard VPN"

![](./vpn.es.images/vpn5.png)

Se vuelve a la pantalla de Inicio y se pulsa el menú desplegable ![](./vpn.es.images/vpn9.png)

![](./vpn.es.images/vpn8.png)

Y se pulsa el botón ![](./vpn.images/vpn9.png)

![](./vpn.es.images/vpn12.png)

Se descargará un archivo al que se debería asignar un nombre corto.

![](./vpn.es.images/vpn13.png)

Se abre un terminal en tu ordinador y se ejecuta este comando:

```
sudo cp ~/Downloads/user.conf /etc/wireguard/ # Downloads = la carpeta donde se haya descargado el archivo; user.conf = el nombre que se le ha dado al archivo vpn

sudo wg-quick up user # user = nombre del archivo anterior

user:~$ ping (IP_imatge**)

user:~$ ssh isard@IP_imatge**
isard@ubuntu-server:~$

```

IP_imatge** : ![](./vpn.es.images/vpn11.png)