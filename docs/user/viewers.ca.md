# Visors

## Tipus de visors

Podem diferenciar dos tipus principals de visors en funció del lloc des d'on accedim:

* **Visors integrats en el navegador**: el visor queda integrat dins d'una pàgina web, des d'una pestanya del navegador podem manejar un Windows o un Linux. La descodificació del senyal de vídeo i l'enviament del senyal del ratolí i el teclat es fa des d'una pestanya del navegador. 

  * **Avantatges**: 
    * No necessites tenir un client instal·lat, funciona des de qualsevol dispositiu (ordinadors, tablets, mòbils) i sistema operatiu que tingui un navegador.

  * **Inconvenients**: 
     * La descodificació no és tan eficient com en un visor dedicat, es nota *més lentitud* en el refresc de l'escriptori i quan es mouen elements dins de l'escriptori.
     * No podem redirigir a l'escriptori virtual dispositius locals connectats per usb com un pendrive, una webcam... 

 * **Aplicacions client d'escriptori**: són aplicacions dedicades a realitzar de visor, estan optimitzades i són la millor opció per a tenir la millor experiència d'usuari
 
  * **Avantatges**: 
     * Optimitzades per als protocols de clients d'escriptori, millor rendiment que en els visors integrats en el navegador. Si no hi ha problemes de xarxa (latència i ample de banda adequats) la sensació de treball amb l'escriptori és similar a la d'un escriptori real... 
     
  * **Inconvenients**: 
    * No en tots els casos venen pre-instal·lats amb el sistema operatiu, amb el que és necessari realitzar una instal·lació en el sistema operatiu de l'equip des del qual ens connectem. 
    * Funcions avançades no disponibles en funció de les versions del client i la versió del protocol.


## Protocols i visors disponibles en Isard

* **SPICE**:  és un protocol de comunicació per a entorns virtuals. Ens permet accedir al senyal de vídeo, ratolí i teclat de l'escriptori com si estiguéssim connectats a la pantalla, ratolí i teclat d'un ordinador real.

  * **Avantatges**:
    * És el propi motor de màquines virtuals que usa Isard (Qemu-*KVM) el que ens dona accés independentment del sistema operatiu que estigui corrent.
    * Podem usar aquest protocol per a veure tota la seqüència d'arrencada de l'escriptori, instal·lar sistemes operatius...
    * No necessitem instal·lar cap component en el sistema operatiu de l'escriptori virtual per a poder interactuar amb ell.
    * És un protocol que optimitza l'amplada de banda de vídeo utilitzat comprimint el senyal i enviant només les zones que varien d'un frame a un altre.
    
  * **Inconvenients**:
      * El client no ve instal·lat per defecte en cap sistema operatiu, la instal·lació és molt senzilla i la pot fer qualsevol usuari, però en entorns corporatius o educatius les restriccions de permisos pot ser que dificultin el poder-lo instal·lar.
      * La instal·lació en windows requereix d'un programa extra per a poder redirigir els ports USB
      * La connexió es fa per un proxy HTTP utilitzant un mètode "CONNECT". Aquest mètode, en alguns casos, és filtrat per algun tallafocs o proxy intermedi.

* **VNC**: és un protocol que treballa en el mateix nivell que SPICE però que en ser més antic el senyal de vídeo no està tan optimitzada. És el protocol que usem en el client integrat en el navegador 

* **RDP**: és el protocol que s'usa per defecte per a connectar-se remotament a un sistema operatiu windows. Necessita que permetem l'accés remot en el sistema operatiu. No està disponible des de l'arrencada de l'escriptori i cal esperar que en la seqüència d'arrencada de l'escriptori obtingui una adreça IP. El principal avantatge és que ofereix la millor experiència de fluïdesa per a un escriptori de windows, usant el client natiu de windows de RDP actual. És el protocol recomanat també quan s'usa juntament amb targetes vGPU de Nvidia.

  * **Avantatges**: 
      * La millor experiència d'usuari si el sistema operatiu de l'escriptori virtual és windows i de l'ordinador des d'on es connecta el client també és windows
      * En els clients windows no és necessari instal·lar programari addicional, ja que el client d'escriptori remot ve per defecte en tots els windows
      * És necessari per a una bona experiència d'usuari usant vGPUs de Nvidia sobre sistemes operatius windows.
      
  * **Inconvenients**: 
      * Si hi ha algun problema en l'arrencada del sistema operatiu no accedeixes al senyal de pantalla (el soluciones connectant-te per spice o vnc)


## Ports i proxies

En Isard s'ha fet un esforç important per no haver d'obrir ports extra i encapsular en proxys HTTP les connexions. Es tracta d'adaptar a qualsevol situació en la qual hi hagi un firewall que pugui dificultar la connexió. Per defecte s'usen els ports:

- TCP/80 per al proxy on s'encapsulen les connexions del protocol SPICE
- TCP/443 per a la web i els visors integrats en el navegador
- TCP/9999 per al proxy per on s'encapsulen les connexions del protocol RDP


## Visor VNC integrat en el navegador

Per a poder veure un escriptori directament des del navegador se selecciona en el menú desplegable l'opció ![](./viewers.ca.images/visor1.png)

![](./viewers.ca.images/visor2.png)


**És necessari tenir en compte si es té bloquejat en el navegador que s'obrin finestres emergents**

En Firefox: Apareixerà un missatge sobre les finestres emergents

![](./viewers.ca.images/visor3.png)

Es prem el botó ![](./viewers.ca.images/visor4.png)

![](./viewers.ca.images/visor5.png)

I obrirà l'escriptori en una pestanya nova del navegador

![](./viewers.ca.images/visor17.png)


## Visor per al protocol SPICE

**Es recomana utilitzar la [versió 7](https://www.spice-space.org/download.html) de Spice.**

### Spice per a Windows

Es descarrega el [virt-viewer Windows Installer](http://virt-manager.org/download) i s'instal·la.

Per a poder utilitzar el visor de Spice, es descarrega el client del visor:

### Microsoft
#### Spice client app

* [Spice viewer client](https://releases.pagure.org/virt-viewer/virt-viewer-x64-9.0.msi)
    * [Spice viewer USB client per a Windows](https://www.spice-space.org/download/windows/usbdk/UsbDk_1.0.22_x64.msi)


### Linux
#### Spice client app

* Distribucions basades en Debian/Ubuntu: `sudo apt install virt-viewer -y`
* Distribucions basades en RedHat/CentOS/Fedora: `sudo dnf install remote-viewer -y`

Una vegada s'hagi instal·lat, quan s'arrenqui un escriptori es pot seleccionar l'opció de "Visor Spice" en el menú desplegable

![](./viewers.ca.images/visor10.png)

I apareixerà una finestra emergent per a poder obrir l'arxiu Spice

![](./viewers.ca.images/visor11.png)

Una vegada obert, apareixerà una finestra amb l'escriptori

![](./viewers.ca.images/visor12.png)


## Visor RDP

Perquè el client RDP es pugui connectar **És necessari tenir activada la xarxa Wireguard en l'escriptori que és a través de la que ens connectarem i en aquest escriptori tenir configurat l'"Escriptori remot"**

En els equips amb sistema operatiu windows no és necessari instal·lar cap programari addicional ja que porten el client d'escriptori remot instal·lat com una eina de widows.

### RDP client app (Windows)

* En general, Windows ja té instal·lada la "Connexió d'escriptori remot". Si no ho té, es descarrega en [Remote Desktop clients](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-clients)

En els equips amb linux cal instal·lar un client com remmina i en macOS es disposa d'una aplicació oficial, Microsoft rRemote Desktop, que es pot descarregar des de [l'Apple Store](https://apps.apple.com/es/app/microsoft-remote-desktop/id1295203466?mt=12)

### RDP client app (Linux)

* En les distribucions de Linux, l'aplicació client per a RDP es diu `Remmina` i es pot instal·lar seguint [aquesta guia](https://remmina.org/how-to-install-remmina/)

Com s'ha comentat anteriorment el visor RDP necessita d'una ip on connectar-nos, per això quan arrenca un escriptori observem un parpelleig i apareixen els enllaços als visors RDP com no seleccionables:

![](./viewers.ca.images/visor16.png)


Una vegada ha obtingut adreça IP, s'informa d'ella i s'activa l'accés als visors:

![](./viewers.ca.images/visor13.png)


En seleccionar el **visor RDP** es descarrega un fitxer amb extensió .rdp:

![](./viewers.ca.images/visor14.png)


El fitxer isard-rdp-gw.rdp conté la informació per a poder connectar-se amb l'escriptori.

És probable que la primera vegada en els sistemes operatius windows ens informi amb una alerta de seguretat, li podem confirmar en la casella "No tornar a preguntar-me sobre connexions a aquest equip" i en el futur ja no ens el demanarà més.

![](./viewers.ca.images/visor15.png)

Ara ens demana les credencials. És important notar que aquest usuari i password és el de l'escriptori virtual que està corrent. En les plantilles que oferim per defecte:

* l'usuari és: isard
* el password és: pirineus

![](./viewers.ca.images/visor18.png)

En accedir a l'escriptori ens demana confirmació per a acceptar el certificat:

![](./viewers.ca.images/visor19.png)

I finalment s'obre el client i podem interactuar amb l'escriptori.

### Activar RDP en un escriptori Ubuntu

A partir de la versió 22 d'Ubuntu ja porten el servidor RDP instal.lat i es pot configurar seguint aquestes [instruccions](https://nextcloud.isardvdi.com/s/XY4ow9P5NHASxmx)

## Descàrrega de clients natius del visor per a altres distribucions

*Per a l'aplicació Spice Client, pot trobar més informació per a qualsevol sistema operatiu en [spice-space](https://www.spice-space.org/download.html)*

### MacOS

#### Spice client app

* Es pot seguir [aquesta guia d'instal·lació](https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c)

#### RDP client app

* Existeix una aplicació oficial, `Microsoft Remote Desktop`, que es pot descarregar des de [l'Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12)

### Android

#### Spice client app

* Hi ha una [versió gratuïta](https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE) en la Play Store i una [de pagament](https://play.google.com/store/apps/details?id=com.iiordanov.aSPICE) amb més funcionalitats.

#### RDP client app

* Microsoft té el seu propi [Escriptori Remot](https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx) en la Play Store

### IOS

#### Spice client app

* Hi ha una [versió de pagament](https://apps.apple.com/gb/app/aspice-pro/id1560593107) en l'Apple Store

#### RDP client app

* Microsoft té el seu propi [Escriptori Remot](https://apps.apple.com/us/app/remote-desktop-mobile/id714464092) en l'Apple Store

## Connectar dispositius (USB) 

### Visor RDP

De l'escriptori que es vulgui connectar l'USB, es prem el botó ![](./viewers.es.images/visor21.png)

![](./viewers.ca.images/visor20.png)

S'ha de descarregar l'arxiu del visor que es vulgui usar amb rdp

![](./viewers.ca.images/visor21.png)

Obrir el fitxer on es tingui la descàrrega i donar-li al botó de "modificar"

![](./viewers.es.images/visor24.png)

Es prem el botó de "Recursos locales"

![](./viewers.es.images/visor25.png)

I es prem el botó de "Más" en dispositius i recursos locals

![](./viewers.es.images/visor26.png)

Una vegada allí, en l'apartat de "Unitats" seleccionem el usb que vulguem, en el meu cas jo he seleccionat el "SUN-USB(D:)"

![](./viewers.es.images/visor27.png)

Es prem el botó de "Conectar"

![](./viewers.es.images/visor28.png)

![](./viewers.es.images/visor29.png)

Usuari: isard i contrasenya: pirineus -> per defecte

![](./viewers.es.images/visor30.png)

Es prem el botó "Si"

![](./viewers.es.images/visor31.png)

I quan s'entre per l'escriptori remot hauria de sortir alguna cosa com això. En aquest cas és D perquè és en el qual estava en el meu ordinador.

![](./viewers.es.images/visor32.png)

### Visor Spice

**IMPORTANT: Descarregar-se el driver d'USB perquè es pugui detectar en el visor.(UsbDk_1.0.22x64.msi): https://www.spice-space.org/download.html**

Una vegada descarregat el driver, s'obre el visor Spice, es prem el botó de "File"

![](./viewers.es.images/visor33.png)

I se selecciona l'opció "USB device selection"

![](./viewers.es.images/visor34.png)

I se selecciona el dispositiu que es vulgui utilitzar.

![](./viewers.es.images/visor35.png)

