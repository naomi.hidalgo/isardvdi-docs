# Introducció

Un alumne ha de conèixer els diferents tipus de visors amb els quals es pot connectar a un escriptori i com a professor hem de saber aconsellar quan és millor usar l'un o l'altre. 

Com a alumnes pots crear escriptoris persistents o volàtils a partir de plantilles que hagin compartit amb tu. Aquesta creació la realitza l'alumne i no la pot controlar directament el professor.

L'alumnes també pot disposar d'escriptoris creats per un professor en un desplegament. Un desplegament està pensat perquè sigui el professor el que genera l'escriptori a l'alumne i en aquest cas el professor sí que té control sobre les accions que es poden realitzar en aquest escriptori. També el professor podrà decidir quan fer que aquests escriptoris creats en un desplegament siguin accessibles o quedin ocults a l'alumne. 

Un usuari que accedeix per primera vegada pot no tenir cap escriptori creat, el manual explicarà primer com crear un escriptori, com accedir als seus visors i posteriorment entra més detalladament sobre com editar o modificar l'escriptori