# Introduction

A student must know the different types of viewers with which they can connect to a desktop and as a teacher we must know how to advise when it is better to use one or the other.

As students you can create persistent or temporal desktops from templates that have been shared with you. This creation is done by the student and cannot be directly controlled by the teacher.

Students can also have desktops created by a teacher in a deployment. A deployment is designed so that it is the teacher who generates the desktop for the student and in this case the teacher does have control over the actions that can be performed on this desktop. The teacher will also be able to decide when to make these desktops created in a display accessible or hidden from the student.

A user who accesses for the first time may not have any desktop created, the manual will first explain how to create a desktop, how to access its viewers and later goes into more detail on how to edit or modify the desktop.