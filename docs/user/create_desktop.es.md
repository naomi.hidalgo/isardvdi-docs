# Crear Escritorio

Hay dos tipos de escritorios:

* **Temporal**: Como su nombre indica es un escritorio "temporal", eso quiere decir que una vez arrancado un escritorio, cuando se cierre se perderá todo lo que se haya guardado o descargado.
* **Persistente**: Es un escritorio que no se borra, cuando se apague, no se perderá la información o programas que se hayan instalado. 


## Vista principal

Para crear un escritorio se pulsa el botón ![](./create_desktop.es.images/creacion2.png)

![](./create_desktop.es.images/creacion1.png)

Se rellenan los campos, se selecciona una plantilla y si no se quiere configurar las opciones avanzadas del escritorio, se pulsa el botón ![](./create_desktop.es.images/creacion4.png)

![](./create_desktop.es.images/creacion3.png)

Se puede cambiar la configuración del escritorio en el apartado de "Opciones avanzadas)

![](./create_desktop.es.images/creacion5.png)

Aquí se puede seleccionar los visores que se quieran utilizar en ese escritorio, editar su hardware, añadir una GPU (si tiene), añadir isos y cambiarle la foto.

![](./create_desktop.es.images/creacion6.png)

