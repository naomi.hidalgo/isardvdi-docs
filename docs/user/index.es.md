# Introducción

Un alumno ha de conocer los diferentes tipos de visores con los que se puede conectar a un escritorio y como profesor hemos de saber aconsejar cuando es mejor usar uno u otro. 

Como alumnos puedes crear escritorios persistentes o volátiles a partir de plantillas que hayan compartido contigo. Esta creación la realiza el alumno y no la puede controlar directamente el profesor.

El alumnos también puede disponer de escritorios creados por un profesor en un despliegue. Un despliegue está pensado para que sea el profesor el que genera el escritorio al alumno y en este caso el profesor sí que tiene control sobre las acciones que se pueden realizar en este escritorio. También el profesor podrá decidir cuando hacer que estos escritorios creados en un despliegue sean accesibles o queden ocultos al alumno. 

Un usuario que accede por primera vez puede no tener ningún escritorio creado, el manual explicará primero como crear un escritorio, como acceder a sus visores y posteriormente entra más en detalle sobre como editar o modificar el escritorio.