# Create desktop

There are two types of desktops:

* **Temporary**: As its name indicates, it is a "temporary" desktop, that means that once a desktop is started, everything that has been saved or downloaded will be lost when it is closed.

* **Persistent**: It is a desktop that is not deleted, when it is turned off, the information or programs that have been installed will not be lost.


## Main view

To create a desktop, click the button ![](./create_desktop.images/create1.png)

![](./create_desktop.images/create2.png)

The fields are filled in, a template is selected and if you do not want to configure the advanced options of the desktop, press the button ![](./create_desktop.images/create5.png)

![](./create_desktop.images/create3.png)

You can change the desktop settings in the "Advanced Options" section.

![](./create_desktop.images/create4.png)

Here you can select the viewers you want to use on that desktop, edit your hardware, add a GPU (if you have one), add isos and change your photo.

![](./create_desktop.images/create6.png)





