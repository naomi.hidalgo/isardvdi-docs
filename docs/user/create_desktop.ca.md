# Crear Escriptori

Hi ha dos tipus d'escriptoris:

* **Temporal**: Com el seu nom indica és un escriptori "temporal", això vol dir que una vegada arrencat un escriptori, quan es tanqui es perdrà tot el que s'hagi guardat o descarregat.
* **Persistent**: És un escriptori que no s'esborra, quan s'apagui, no es perdrà la informació o programes que s'hagin instal·lat. 


## Vista principal

Per a crear un escriptori es prem el botó ![](./create_desktop.ca.images/crear1.png)

![](./create_desktop.ca.images/crear2.png)

S'emplenen els camps, se selecciona una plantilla i si no es vol configurar les opcions avançades de l'escriptori, es prem el botó ![](./create_desktop.ca.images/crear6.png)

![](./create_desktop.ca.images/crear3.png)

Es pot canviar la configuració de l'escriptori en l'apartat de "Opcions avançades)

![](./create_desktop.ca.images/crear4.png)

Aquí es pot seleccionar els visors que es vulguin utilitzar en aquest escriptori, editar el seu maquinari, afegir una GPU (si té), afegir isos i canviar-li la foto.

![](./create_desktop.ca.images/crear5.png)