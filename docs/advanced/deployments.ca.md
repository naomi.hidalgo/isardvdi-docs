# Desplegaments

Els desplegaments són escriptoris creats per a altres usuaris.

## Com crear desplegaments

Per a crear un desplegament, s'ha d'anar a la barra superior i es prem el botó ![](./deployments.ca.images/deployments1.png)

![](./deployments.ca.images/deployments2.png)

Es prem el botó ![](./deployments.ca.images/deployments3.png)

![](./deployments.ca.images/deployments4.png)

Es redirigeix a aquest formulari; on es pot emplenar amb el nom del desplegament, el de l'escriptori i una petita descripció. S'ha de seleccionar una plantilla de la qual volem crear els nostres escriptoris. Per a fer-los visible als usuaris es prem el botó ![](./deployments.ca.images/deployments5.png)

![](./deployments.ca.images/deployments6.png)


## Com iniciar-los/parar-los

### Iniciar-los

S'ha d'entrar dins del desplegament.

![](./deployments.ca.images/deployments7.png)

Per a iniciar un escriptori es prem el botó ![](./deployments.ca.images/deployments8.png)

![](./deployments.ca.images/deployments9.png)

Una vegada iniciat, detectarà l'IP de l'escriptori (si té), l'Estat (Si està iniciat o aturat), i es pot seleccionar el tipus de visor

![](./deployments.ca.images/deployments10.png)

Es pot seleccionar el visor que interessi prement el menú despleglable ![](./deployments.ca.images/deployments11.png)


### Parar-los

S'ha d'entrar dins del desplegament.

![](./deployments.ca.images/deployments7.png)

#### Individualment

Per a parar un escriptori es prem el botó ![](./deployments.ca.images/deployments14.png)

![](./deployments.ca.images/deployments15.png)


#### Tots

Per a aturar tots els escriptoris es prem el botó ![](./deployments.ca.images/deployments16.png)

![](./deployments.ca.images/deployments17.png)


## Com anar al Videowall

Cal estar dins del desplegament per poder prémer el botó ![](./deployments.ca.images/deployments18.png)

![](./deployments.ca.images/deployments19.png)

I es redirigeix a aquesta pàgina on es poden veure els escriptoris dels usuaris.

![](./deployments.ca.images/deployments20.png)

Per a interactuar amb un escriptori es prem el botó ![](./deployments.ca.images/deployments21.png)

I et redirigeix 

![](./deployments.ca.images/deployments22.png)


## Com fer visible/invisible

### Visible

Es prem el botó ![](./deployments.ca.images/deployments23.png)

![](./deployments.ca.images/deployments24.png)

Apareixerà una finestra emergent 

![](./deployments.ca.images/deployments25.png)

Una vegada seleccionat "Visible", el desplegament quedarà en verd

![](./deployments.ca.images/deployments29.png)


### Invisible

Es prem el botó ![](./deployments.ca.images/deployments26.png)

![](./deployments.ca.images/deployments27.png)

Apareixerà una finestra emergent 

![](./deployments.ca.images/deployments28.png)

Una vegada seleccionat "Invisible", el desplegament quedarà en vermell

![](./deployments.ca.images/deployments30.png)


## Com recrear els escriptoris

Aquesta funció serveix per a recrear els escriptoris eliminats del desplegament.

Es prem el botó ![](./deployments.ca.images/deployments31.png)

![](./deployments.ca.images/deployments32.png)

![](./deployments.ca.images/deployments33.png)


## Com descarregar-se el fitxer de visors directes

Aquest fitxer serveix per a veure el llistat d'usuaris dins del propi desplegament.

Per a obtenir aquest fitxer, es prem el botó ![](./deployments.ca.images/deployments34.png)

I apareix una finestra emergent

![](./deployments.ca.images/deployments35.png)

En aquest arxiu apareix un excel amb el nom d'usuari, el seu nom, el seu correu i un enllaç que redirigeix l'escriptori de l'usuari.

![](./deployments.ca.images/deployments36.png)


## Com eliminar un desplegament

Per a eliminar un desplegament es prem el botó ![](./deployments.ca.images/deployments37.png)

![](./deployments.ca.images/deployments38.png)