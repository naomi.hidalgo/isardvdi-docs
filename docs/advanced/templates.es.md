# Plantillas

## Crear 

Para crear una plantilla a partir de un escritorio se pulsa el icono ![](./templates.es.images/templates1.png)

![](./templates.es.images/templates2.png)

Y el icono ![](./templates.es.images/templates3.png)

![](./templates.es.images/templates4.png)

Se le puede asignar cualquier nombre/descripción, escoger si se quiere habilitar/deshabilitar (hacerla visible/invisible), y compartirla con grupos/usuarios

![](./templates.es.images/templates5.png)


## Tus Plantillas

Para ver las plantillas que has creado, se pulsa el botón ![](./templates.es.images/templates6.png)

![](./templates.es.images/templates7.png)

![](./templates.es.images/templates8.png)


## Compartidas contigo

En este apartado se pueden ver las plantillas que han sido compartidas con tu usuario.

![](./templates.es.images/templates9.png)


## Editar

Para editar una plantilla se pulsa el icono ![](./templates.es.images/templates10.png)

![](./templates.es.images/templates11.png)

Y te redirecciona a la página donde poder editar la información del escritorio, visores, hardware, media, etc.

![](./templates.es.images/templates12.png)


## Compartir

Para compartir una plantilla se pulsa el icono ![](./templates.es.images/templates13.png)

![](./templates.es.images/templates14.png)

Aparecerá una ventana de diálogo donde se puede compartir con grupos/usuarios

![](./templates.es.images/templates15.png)


## Hacer Visible/Invisible


### Visible

Para hacer visible una plantilla para los usuarios, se pulsa el icono ![](./templates.es.images/templates18.png)

![](./templates.es.images/templates19.png)


### Invisible

Para hacer invisible una plantilla para los usuarios, se pulsa el icono ![](./templates.es.images/templates16.png)

![](./templates.es.images/templates17.png) 