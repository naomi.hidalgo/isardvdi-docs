# Visor directo

Esto sirve para compartir un escritorio desde un enlace.

Se pulsa el botón ![](./direct_viewer.es.images/visor_directo1.png) del escritorio que se quiera compartir por enlace

![](./direct_viewer.es.images/visor_directo2.png)

Y a continuación se pulsa el botón ![](./direct_viewer.es.images/visor_directo3.png)

![](./direct_viewer.es.images/visor_directo4.png)

Saldrá una ventana de diálogo donde se tiene que seleccionar la casilla para que aparezca el enlace y poder copiarlo

![](./direct_viewer.es.images/visor_directo5.png)

