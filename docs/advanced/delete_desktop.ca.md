# Esborrar escriptoris

Per a esborrar un escriptori es prem el botó ![](./delete_desktop.es.images/delete_desktop1.png)

![](./delete_desktop.ca.images/delete_desktop1.png)

Després es prem el botó ![](./delete_desktop.es.images/delete_desktop3.png)

![](./delete_desktop.ca.images/delete_desktop2.png)

I apareixerà una finestra de diàleg conforme es vol eliminar l'escriptori

![](./delete_desktop.ca.images/delete_desktop3.png)

