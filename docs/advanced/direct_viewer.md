# Direct viewer

This is for sharing a desktop from a link.

Press the button ![](./direct_viewer.es.images/visor_directo1.png) of the desktop you want to share by link

![](./direct_viewer.images/direct_viewer1.png)

And then press the button ![](./direct_viewer.es.images/visor_directo3.png)

![](./direct_viewer.images/direct_viewer2.png)

A dialog box will appear where you have to select the checkbox so that the link appears and you can copy it

![](./direct_viewer.images/direct_viewer3.png)