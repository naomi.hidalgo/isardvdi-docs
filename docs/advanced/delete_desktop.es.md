# Eliminar escritorios

Para eliminar un escritorio se pulsa el botón ![](./delete_desktop.es.images/delete_desktop1.png)

![](./delete_desktop.es.images/delete_desktop2.png)

Luego se pulsa el botón ![](./delete_desktop.es.images/delete_desktop3.png)

![](./delete_desktop.es.images/delete_desktop4.png)

Y aparecerá una ventana de diálogo conforme se quiere eliminar el escritorio

![](./delete_desktop.es.images/delete_desktop5.png)


