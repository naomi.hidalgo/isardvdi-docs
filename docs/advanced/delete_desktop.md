# Delete desktops

To delete a desktop, press the button ![](./delete_desktop.es.images/delete_desktop1.png)

![](./delete_desktop.images/delete_desktop1.png)

Then press the button ![](./delete_desktop.es.images/delete_desktop3.png)

![](./delete_desktop.images/delete_desktop2.png)

And a dialog box will appear as you want to remove the desktop

![](./delete_desktop.images/delete_desktop3.png)


