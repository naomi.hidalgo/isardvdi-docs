# Plantilles

## Crear 

Per a crear una plantilla a partir d'un escriptori es prem la icona ![](./templates.es.images/templates1.png)

![](./templates.ca.images/templates1.png)

I la icona ![](./templates.es.images/templates3.png)

![](./templates.ca.images/templates2.png)

Se li pot assignar qualsevol nom/descripció, triar si es vol habilitar/deshabilitar (fer-la visible/invisible), i compartir-la amb grups/usuaris

![](./templates.ca.images/templates3.png)


## Les teves Plantilles

Per a veure les plantilles que has creat, es prem el botó ![](./templates.ca.images/templates13.png)

![](./templates.ca.images/templates4.png)

![](./templates.ca.images/templates5.png)


## Compartides amb tu

En aquest apartat es poden veure les plantilles que han estat compartides amb el teu usuari.

![](./templates.ca.images/templates6.png)


## Editar

Per a editar una plantilla es prem la icona ![](./templates.es.images/templates10.png)

![](./templates.ca.images/templates7.png)

I et redirigeix a la pàgina on poder editar la informació de l'escriptori, visors, maquinari, mitjana, etc.

![](./templates.ca.images/templates8.png)


## Compartir

Per a compartir una plantilla es prem la icona ![](./templates.es.images/templates13.png)

![](./templates.ca.images/templates9.png)

Apareixerà una finestra de diàleg on es pot compartir amb grups/usuaris

![](./templates.ca.images/templates10.png)


## Fer Visible/Invisible


### Visible

Per a fer visible una plantilla per als usuaris, es prem la icona ![](./templates.es.images/templates18.png)

![](./templates.ca.images/templates11.png)


### Invisible

Per a fer invisible una plantilla per als usuaris, es prem la icona ![](./templates.es.images/templates16.png)

![](./templates.ca.images/templates12.png) 