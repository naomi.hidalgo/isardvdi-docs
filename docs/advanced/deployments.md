# Deployments

Displays are desktops created for other users.

## How to create deployments

To create a deployment, you must go to the top bar and press the button ![](./deployments.images/deployments1.png)

![](./deployments.images/deployments2.png)

Press the button ![](./deployments.images/deployments3.png)

![](./deployments.images/deployments4.png)

It is redirected to this form; where it can be filled with the name of the deployment, the name of the desktop and a short description. A template must be selected from which we want to create our desktops. To make them visible to users, press the button ![](./deployments.images/deployments5.png)

![](./deployments.images/deployments6.png)


## How to start/stop them

### Start

You have to get inside the deployment.

![](./deployments.images/deployments7.png)

To start a desktop, press the button ![](./deployments.images/deployments8.png)

![](./deployments.images/deployments9.png)

Once started, it will detect the "IP" of the desktop (if it has one), the "State" (If it is started or stopped), and the type of viewer can be selected

![](./deployments.images/deployments10.png)

You can select the viewer you are interested in by pressing the drop-down menu ![](./deployments.images/deployments11.png)


### Stop

You have to get inside the deployment.

![](./deployments.images/deployments7.png)

#### Individually

To stop a desktop, press the button ![](./deployments.images/deployments13.png)

![](./deployments.images/deployments14.png)


#### All

To stop all desktops, press the button ![](./deployments.ca.images/deployments16.png)

![](./deployments.images/deployments15.png)


## How to go to Videowall

You need to be inside the deployment to be able to press the button ![](./deployments.ca.images/deployments18.png)

![](./deployments.images/deployments16.png)

And you are redirected to this page where you can see the users' desktops.

![](./deployments.images/deployments17.png)

To interact with a desktop, press the button ![](./deployments.ca.images/deployments21.png)

And redirects you to ![](./deployments.images/deployments18.png)


## How to make visible/invisible

### Visible

Press the button ![](./deployments.ca.images/deployments23.png)

![](./deployments.images/deployments19.png)

A dialog box will appear

![](./deployments.images/deployments20.png)

Once "Visible" is selected, the deplpoyment will turn green

![](./deployments.images/deployments21.png)


### Invisible

Press the button ![](./deployments.ca.images/deployments26.png)

![](./deployments.images/deployments22.png)

A dialog box will appear

![](./deployments.images/deployments23.png)

Once "Invisible" is selected, the deployment will turn red

![](./deployments.images/deployments24.png)


## How to recreate desktops

This function is used to recreate desktops removed from the deployment.

Press the button ![](./deployments.ca.images/deployments31.png)

![](./deployments.images/deployments25.png)

![](./deployments.images/deployments26.png)


## How to download the direct viewer file

This file is used to see the list of users within the deployment itself.

To get this file, press the button ![](./deployments.ca.images/deployments34.png)

A dialog box will appear

![](./deployments.images/deployments27.png)

An excel file appears in this file with the user's name, their name, their email and a link that redirects the user's desktop.

![](./deployments.ca.images/deployments36.png)


## How to delete a deployment

To delete a deployment, press the button ![](./deployments.ca.images/deployments37.png)

![](./deployments.images/deployments28.png)