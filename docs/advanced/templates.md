# Templates

## Create 

To create a template from a desktop, press the icon ![](./templates.es.images/templates1.png)

![](./templates.images/templates1.png)

And the icon ![](./templates.es.images/templates3.png)

![](./templates.images/templates2.png)

You can assign any name/description, choose whether to enable/disable it (make it visible/invisible), and share it with groups/users

![](./templates.images/templates3.png)


## Your Templates

To see the templates you have created, press the button ![](./templates.images/templates13.png)

![](./templates.images/templates4.png)

![](./templates.images/templates5.png)


## Shared with you

In this section you can see the templates that have been shared with your user.

![](./templates.images/templates6.png)


## Edit

To edit a template, press the icon ![](./templates.es.images/templates10.png)

![](./templates.images/templates7.png)

And it redirects you to the page where you can edit the desktop information, viewers, hardware, media, etc.

![](./templates.images/templates8.png)


## Share

To share a template, press the icon ![](./templates.es.images/templates13.png)

![](./templates.images/templates9.png)

A dialog window will appear where you can share with groups/users

![](./templates.images/templates10.png)


## Make Visible/Invisible


### Visible

To make a template visible to users, press the icon ![](./templates.es.images/templates18.png)

![](./templates.images/templates11.png)


### Invisible

To make a template invisible to users, press the icon ![](./templates.es.images/templates16.png)

![](./templates.images/templates12.png) 