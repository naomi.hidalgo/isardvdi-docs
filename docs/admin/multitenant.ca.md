# Tenencia múltiple

Les quotes poden limitar els recursos utilitzats per cada rol, categoria, grup o usuari individualment per a diferents articles.


## Introducció

La instal·lació multiusuari de IsardVDI permet que diverses organitzacions/empreses comparteixin la mateixa instal·lació de IsardVDI. És a dir, la mateixa infraestructura ara es pot *dividir* per *límits* en una infraestructura compartida, on els *manager* d'aquesta organització poden administrar tot dins dels límits definits.

![](../images/admin/mt_description.png)

Això permet un nou nivell de control ja que les categories i grups en categories poden tenir límits establerts (usuaris, escriptoris, vCPUs, ...).


## Configuració

En el panell d'Administració, en l'apartat de [**usuaris**](/admin/users.ca/) es pot administrar categories que poden tenir un **usuari manager** amb control total sobre aquesta categoria.

Per a crear una nova categoria amb límits d'ús de recursos i registre automàtic d'usuaris (github/google) com a usuari [administrador](index.ca.md):

- **Afegir nova categoria** i posar-li un nom. Per exemple, es crea la categoria *ABCD*.

![](./users.images/users14.png)

També s'agregarà als grups un nou grup Principal* amb aquesta categoria principal.

![](./users.images/users15.png)

Es pot crear tants grups en aquesta categoria com es desitgi, com [Managers](/manager/manage_user.ca/#creacio-de-grups).

- Per a **establir límits de categoria** s'obre els detalls de la categoria i es prem el botó de *Límits*.

![](./users.images/users16.png)

Es pot establir aquí *quotes* i *límits* per a usuaris en aquesta categoria com per [Grups](/manager/manage_user.ca/#quotas-i-limits).

- Per a [**agregar un nou usuari manager**](/manager/manage_user.ca/#afegir-usuaris), s'ha d'anar a la secció de "User" en el panell d'administrador sent usuari "Manager"/"Admin"

S'ha d'assegurar d'establir la seva funció en "role" com *manager*, la categoria en **ABCD** i el grup en **ABCD principal**

![](./users.images/users17.png)

- Per a **obtenir codis OAuth de registre automàtic**, [el pot fer un usuari "manager"](/manager/manage_user.ca/#clau-dautoregistre).

S'ha configurat la instància *ABCD* amb un [manager](../manager/index.ca.md) i se li han establert uns límits.