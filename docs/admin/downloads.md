# Downloads

In this section you will find the resources already downloaded from the IsardVDI cloud service.

To go to this section, press the button ![](downloads.images/downloads1.png)

![](downloads.images/downloads2.png)

## Domains

Here you can download the desktops that are in the IsardVDI cloud.

![](downloads.images/downloads3.png)

## Media

The isos and floppies provided by IsardVDI are found.

![](downloads.images/downloads4.png)

## Videos

The video resources provided by IsardVDI.

![](downloads.images/downloads5.png)

## OS Hardware Templates

Here you can find the templates that are downloaded by default in IsardVDI for various operating systems (virt-install)

![](downloads.images/downloads6.png)

## viewers

Here you can download the Spice viewers for Windows.

![](downloads.images/downloads7.png)

