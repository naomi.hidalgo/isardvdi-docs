# Domains

Para ir a la sección de "Domains", se pulsa el botón ![](./users.es.images/users1.png)

![](./users.es.images/users2.png)

Y se pulsa el menú desplegable ![](./domains.images/domains1.png)

![](./domains.images/domains2.png)


## Desktops

En esta sección se pueden ver todos los escritorios que se hayan creado de todas las categorías.

![](./domains.images/domains4.png)

Si se pulsa el icono ![](./domains.images/domains5.png), se puede ver más información del escritorio.

* Hardware: como su nombre indica, es el hardware que se le ha asignado a ése escritorio

* Template tree: indica de dónde derivan los escritorios

![](./domains.images/domains6.png)


## Templates

En esta sección se pueden ver todas las plantillas que se hayan creado de todas las categorías.

![](./domains.images/domains7.png)

Si se pulsa el icono ![](./domains.images/domains5.png), se puede ver más información de la plantilla.

* Hardware: como su nombre indica, es el hardware que se le ha asignado a esa plantilla

* Allows: indica a quien se le ha dado permisos para utilizar la plantilla

![](./domains.images/domains8.png)


## Media

En esta sección se pueden ver todos los archivos media que se hayan subido de todas las categorías.

![](./domains.images/domains9.png)


## Resources

En esta sección se pueden ver todos los recursos que tiene la instalación, éstos saldrán al crear un escritorio/plantilla.

![](./domains.images/domains10.png)


### Graphics

En esta sección se puede añadir diferentes tipos de visores con los que se pueden ver un escritorio.

Para añadir un visor nuevo, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains12.png)

Y se rellena el formulario

![](./domains.images/domains13.png)


### Videos

En esta sección se pueden añadir diferentes formatos de vídeo.

Para añadir un nuevo formato, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains14.png)

Y se rellena el formulario

![](./domains.images/domains15.png)

(heads -> cuantos monitores quieres)

### Interfaces

En esta sección se pueden añadir redes privadas a los escritorios.

Para añadir una red, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains16.png)

Y se rellena el formulario

* Type: Bridge , Network, OpenVSwitch, Personal

    - Bridge: Enlaza con un puente hacia una red del servidor. En el servidor se puede tener interfaces que enlacen por ejemplo con vlans hacia un troncal de tu red, y se puede mapear estas interfaces en isard y conectarlas a los escritorios.

    Para poder mapear la interfaz dentro del hipervisor, en el archivo de isardvdi.conf se tiene que modificar esta línea:

    ```
    # ------ Trunk port & vlans --------------------------------------------------
    ## Uncomment to map host interface name inside hypervisor container.
    ## If static vlans are commented then hypervisor will initiate an 
    ## auto-discovery process. The discovery process will last for 260
    ## seconds and this will delay the hypervisor from being available.
    ## So it is recommended to set also the static vlans.
    ## Note: It will create VlanXXX automatically on webapp. You need to
    ## assign who is allowed to use this VlanXXX interfaces.
    #HYPERVISOR_HOST_TRUNK_INTERFACE=

    ## This environment variable depends on previous one. When setting
    ## vlans number comma separated it will disable auto-discovery and
    ## fix this as forced vlans inside hypervisor.
    #HYPERVISOR_STATIC_VLANS=
    ```


* Model: rtl8931, virtio, e1000

    - Es más eficiente utilizar virtio (que es una interface paravirtualizada), mientras que las e1000 o rtl son simulaciones de tarjetas, y va más lento aunque es más compatible con sistemas operativos antiguos y no se necesita instalar drivers en el caso de windows.

    **Si se tiene un sistema operativo moderno, con el virtio funciona, sino con los otros que son interfaces. Windows antiguos: rtl, e1000**


* QoS: limit up and down to 1 Mbps, unlimited

![](./domains.images/domains17.png)


### Boots

En esta sección se indican los diferentes modos de arranque de un escritorio.

**Si no se tiene permisos no se puede seleccionar una iso de arranque y no se deja por defecto que los usuarios se puedan mapear isos.**

Para dar permisos se pulsa en el icono ![](./domains.images/domains25.png)

![](./domains.images/domains18.png)


### Network QoS

En esta sección se indican las limitaciones que se pueden poner a las redes.

Para añadir una limitación, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains19.png)

Y se rellena el formulario

![](./domains.images/domains20.png)


### Disk QoS

En esta sección se indican las limitaciones que se pueden poner a los discos.

Para añadir un límite de disco, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains21.png)

Y se rellena el formulario

![](./domains.images/domains22.png)


### Remote VPNs

En esta sección se pueden añadir redes remotas a los escritorios.

Para añadir una red remota, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains23.png)

Y se rellena el formulario

![](./domains.images/domains24.png)


## Bookables



### Priority

En esta sección se pueden crear prioridades dependiendo de lo que se necesite en ese momento. Aquí se pueden crear reglas con un determinado tiempo máximo de reserva, número de escritorios, etc.

![](./domains.images/domains26.png)

Para añadir una prioridad nueva, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains27.png)

Y saldrá una ventana de diálogo donde se pueden rellenar los campos, también per,ite compartir con otros roles, categorías, grupos o usuarios:

* **Name**: El nombre que se le quiere dar a la prioridad

* **Description**: Una pequeña descripción para poder saber un poco más de información sobre esa prioridad

* **Rule**: Es la regla que le damos a esa prioridad, esto se aplica en el apartado de "Resources". Es importante tener en cuenta que si se quiere aplicar más de una regla a una prioridad, que se llamen iguales.

* **Priority**: Es la prioridad que se le da a la regla; cuánto mayor sea el número, más prioridad tendrá y cuánto menor sea el número, menos tendrá.

* **Forbid time**: Es el tiempo de antelación que no se pueden hacer reservas.

* **Max time**: Es el tiempo máximo de una reserva.

* **Max items**: Son el máximo de escritorios que se permite tener con dicha regla.

![](./domains.images/domains28.png)


#### Compartir 

Si se quiere compartir una prioridad con un rol, categoría, grupo o usuario se pulsa el icono ![](./domains.images/domains29.png)

![](./domains.images/domains30.png)

Y se rellena el formulario

![](./domains.images/domains31.png)


#### Editar

Para editar una prioridad, se pulsa el icono ![](./domains.images/domains32.png)

![](./domains.images/domains33.png)

Aparece una ventana de diálogo donde se pueden editar los campos:

![](./domains.images/domains34.png)


### Resources

En esta sección se puede encontrar todos los perfiles de GPUs que se hayan seleccionado en el apartado de "Hypervisores"

Aquí se le pueden aplicar las prioridades/reglas que se hayan creado anteriormente.

![](./domains.images/domains35.png)


#### Compartir

Para compartir un recurso, se pulsa el icono ![](./domains.images/domains29.png)

![](./domains.images/domains36.png)

Y se rellena el formulario

![](./domains.images/domains37.png)


#### Editar

Para editar un recurso, se pulsa el icono ![](./domains.images/domains32.png)

![](./domains.images/domains38.png)

Aparece una ventana de diálogo donde se pueden editar los campos:

![](./domains.images/domains39.png)


### Events 

En esta sección se pueden ver los eventos que se generan derivados de las acciones de las gpus.