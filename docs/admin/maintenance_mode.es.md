# Modo de mantenimiento

El modo de mantenimiento desactiva las interfaces web para los usuarios con roles de managers, avanzados y de usuario.

El modo de mantenimiento se puede habilitar y deshabilitar a través de la sección de [configuración](./config.es.md) del panel de administración por parte de un usuario con función de administrador.

El administrador del sistema puede crear un archivo llamado `/opt/isard/config/maintenance` para cambiar al modo de mantenimiento al inicio. El archivo se elimina una vez que el sistema cambia al modo de mantenimiento.

