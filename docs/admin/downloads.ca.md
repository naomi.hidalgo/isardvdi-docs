# Downloads

En aquesta secció es troba els recursos ja descarregats del servei cloud d'IsardVDI.

Per anar a aquesta secció, es prem el botó de ![](downloads.images/downloads1.png)

![](downloads.images/downloads2.png)

## Domains

Aquí es poden descarregar els escriptoris que són al núvol d'IsardVDI.

![](downloads.images/downloads3.png)

## Media

Es troben els isos i floppys que proporciona IsardVDI.

![](downloads.images/downloads4.png)

## Videos

Els recursos de vídeo que proporciona IsardVDI.

![](downloads.images/downloads5.png)

## OS Hardware Templates

Plantilles que vénen descarregades per defecte a IsardVDI per a diversos sistemes opertatius (virt-install)

![](downloads.images/downloads6.png)

## Viewers

Aquí es poden descarregar els visors de Spice per a Windows.

![](downloads.images/downloads7.png)