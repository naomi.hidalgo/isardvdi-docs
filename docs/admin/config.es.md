# Config

Para ir a la sección de "Config", se pulsa el botón ![](./users.es.images/users1.png)

![](./users.es.images/users2.png)

Y se pulsa el botón ![](./config.images/config1.png)

![](./config.images/config2.png)


## Job Scheduler

En este apartado se puede programar un "trabajo", puede ser una copia de seguridad o 

* Type: Cron (que haga el trabajo a una hora determinada), interval (que haga el trabajo cada x tiempo)

* Hour: indica la hora

* Minute: indica el minuto

* Action: backup database, check ephimeral domain status


## Backup database

En esta sección se pueden realizar copias de seguridad de la base de datos del sistema.

![](./config.images/config9.png)

* Se puede subir una copia de seguridad que ya se haya realizado. Para ello, se pulsa el botón ![](./config.images/config10.png) y se arrastra el archivo a la ventana diálogo:

![](./config.images/config11.png)

* Para crear una nueva copia de seguridad se pulsa el botón ![](./config.images/config12.png)

![](./config.images/config13.png)

* Para eliminar una copia de seguridad se pulsa el botón ![](./config.images/config14.png)

![](./config.images/config18.png)

* Para restaurar una copia de seguridad se pulsa el botón ![](./config.images/config15.png)

![](./config.images/config19.png)

* Para ver la información de la copia de seguridad se pulsa el botón ![](./config.images/config16.png)

![](./config.images/config20.png)

* Para descargarse el archivo de la copia de seguridad se pulsa el botón ![](./config.images/config17.png)

![](./config.images/config21.png)


## Maintenance mode

El modo de mantenimiento desactiva las interfaces web para los usuarios con roles de manager, advanced y de user.

Para habilitar y deshabilitar esta función, se marca la casilla

![](./config.images/config6.png)

Una vez marcada la casilla, cuando los usuarios intenten iniciar sesión a su cuenta, les aparecerá un mensaje de mantenimiento

![](./config.images/config7.png)

![](./config.images/config8-es.png)
