# Users

## Crear categorías

Se pulsa el botón ![](./users.es.images/users1.png) para ir al panel de Administración

![](./users.es.images/users2.png)

En el apartado de "Users", se busca donde ponga "Categories" y se pulsa el botón ![](./users.images/users3.png)

![](./users.images/users4.png)

Aparecerá una ventana de diálogo dónde se rellenan los campos del formulario.

![](./users.images/users5.png)

Hay tres opciones que se pueden aplicar al crear una categoría:

* Frontend dropdown show: Que en la página de inicio de sesión, aparezca la categoría creada en el menú desplegable

![](./users.es.images/users3.png)

* Auto desktops creation: Se creará un escritorio cuando inicie sesión un usuario con la plantilla que se escoja (en este ejemplo con "Windows 10 PRO base")

![](./users.images/users7.png)

* Ephemeral desktops: Para poder poner un tiempo limitado de uso a un escritorio temporal. (Para esto también se tiene que configurar en la sección de "Config" el "Job Scheduler")

![](./users.images/users8.png)


## Acceso mediante OAuth

Para poder acceder mediante Google desde una cuenta externa, se va al panel de "Administración" en el apartado "Users".

Y en el apartado de "Categorías" se pulsa el botón ![](./users.images/users9.png) de la categoría la cuál queramos darle acceso.

![](./users.images/users10.png)

Se pulsa el botón ![](./users.images/users11.png)

![](./users.images/users12.png)

Y aparece una ventana de diálogo con un formulario. Se rellena el campo de "Allowed email domain for foreign account like Google" con el dominio del correo.

![](./users.images/users13.png)
