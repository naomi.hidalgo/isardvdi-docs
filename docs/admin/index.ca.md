# Introducció

Els usuaris d'IsardVDI estan classificats en rols. Aquesta secció descriu funcions que poden ser utilitzades per usuaris amb rol administrador.

Els usuaris amb **rol d'administrador** tenen les funcionalitats de tots els [usuaris](../user/index.ca.md), [avançats usuaris](../advanced/index.ca.md), [usuaris mànager](../manager/index.ca.md) i les descrites en aquesta secció.
