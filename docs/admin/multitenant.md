# Multitenancy

Quotas can limit the resources used by each role, category, group, or user individually for different items.


## Introducción

IsardVDI multi-user installation allows multiple organizations/companies to share the same IsardVDI installation. The same infrastructure can now be *divided* by *boundaries* into a shared infrastructure, where the *managers* of that organization can manage everything within defined boundaries.

![](../images/admin/mt_description.png)

This allows a new level of control as categories and groups in categories can have limits set (users, desktops, vCPUs, ...).


## Configuration

In the Administration panel, in the section [**users**](/admin/users/) you can manage categories that can have a **manager user** with full control over that category.

To create a new category with resource usage limits and automatic user registration (github/google) as user [admin](index.md):


- **Add new category** and give it a name. For example, the category *ABCD* is created.

  ![](./users.images/users14.png)

  A new *Parent* group with this parent category will also be added to the groups.

  ![](./users.images/users15.png)

  You can create as many groups in that category as you want, like [Managers](/manager/manage_user/#group-creation).

- To **set category limits**, open the category details and press the *Limits* button.

  ![](./users.images/users16.png)

You can set here *quotas* and *limits* for users in this category as per [Groups](/manager/manage_user/#quota-and-limits).

- To [**add a new manager user**](/manager/manage_user/#add-users), you have to go to the "User" section in the administrator panel being the user "Manager"/"Admin"

Make sure to set your role to *manager*, category to **ABCD**, and group to **ABCD main**

![](./users.images/users17.png)

- To **obtain automatic registration OAuth codes**, [can be done by a "manager" user](/manager/manage_user/#enrollment-keys).

The *ABCD* instance has been configured with a [manager](../manager/index.md) and limits have been set for it.