# Maintenance mode

Maintenance mode disables web interfaces for users with manager, advanced and user roles.

Maintenance mode can be enabled and disabled via [configuration](./config.md) section of administration interface by an user with administrator role.

System administrator can create a file named `/opt/isard/config/maintenance` to switch to maintenance mode at start up. The file is removed once system is switched to maintenance mode.
