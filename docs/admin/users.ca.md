# Users

## Crear categories

Es prem el botó ![](./users.ca.images/users1.png) per a anar al panell d'Administració

![](./users.ca.images/users2.png)

En l'apartat de "Users", es busca on posi "Categories" i es prem el botó ![](./users.images/users3.png)

![](./users.images/users4.png)

Apareixerà una finestra de diàleg on s'emplenen els camps del formulari.

![](./users.images/users5.png)

Hi ha tres opcions que es poden aplicar en crear una categoria:

* Frontend dropdown show: Que en la pàgina d'inici de sessió, aparegui la categoria creada en el menú desplegable

![](./users.ca.images/users3.png)

* Auto desktops creation: Es crearà un escriptori quan iniciï sessió un usuari amb la plantilla que es triï (en aquest exemple amb "Windows 10 PRO base")

![](./users.images/users7.png)

* Ephemeral desktops: Per a poder posar un temps limitat d'ús a un escriptori temporal. (Per a això també s'ha de configurar en la secció de "Config" el "Job Scheduler")

![](./users.images/users8.png)


## Accés mitjançant OAuth

Per a poder accedir mitjançant Google des d'un compte extern, es va al panell de "Administració" en l'apartat "Users".

I en l'apartat de "Categories" es prem el botó ![](./users.images/users9.png) de la categoria la qual vulguem donar-li accés.

![](./users.images/users10.png)

Es prem el botó ![](./users.images/users11.png)

![](./users.images/users12.png)

I apareix una finestra de diàleg amb un formulari. S'emplena el camp de "Allowed email domain for foreign account like Google" amb el domini del correu.

![](./users.images/users13.png)