# Domains

To go to the "Domains" section, press the button ![](./users.images/users1.png)

![](./users.images/users2.png)

And press the dropdown menu ![](./domains.images/domains1.png)

![](./domains.images/domains2.png)


## Desktops

In this section you can see all the desktops that have been created from all categories.

![](./domains.images/domains4.png)

If you click the ![](./domains.images/domains5.png) icon, you can see more information about the desktop.

* Hardware: as its name indicates, it is the hardware that has been assigned to that desktop

* Template tree: indicates where the desktops derive from

![](./domains.images/domains6.png)


## Templates

In this section you can see all the templates that have been created from all categories.

![](./domains.images/domains7.png)

If you click the ![](./domains.images/domains5.png) icon, you can see more information about the template.

* Hardware: as its name indicates, it is the hardware that has been assigned to that template

* Allows: indicates who has been given permission to use the template

![](./domains.images/domains8.png)

### Templates options

#### Forced Hypervisor

#### Duplicate

This option will allow to duplicate in database the template while keeping the original template disk. 

This duplication can be done as many times as you want and it is useful in some cases like:

- You want to delete de user's template while keeping the template in system. You can duplicate the user's template and assing this new one to you for example and then delete de user's template.
- You want to share template within other categories. You can then assign the duplicated template to another user in another category and apply sharing permissions to this new one while keeping the old one sharings.

Keep in mind that the original template disk won't be physically deleted till the last template duplicated from this disk is removed from system.


#### Delete

#### Edit

#### XML

## Media

In this section you can see all the media files that have been uploaded from all categories.

![](./domains.images/domains9.png)


## Resources

In this section you can see all the resources that the installation has, these will appear when creating a desktop/template.

![](./domains.images/domains10.png)


### Graphics

In this section you can add different types of viewers with which you can see a desktop.

To add a new viewer, click the button ![](./domains.images/domains11.png)

![](./domains.images/domains12.png)

And the fill out the form

![](./domains.images/domains13.png)


### Videos

In this section you can add different video formats.

To add a new format, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains14.png)

And the fill out the form

![](./domains.images/domains15.png)

(heads -> cuantos monitores quieres)

### Interfaces

In this section you can add private networks to desktops.

To add a network, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains16.png)

And the fill out the form

* Type: Bridge , Network, OpenVSwitch, Personal

    - Bridge: It links with a bridge to a server network. On the server you can have interfaces that link, for example, with vlans to a backbone of your network, and you can map these interfaces in isard and connect them to the desktops.

    In order to map the interface within the hypervisor, this line must be modified in the isardvdi.conf file:

    ```
    # ------ Trunk port & vlans --------------------------------------------------
    ## Uncomment to map host interface name inside hypervisor container.
    ## If static vlans are commented then hypervisor will initiate an 
    ## auto-discovery process. The discovery process will last for 260
    ## seconds and this will delay the hypervisor from being available.
    ## So it is recommended to set also the static vlans.
    ## Note: It will create VlanXXX automatically on webapp. You need to
    ## assign who is allowed to use this VlanXXX interfaces.
    #HYPERVISOR_HOST_TRUNK_INTERFACE=

    ## This environment variable depends on previous one. When setting
    ## vlans number comma separated it will disable auto-discovery and
    ## fix this as forced vlans inside hypervisor.
    #HYPERVISOR_STATIC_VLANS=
    ```


* Model: rtl8931, virtio, e1000

    - It is more efficient to use virtio (which is a paravirtualized interface), while the e1000 or rtl are card simulations, and it is slower, although it is more compatible with older operating systems and it is not necessary to install drivers in the case of windows.

    **If you have a modern operating system, it works with virtio, but with the others that are interfaces. Old windows: rtl, e1000**


* QoS: limit up and down to 1 Mbps, unlimited

![](./domains.images/domains17.png)


### Boots

This section lists the different boot modes for a desktop.

**If you do not have permissions, you cannot select a bootable iso and it is not allowed by default that users can map isos.**

To give permissions, click on the icon ![](./domains.images/domains25.png)

![](./domains.images/domains18.png)


### Network QoS

This section lists the limitations that can be placed on networks.

To add a limitation, click the button ![](./domains.images/domains11.png)

![](./domains.images/domains19.png)

And the fill out the form

![](./domains.images/domains20.png)


### Disk QoS

This section lists the limitations that can be placed on disks.

To add a disk limit, click the button ![](./domains.images/domains11.png)

![](./domains.images/domains21.png)

And the fill out the form

![](./domains.images/domains22.png)


### Remote VPNs

In this section you can add remote networks to desktops.

To add a remote network, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains23.png)

And the fill out the form

![](./domains.images/domains24.png)


## Bookables



### Priority

In this section you can create priorities depending on what is needed at that time. Here you can create rules with a certain maximum reservation time, number of desks, etc.

![](./domains.images/domains26.png)

To add a new priority, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains27.png)

And a dialog box will appear where you can fill in the fields, also allows you to share with other roles, categories, groups or users:

* **Name**: The name that you want to give to the priority

* **Description**: A short description to know a little more information about that priority

* **Rule**: It is the rule that we assign to that priority, this is applied in the "Resources" section. It is important to note that if you want to apply more than one rule to a priority, you need to name them the same.

* **Priority**: It is the priority given to the rule; the higher the number, the more priority it will have and the lower the number, the less it will have.

* **Forbid time**: It is the time in advance that reservations cannot be made.

* **Max time**: It is the maximum time of a reservation.

* **Max items**: These are the maximum number of desks allowed to have this rule.

![](./domains.images/domains28.png)


#### Share

If you want to share a priority with a role, category, group or user, click on the icon ![](./domains.images/domains29.png)

![](./domains.images/domains30.png)

And fill out the form

![](./domains.images/domains31.png)


#### Edit

To edit a priority, click the icon ![](./domains.images/domains32.png)

![](./domains.images/domains33.png)

A dialog box appears where you can edit the fields:

![](./domains.images/domains34.png)


### Resources

In this section you can find all the GPU profiles that have been selected in the "Hypervisors" section

Here you can apply the priorities/rules that have been previously created.

![](./domains.images/domains35.png)


#### Share

To share a resource, click the icon![](./domains.images/domains29.png)

![](./domains.images/domains36.png)

And fill out the form

![](./domains.images/domains37.png)


#### Edit

To edit a resource, click the icon ![](./domains.images/domains32.png)

![](./domains.images/domains38.png)

A dialog box appears where you can edit the fields:

![](./domains.images/domains39.png)


### Events 

In this section you can see the events that are generated derived from the actions of the gpus.



