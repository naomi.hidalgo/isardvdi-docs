# Downloads

En esta sección se encuentran los recursos ya descargados del servicio cloud de IsardVDI.

Para ir a esta sección, se pulsa el botón de ![](downloads.images/downloads1.png)

![](downloads.images/downloads2.png)

## Domains

Aquí se pueden descargar los escritorios que están en la nube de IsardVDI.

![](downloads.images/downloads3.png)

## Media

Se encuentran las isos y floppys que proporciona IsardVDI.

![](downloads.images/downloads4.png)

## Videos

Los recursos de vídeo que proporciona IsardVDI.

![](downloads.images/downloads5.png)

## OS Hardware Templates

Plantillas que vienen descargadas por defecto en IsardVDI para diversos sistemas opertativos (virt-install)

![](downloads.images/downloads6.png)

## Viewers

Aquí se pueden descargar los visores de Spice para Windows.

![](downloads.images/downloads7.png)