# Introduction

Users of IsardVDI are classified in roles. This section describes features that can be used by adminitrator role users.

Users with **administator role** have the features of all [users](../user/index.md), [advanced users](../advanced/index.md), [manager users](../manager/index.md) and those described in this section.
