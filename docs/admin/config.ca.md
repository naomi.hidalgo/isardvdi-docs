# Config

Per a anar a la secció de "Config", es prem el botó ![](./users.ca.images/users1.png)

![](./users.ca.images/users2.png)

I es prem el botó ![](./config.images/config1.png)

![](./config.images/config2.png)


## Job Scheduler

En este apartado se puede programar un "trabajo", puede ser una copia de seguridad o 

* Type: Cron (que haga el trabajo a una hora determinada), interval (que haga el trabajo cada x tiempo)

* Hour: indica la hora

* Minute: indica el minuto

* Action: backup database, check ephimeral domain status


## Backup database

En aquesta secció es poden realitzar còpies de seguretat de la base de dades del sistema.

![](./config.images/config9.png)

* Es pot pujar una còpia de seguretat que ja s'hagi realitzat. Per a això, es prem el botó ![](./config.images/config10.png) i s'arrossega l'arxiu a la finestra diàleg:

![](./config.images/config11.png)

* Per a crear una nova còpia de seguretat es prem el botó ![](./config.images/config12.png)

![](./config.images/config13.png)

* Per a eliminar una còpia de seguretat es prem el botó ![](./config.images/config14.png)

![](./config.images/config18.png)

* Per a restaurar una còpia de seguretat es prem el botó ![](./config.images/config15.png)

![](./config.images/config19.png)

* Per a veure la informació de la còpia de seguretat es prem el botó ![](./config.images/config16.png)

![](./config.images/config20.png)

* Per a descarregar-se l'arxiu de la còpia de seguretat es prem el botó ![](./config.images/config17.png)

![](./config.images/config21.png)


## Maintenance mode

La manera de manteniment desactiva les interfícies web per als usuaris amb rols de manager, advanced i de user.

Per a habilitar i deshabilitar aquesta funció, es marca la casella

![](./config.images/config6.png)

Una vegada marcada la casella, quan els usuaris intentin iniciar sessió al seu compte, els apareixerà un missatge de manteniment

![](./config.images/config7.png)

![](./config.images/config8-ca.png)
