# Tenencia Múltiple

Las cuotas pueden limitar los recursos utilizados por cada rol, categoría, grupo o usuario individualmente para diferentes artículos.


## Introducción

La instalación multiusuario de IsardVDI permite que varias organizaciones/empresas compartan la misma instalación de IsardVDI. Es decir, la misma infraestructura ahora se puede *dividir* por *límites* en una infraestructura compartida, donde los *managers* de esa organización pueden administrar todo dentro de los límites definidos.

![](../images/admin/mt_description.png)

Esto permite un nuevo nivel de control ya que las categorías y grupos en categorías pueden tener límites establecidos (usuarios, escritorios, vCPUs, ...).


## Configuración

En el panel de Administración, en el apartado de [**usuarios**](/admin/users.es/) se puede administrar categorías que pueden tener un **usuario manager** con control total sobre esa categoría.

Para crear una nueva categoría con límites de uso de recursos y registro automático de usuarios (github/google) como usuario [administrador](index.es.md):

- **Añadir nueva categoría** y ponerle un nombre. Por ejemplo, se crea la categoría *ABCD*.

  ![](./users.images/users14.png)

  También se agregará a los grupos un nuevo grupo *Principal* con esta categoría principal.

  ![](./users.images/users15.png)

  Se puede crear tantos grupos en esa categoría como se desee, como [Managers](/manager/manage_user.es/#creacion-de-grupos).

- Para **establecer límites de categoría** se abre los detalles de la categoría y se pulsa el botón de *Límites*.

  ![](./users.images/users16.png)

Se puede establecer aquí *cuotas* y *límites* para usuarios en esta categoría como por [Grupos](/manager/manage_user.es/#cuotas-y-limites).

- Para [**agregar un nuevo usuario manager**](/manager/manage_user.es/#agregar-usuarios), se tiene que ir a la sección de "User" en el panel de administrador siendo usuario "Manager"/"Admin"

Se debe asegurar de establecer su función en "role" como *manager*, la categoría en **ABCD** y el grupo en **ABCD principal**

![](./users.images/users17.png)

- Para **obtener códigos OAuth de registro automático**, [lo puede hacer un usuario "manager"](/manager/manage_user.es/#clave-de-autoregistro).

Se ha configurado la instancia *ABCD* con un [manager](../manager/index.es.md) y se le han establecido unos límites. 
