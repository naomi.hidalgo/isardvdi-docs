# Hypervisors

## Tipus d'hypervisors

### Hipervisor pur

Quan s'edita o es crea un hipervisor, es pot verificar si només serà un hipervisor. En aquest mode, aquest servidor només serà utilitzat per IsardVDI per executar escriptoris virtuals. Aquest servidor no gestionarà operacions de disc virtual.


### Operacions de disc pur

En aquest mode, el servidor només s'utilitzarà per gestionar operacions de disc d'escriptori virtual.


### Hipervisor mixt + operacions de disc

Aquest mode utilitzarà el servidor tant per executar escriptoris virtuals com per executar operacions de disc virtual.

## Hipervisors externs

El mètode recomanat per configurar un nou hipervisor és fer servir la imatge docker isard-generic-hyper, ja que mostrarà tots els serveis necessaris a qualsevol host de Linux amb docker. Es requereixen tres passos:

1. S'instal·la l'hipervisor KVM (es recomana docker)
2. S'afegeixen claus ssh per al nou hipervisor KVM
3. Es crea un nou hipervisor a la interfície d'usuari web d'IsardVDI.


### Instal·lar hipervisor KVM

#### Instal·lació de Docker (recomanat)

La configuració ràpida i senzilla d'un hipervisor extern es pot fer utilitzant el fitxer de composició generat **./build.sh hypervisor** hypervisor.yml. Al Linux net on s'ha instal·lat el servidor que serà l'hipervisor KVM, s'inicia el contenidor:

```
./build.sh hypervisor
docker-compose -f hypervisor.yml up -d
```

**S'ha de configurar el fitxer isardvdi.cfg abans de crear el fitxer hypervisor.yml.**

Principalment, s'hi pot configurar el domini de vídeo letsencrypt (si no, utilitzarà un certificat autogenerat). Això hauria de mostrar un hipervisor Isard complet al vostre servidor.

Si no s'estableix una configuració de letsencrypt al fitxer isardvdi.cfg, cal configurar els mateixos certificats de visors a tots els hipervisors. En aquest cas, cal copiar els fitxers de certificat /opt/isard/certs/viewers/* del host web a la mateixa carpeta als seus hipervisors remots (eliminant els que ja hi són) i es torna a obrir la finestra acoblable de l'hipervisor.

S'executa aquesta ordre per restablir les claus ssh i la contrasenya:

```bash
docker exec -e PASSWORD=<YOUR_ROOT_PASSWORD> isard-hypervisor bash -c '/reset-hyper.sh'
```

Després d'això, es pot continuar afegint claus ssh per a aquest hipervisor al seu contenidor IsardVDI isard-engine i es crea un nou hipervisor a la interfície d'usuari:


```bash
docker exec -e HYPERVISOR=<IP|DNS> -e PASSWORD=<YOUR_ROOT_PASSWD> -e PORT=2022 isard-engine bash -c '/add-hypervisor.sh'; history -d $((HISTCMD-1))
```


#### Manual d'instal·lació

Si es vol utilitzar un altre host com a hipervisor KVM, s'ha d'assegurar que es tingui:

- **libvirtd**:
  - * **servei funcionant**: ```systemctl status libvirtd```. Consulteu amb la vostra distribució com instal·lar un hipervisor KVM complet:

      * https://wiki.debian.org/KVM#Installation
      * https://help.ubuntu.com/community/KVM/Installation
      * https://wiki.centos.org/HowTos/KVM
      * https://wiki.alpinelinux.org/wiki/KVM

    * **certificats habilitats**: els certificats reals a la ruta IsardVDI ```/opt/isard/certs/default/``` s'han de copiar a la nova ruta del servidor KVM ```/etc/pki/libvirt-spice ```. També cal afegir algunes línies a *libvirtd.conf* i *qemu.conf* per fer ús dels certificats:

      * ```bash
        echo "listen_tls = 0" >> /etc/libvirt/libvirtd.conf; 
        echo 'listen_tcp = 1' >> /etc/libvirt/libvirtd.conf;
        echo 'spice_listen = "0.0.0.0"' >> /etc/libvirt/qemu.conf
        echo 'spice_tls = 1' >> /etc/libvirt/qemu.conf
        ```

    * **servidor websocket**: per permetre la connexió als espectadors a través dels navegadors, també cal configurar un servidor websocket. S'hauria de mirar el *start_proxy.py* inclòs als acobladors/hipervisors.

- **Servei sshd en execució i accessible per a l'usuari**: "systemctl status sshd"
- **curl**: S'utilitzarà per obtenir actualitzacions.
- **qemu-kvm**: S'ha de crear un enllaç al qemu-system-x86_64 com aquest ```ln -s /usr/bin/qemu-system-x86_64 /usr/bin/qemu-kvm```


### Afegeix claus ssh per al nou hipervisor

Prèviament cal afegir claus ssh per accedir a aquest hipervisor. Per fer això, es proporciona un script dins del contenidor isard-app:

```bash
docker exec -e HYPERVISOR=<IP|DNS> -e PASSWORD=<YOUR_ROOT_PASSWD> isard-engine bash -c '/add-hypervisor.sh'
```

**Nota**: Hi ha paràmetres opcionals que es poden configurar:

- PORT: Default is *22*
- USER: Default is *root*

Després de fer això, cal anar al menú d'hipervisors i afegir un nou hipervisor amb la mateixa IP/DNS. Engine intentarà connectar-se quan s'habiliti aquest hipervisor.

**Exemple:**

S'ha de tenir en compte que s'ha afegit ``; history -d $((HISTCMD-1))`` a l'ordre per evitar que la contrasenya es mantingui al nostre historial.

```bash
root@server:~/isard# docker exec -e HYPERVISOR=192.168.0.114 -e PASSWORD=supersecret isard_isard-app_1 bash -c '/add-hypervisor.sh'; history -d $((HISTCMD-1))
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/community/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/edge/testing/x86_64/APKINDEX.tar.gz
(1/1) Installing sshpass (1.06-r0)
Executing busybox-1.28.4-r2.trigger
OK: 235 MiB in 127 packages
Trying to ssh into 192.168.0.114...
# 192.168.0.114:22 SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.1
# 192.168.0.114:22 SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.1
# 192.168.0.114:22 SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.1
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/root/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
expr: warning: '^ERROR: ': using '^' as the first character
of a basic regular expression is not portable; it is ignored

/usr/bin/ssh-copy-id: WARNING: All keys were skipped because they already exist on the remote system.
		(if you think this is a mistake, you may want to use -f option)

Hypervisor ssh access granted.
Access to 192.168.0.114 granted and found libvirtd service running.
Now you can create this hypervisor in IsardVDI web interface.
```

Si falla, es desactiva l'hipervisor, s'editen els paràmetres i s'habilita novament. L'engine d'IsardVDI tornarà a verificar la connexió. Es pot veure el missatge d'error en els detalls de l'hipervisor (es prem la icona "+" per veure'n els detalls).

Si l'engine no intenta tornar a connectar-se després d'habilitar el nou hipervisor, cal reiniciar el contenidor de "isard-app":

```bash
docker restart isard_isard-app_1
```

**NOTA**: Probablement es voldrà eliminar l'hipervisor isard predeterminat després de crear un nou hipervisor extern. Per això, es deshabilita "isard-hypervisor" i s'elimina des dels detalls de l'hipervisor. Després es pot fer l'acció *Forçar eliminació* i es torna a reiniciar el contenidor de l'aplicació isard.


### Afegir hipervisor a la interfície d'usuari web

Es pot afegir un hipervisor extern a la interfície d'usuari web com a [usuari administrador](/admin/index.ca/). Per fer-ho cal anar al panell d'administració i a l'apartat de "Hypervisors", es prem el botó "Add new"

![](./hypervisors.images/hyper2.png)

I apareixerà una finestra de diàleg on es poden emplenar els camps. Es pot configurar qualsevol cosa al camp ID per identificar aquest hipervisor, però cal anar amb compte amb altres camps de configuració, ja que han de coincidir amb l'hipervisor remot:

![](./hypervisors.images/hyper1.png)

El nom de host ha de ser la IP/DNS accessible des d'aquesta instal·lació d'IsardVDI. Podria ser una xarxa interna entre tots dos hosts. L'usuari serà *root* i el port ssh **2022** ja que es deixa el port ssh predeterminat 22 per a l'accés ssh al host.

Es recomana activar les opcions de "disk operations" i "hypervisor" i configurar el host web per a aquest servidor IP/DNS d'instal·lació web i el servidor intermediari de vídeo per a l'IP/DNS accessible externament per a l'hipervisor (accés als seus visors a través dels ports 80 i 443).

S'estableix el nom del host de l'hipervisor del proxy de vídeo a isard-hypervisor, ja que és el nom del contenidor per als valors predeterminats de l'hipervisor.


## VLANS en hipervisor

Per connectar una interfície troncal amb definicions de vlan dins de l'hipervisor, cal configurar (i descomentar) les variables al fitxer isardvdi.cfg:

**NOTA IMPORTANT**: Recordar refer un build del docker-compose emetent l'ordre ```./build.sh``` novament.

- **HYPERVISOR_HOST_TRUNK_INTERFACE:** Aquest és el nom de la interfície troncal de la xarxa del host com es veu al host. Per exemple: eth1

  Si no es configura HYPERVISOR_STATIC_VLANS, l'hipervisor detectarà automàticament les vlans durant 260 segons cada cop que s'inici el contenidor isard-hypervisor. Per tant, és millor definir les VLANs estàtiques que se sàpiguen que estan al tronc.

- **HYPERVISOR_STATIC_VLANS**: S'ha d'establir una llista de números de vlan separats per comes. Configurar això evitarà la detecció automàtica de VLAN durant 260 segons.

Això afegirà a la base de dades les VLANs trobades com a 'vXXX', on XXX és el número de VLAN trobat. Perquè això funcioni, cal verificar que es pugui accedir a STATS_RETHINKDB_HOST al port 28015 des de l'hipervisor.

Es comprova que el que conté l'isard-db tingui el nom de host correcte. Això només és necessari si l'hipervisor es té en una altra màquina:

- **STATS_RETHINKDB_HOST**: S'ha d'establir al host isard-db correcte si es té l'hipervisor en una altra màquina. No cal si es té un IsardVDI 'tot en un'.

NOTA: La interfície del host ha d'estar en mode promissor: *ip link set dev ethX promic on*. Si s'inicia el contenidor isard-hypervisor, cal aturar-lo i tornar-lo a iniciar.


## Conceptes d'infraestructura

Quan s'afegeix un hipervisor extern, cal tenir en compte (i configurar segons calgui) el següent:

- **Ruta dels discos**: Per defecte emmagatzemarà els discs a /opt/isard. No cal crear aquesta carpeta, però es recomana muntar l'emmagatzematge d'E/S ràpid (com el disc nvme) en aquesta ruta.

- **Rendiment de la xarxa**: Si s'utilitzarà un NAS cal tenir en compte que cal utilitzar una xarxa ràpida. Velocitat de xarxa recomanada superior a 10 Gbps.


### Rendiment dels discos

Com que s'utilitzen diferents rutes per a bases, plantilles i discs de grups, es podria millorar el rendiment muntant diferents discs nvme d'emmagatzematge a cada ruta:

- **/opt/isard/bases**: Allí residiran les imatges base que es creïn. En general, aquestes imatges de disc s'utilitzaran com a base per a moltes plantilles i discs dusuari (grups), de manera que l'emmagatzematge ha de proporcionar un accés de lectura el més ràpid possible.

- **/opt/isard/templates**: les plantilles també s'utilitzaran com a discs principals per a diversos discs d'usuari (grups). Així que millor utilitzar un disc d'accés de lectura ràpida.

- **/opt/isard/groups**: els escriptoris en execució residiran aquí. Per tant, aquí es faran moltes E/S simultànies, per la qual cosa l'emmagatzematge ha de proporcionar la major quantitat d'IOPS possible. Els discos NVME (o fins i tot les incursions NVME) brindaran el millor rendiment.


### Rendiment de la xarxa

La xarxa d'emmagatzematge entre els hipervisors i els servidors d'emmagatzematge NAS ha de ser com a mínim 10 Gbps. Tots els hipervisors s'han de muntar l'emmagatzematge a la ruta **/isard** i aquest muntatge ha de ser el mateix per a tots els hipervisors.

Com es pot triar en crear un hipervisor que sigui un **hipervisor pur**, **operacions de disc pur** o un **mixt**, es pot afegir servidors d'emmagatzematge NAS com a operacions de disc pur. Això portarà una manipulació més ràpida dels discos, ja que IsardVDI utilitzarà l'emmagatzematge directe i les ordres a NAS.

A IsardVDI es té una infraestructura amb sis hipervisors i dos NAS pacemaker agrupats de fabricació pròpia que comparteixen emmagatzematge amb NFSv4 i el rendiment és molt bo.


### Documents tècnics d'alta disponibilitat/alt rendiment

S'intenta mantenir tot el coneixement i l'experiència d'IsardVDI en clústers d'alt rendiment i disponibilitat utilitzant pacemaker, drbd, caixets de disc, migracions en viu, etc. a l'apartat de [Project Deploy](/deploy/) del manual.

En aquest apartat del manual hi ha vídeos sobre la migració de [emmagatzematge i escriptori virtual en viu](/deploy/#videos-conferences) que es van fer a la infraestructura d'IsardVDI: migrar l'escriptori virtual d'un hipervisor a un altre mentre **alhora** es migra l'emmagatzematge d'un emmagatzematge NAS a un altre sense que l'usuari de l'escriptori virtual sàpiga el que havia passat.


