# Hypervisores

## Tipos de hypervisores

### Hipervisor puro 

Cuando se edita o se crea un hipervisor, se puede verificar si será solo un hipervisor. En este modo, ese servidor solo será utilizado por IsardVDI para ejecutar escritorios virtuales. Este servidor no manejará operaciones de discos virtuales.


### Operaciones de disco puro

En este modo, el servidor solo se utilizará para manejar operaciones de disco de escritorios virtuales.


### Hipervisor mixto + operaciones de disco

Este modo utilizará el servidor tanto para ejecutar escritorios virtuales como para ejecutar operaciones de disco virtual.


## Hipervisores externos

El método recomendado para configurar un nuevo hipervisor es usar la imagen docker isard-generic-hyper, ya que mostrará todos los servicios necesarios en cualquier host de Linux con docker. Se requieren tres pasos:

1. Se instala el hipervisor KVM (se recomienda docker)
2. Se agrega claves ssh para el nuevo hipervisor KVM
3. Se crea un nuevo hipervisor en la interfaz de usuario web de IsardVDI.


### Instalar hipervisor KVM

#### Instalación de Docker (recomendado)

La configuración rápida y sencilla de un hipervisor externo se puede realizar utilizando el archivo de composición generado **./build.sh hypervisor** hypervisor.yml. En el Linux limpio donde se ha instalado el servidor que será el hipervisor KVM, se inicia el contenedor:

```
./build.sh hypervisor
docker-compose -f hypervisor.yml up -d
```

**Se tiene que configurar el archivo isardvdi.cfg antes de crear el archivo hypervisor.yml.**

Principalmente, se puede configurar allí el dominio de video letsencrypt (si no, usará un certificado autogenerado). Eso debería mostrar un hipervisor Isard completo en su servidor.

Si no se establece una configuración de letsencrypt en el archivo isardvdi.cfg, se debe configurar los mismos certificados de visores en todos los hipervisores. En este caso, se debe copiar los archivos de certificado /opt/isard/certs/viewers/* del host web a la misma carpeta en sus hipervisores remotos (eliminando los que ya están allí) y se vuelve a abrir la ventana acoplable del hipervisor.

Se ejecuta este comando para restablecer las claves ssh y la contraseña:

```bash
docker exec -e PASSWORD=<YOUR_ROOT_PASSWORD> isard-hypervisor bash -c '/reset-hyper.sh'
```

Después de eso, se puede continuar agregando claves ssh para este hipervisor en su contenedor IsardVDI isard-engine y se crea un nuevo hipervisor en la interfaz de usuario:


```bash
docker exec -e HYPERVISOR=<IP|DNS> -e PASSWORD=<YOUR_ROOT_PASSWD> -e PORT=2022 isard-engine bash -c '/add-hypervisor.sh'; history -d $((HISTCMD-1))
```

#### Manual de instalación

Si se desea utilizar otro host como hipervisor KVM, se tiene que asegurar de que tenga:

- **libvirtd**:
  - * **servicio funcionando**: ```systemctl status libvirtd```. Consulte con su distribución cómo instalar un hipervisor KVM completo:

      * https://wiki.debian.org/KVM#Installation
      * https://help.ubuntu.com/community/KVM/Installation
      * https://wiki.centos.org/HowTos/KVM
      * https://wiki.alpinelinux.org/wiki/KVM

    * **certificados habilitados**: los certificados reales en la ruta IsardVDI ```/opt/isard/certs/default/``` se deben copiar en la nueva ruta del servidor KVM ```/etc/pki/libvirt-spice``` . También se deben agregar algunas líneas a *libvirtd.conf* y *qemu.conf* para hacer uso de los certificados:

      * ```bash
        echo "listen_tls = 0" >> /etc/libvirt/libvirtd.conf; 
        echo 'listen_tcp = 1' >> /etc/libvirt/libvirtd.conf;
        echo 'spice_listen = "0.0.0.0"' >> /etc/libvirt/qemu.conf
        echo 'spice_tls = 1' >> /etc/libvirt/qemu.conf
        ```

    * **servidor websocket**: para permitir la conexión a los espectadores a través de los navegadores, también se debe configurar un servidor websocket. Se debería mirar el *start_proxy.py* incluido en los acopladores/hipervisores.

- **Servicio sshd en ejecución y accesible para el usuario**: ```systemctl status sshd```
- **curl**: Se utilizará para obtener actualizaciones. 
- **qemu-kvm**: Se debe crear un enlace al qemu-system-x86_64 como este ```ln -s /usr/bin/qemu-system-x86_64 /usr/bin/qemu-kvm```


### Agregar claves ssh para el nuevo hipervisor

Previamente se debe agregar claves ssh para acceder a ese hipervisor. Para hacer esto, se proporciona un script dentro del contenedor isard-app:

```bash
docker exec -e HYPERVISOR=<IP|DNS> -e PASSWORD=<YOUR_ROOT_PASSWD> isard-engine bash -c '/add-hypervisor.sh'
```

**Nota**: Hay parámetros opcionales que se pueden configurar:

- PORT: Default is *22*
- USER: Default is *root*

Después de hacer esto, se debe ir al menú de hipervisores y agregar un nuevo hipervisor con la misma IP/DNS. Engine intentará conectarse cuando habilite ese hipervisor.

**Ejemplo:**

Se tiene que tener en cuenta que se ha añadido ``; history -d $((HISTCMD-1))`` al comando para evitar que la contraseña se mantenga en nuestro historial.

```bash
root@server:~/isard# docker exec -e HYPERVISOR=192.168.0.114 -e PASSWORD=supersecret isard_isard-app_1 bash -c '/add-hypervisor.sh'; history -d $((HISTCMD-1))
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/community/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/edge/testing/x86_64/APKINDEX.tar.gz
(1/1) Installing sshpass (1.06-r0)
Executing busybox-1.28.4-r2.trigger
OK: 235 MiB in 127 packages
Trying to ssh into 192.168.0.114...
# 192.168.0.114:22 SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.1
# 192.168.0.114:22 SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.1
# 192.168.0.114:22 SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.1
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/root/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
expr: warning: '^ERROR: ': using '^' as the first character
of a basic regular expression is not portable; it is ignored

/usr/bin/ssh-copy-id: WARNING: All keys were skipped because they already exist on the remote system.
		(if you think this is a mistake, you may want to use -f option)

Hypervisor ssh access granted.
Access to 192.168.0.114 granted and found libvirtd service running.
Now you can create this hypervisor in IsardVDI web interface.
```

Si falla, se deshabilita el hipervisor, se editan los parámetros y se habilita nuevamente. El engine de IsardVDI verificará la conexión nuevamente. Se puede ver el mensaje de error en los detalles del hipervisor (se pulsa el icono "+" para ver los detalles).

Si el engine no intenta volver a conectarse después de habilitar el nuevo hipervisor, se tiene que reiniciar el contenedor de "isard-app":


```bash
docker restart isard_isard-app_1
```

**NOTA**: Probablemente se desee eliminar el hipervisor isard predeterminado después de crear un nuevo hipervisor externo. Para ello, se deshabilita "isard-hypervisor" y se elimina desde los detalles del hipervisor. Después se puede realizar la acción *Forzar eliminación* y se vuelve a reiniciar el contenedor de la aplicación isard.


### Agregar hipervisor en la interfaz de usuario web

Se puede agregar un hipervisor externo en la interfaz de usuario web como [usuario administrador](/admin/index.es/). Para ello se tiene que ir al panel de administración y en el apartado de "Hypervisors", se pulsa el botón "Add new"

![](./hypervisors.images/hyper2.png)

Y aparecerá una ventana de diálogo donde se pueden rellenar los campos. Se puede configurar cualquier cosa en el campo ID para identificar este hipervisor, pero se tiene que tener cuidado con otros campos de configuración, ya que deben coincidir con el hipervisor remoto:

![](./hypervisors.images/hyper1.png)

El nombre de host debe ser la IP/DNS accesible desde esta instalación de IsardVDI. Podría ser una red interna entre ambos hosts. El usuario será *root* y el puerto ssh **2022** ya que se deja el puerto ssh predeterminado 22 para el acceso ssh al host.

Se recomienda activar las opciones de "disk operations" y "hypervisor" y configurar el host web para este servidor IP/DNS de instalación web y el proxy de video para el IP/DNS accesible externamente para el hipervisor (acceso a sus visores a través de los puertos 80 y 443). 

Se establece el nombre de host del hipervisor del proxy de video a isard-hypervisor, ya que es el nombre del contenedor para los valores predeterminados del hipervisor.


## VLANS en hipervisor

Para conectar una interfaz troncal con definiciones de vlan dentro del hipervisor, se debe configurar (y descomentar) las variables en el fichero isardvdi.cfg:

**NOTA IMPORTANTE**: Recordar rehacer un build del docker-compose emitiendo el comando ```./build.sh``` nuevamente.

- **HYPERVISOR_HOST_TRUNK_INTERFACE:** Este es el nombre de la interfaz troncal de la red del host como se ve en el host. Por ejemplo: eth1

  Si no se configura HYPERVISOR_STATIC_VLANS, el hipervisor detectará automáticamente las vlans durante 260 segundos cada vez que inicie el contenedor isard-hypervisor. Por lo tanto, es mejor definir las VLANs estáticas que se sepan que están en el tronco.

- **HYPERVISOR_STATIC_VLANS**: Se tiene que establecer una lista de números de vlan separados por comas. Configurar esto evitará la detección automática de VLAN durante 260 segundos.

Esto agregará a la base de datos las VLANs encontradas como 'vXXX', donde XXX es el número de VLAN encontrado. Para que esto funcione, se debe verificar que se pueda acceder a STATS_RETHINKDB_HOST en el puerto 28015 desde el hipervisor.

Se comprueba que el que contiene el isard-db tenga el nombre de host correcto. Esto solo es necesario si se tiene el hipervisor en otra máquina:

- **STATS_RETHINKDB_HOST**: Se debe establecer en el host isard-db correcto si se tiene el hipervisor en otra máquina. No es necesario si se tiene un IsardVDI 'todo en uno'.

NOTA: La interfaz del host debe estar en modo promisorio: *ip link set dev ethX promic on*. Si se inicia el contenedor isard-hypervisor, se debe detener y volver a iniciar.


## Conceptos de infraestructura

Cuando se agrega un hipervisor externo, se debe tener en cuenta (y configurar según sea necesario) lo siguiente:

- **Ruta de los discos**: Por defecto almacenará los discos en /opt/isard. No es necesario crear esa carpeta, pero se recomienda montar el almacenamiento de E/S rápido (como el disco nvme) en esa ruta.

- **Rendimiento de la red**: Si se va a utilizar un NAS se debe tener en cuenta que se debe utilizar una red rápida. Velocidad de red recomendada superior a 10 Gbps.


### Rendimiento de los discos

Como se usan diferentes rutas para bases, plantillas y discos de grupos, se podría mejorar el rendimiento montando diferentes discos nvme de almacenamiento en cada ruta:

- **/opt/isard/bases**: Allí residirán las imágenes base que se creen. Por lo general, esas imágenes de disco se utilizarán como base para muchas plantillas y discos de usuario (grupos), por lo que el almacenamiento debe proporcionar un acceso de lectura lo más rápido posible.

- **/opt/isard/templates**: las plantillas también se utilizarán como discos principales para varios discos de usuario (grupos). Así que mejor usar un disco de acceso de lectura rápida.

- **/opt/isard/groups**: los escritorios en ejecución residirán aquí. Por lo tanto, aquí se realizarán muchas E/S simultáneas, por lo que el almacenamiento debe proporcionar la mayor cantidad de IOPS posible. Los discos NVME (o incluso las incursiones NVME) brindarán el mejor rendimiento.


### Rendimiento de la red

La red de almacenamiento entre los hipervisores y los servidores de almacenamiento NAS debe ser de al menos 10 Gbps. Todos los hipervisores deben montar el almacenamiento en la ruta **/isard** y ese montaje debe ser el mismo para todos los hipervisores.

Como se puede elegir al crear un hipervisor que sea un **hipervisor puro**, **operaciones de disco puro** o uno **mixto**, se puede agregar servidores de almacenamiento NAS como operaciones de disco puro. Eso traerá una manipulación más rápida de los discos, ya que IsardVDI utilizará el almacenamiento directo y los comandos en NAS.

En IsardVDI se tiene una infraestructura con seis hipervisores y dos NAS pacemaker agrupados de fabricación propia que comparten almacenamiento con NFSv4 y el rendimiento es muy bueno.


### Documentos técnicos de alta disponibilidad/alto rendimiento

Se intenta mantener todo el conocimiento y experiencia de IsardVDI en clústeres de alto rendimiento y disponibilidad utilizando pacemaker, drbd, cachés de disco, migraciones en vivo, etc. en el apartado de [Project Deploy](/deploy/) del manual.

En ese apartado del manual se encuentran vídeos sobre la migración de [almacenamiento y escritorio virtual en vivo](/deploy/#videos-conferences) que se hicieron en la infraestructua de IsardVDI: migrar el escritorio virtual de un hipervisor a otro mientras **al mismo tiempo** migramos su almacenamiento de un almacenamiento NAS a otro sin que el usuario del escritorio virtual supiera lo que había sucedido.
