# Introducción

Los usuarios de IsardVDI se clasifican en roles. En esta sección se describen las funciones que pueden utilizar los usuarios con funciones de administrador.

Los usuarios con **función de administrador** tienen las características de todos los [usuarios](../user/index.es.md), [usuarios avanzados](../advanced/index.es.md), [usuarios administradores](../manager/index.es.md) y los descritos en esta sección.