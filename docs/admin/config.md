# Config

To go to the "Config" section, press the button ![](./users.images/users1.png)

![](./users.images/users2.png)

And press the button ![](./config.images/config1.png)

![](./config.images/config2.png)


## Job Scheduler

En este apartado se puede programar un "trabajo", puede ser una copia de seguridad o 

* Type: Cron (que haga el trabajo a una hora determinada), interval (que haga el trabajo cada x tiempo)

* Hour: indica la hora

* Minute: indica el minuto

* Action: backup database, check ephimeral domain status


## Backup database

In this section you can make backup copies of the system database.

![](./config.images/config9.png)

* You can upload a backup that has already been made. To do this, press the button ![](./config.images/config10.png) and drag the file to the dialog box:

![](./config.images/config11.png)

* To create a new backup, press the button ![](./config.images/config12.png)

![](./config.images/config13.png)

* To delete a backup, press the button ![](./config.images/config14.png)

![](./config.images/config18.png)

* To restore a backup, click the button. ![](./config.images/config15.png)

![](./config.images/config19.png)

* To see the backup information, press the button ![](./config.images/config16.png)

![](./config.images/config20.png)

* To download the backup file, press the button ![](./config.images/config17.png)

![](./config.images/config21.png)


## Maintenance mode

Maintenance mode disables web interfaces for users with manager, advanced, and user roles.

To enable and disable this function, check the box

![](./config.images/config6.png)

Once the box is checked, when users try to log in to their account, they will see a maintenance message

![](./config.images/config7.png)

![](./config.images/config8-es.png)
