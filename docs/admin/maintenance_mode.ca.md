# Mode de manteniment

El mode de manteniment desactiva les interfícies web per als usuaris amb rols de managers, avançats i d'usuari.

El mode de manteniment es pot habilitar i deshabilitar a través de la secció de [configuració](./config.ca.md) del panell d'administració per part d'un usuari amb funció d'administrador.

L'administrador del sistema pot crear un arxiu anomenat `/opt/isard/config/maintenance` per a canviar a el mode de manteniment a l'inici. L'arxiu s'elimina una vegada que el sistema canvia a el mode de manteniment.