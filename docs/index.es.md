# Introducción

Bienvenido a la documentación de [IsardVDI](https://isardvdi.com).

Aquí podéis encontrar información útil para una gran variedad de usuarios. Desde usuarios de escritorio virtual a administradores de sistema.

Como usuario, puedes empezar a la [sección de usuario](user/index.es.md).

Si estáis interesado a instalarlo, vais a la [sección de instalación](install).

Si no encontráis el que estáis buscando, contactáis con nosotros a .. <mailto:info@isardvdi.com>

## Qué es el sistema IsardVDI?

IsardVDI es un sistema de despliegue de Infraestructura de Escritorios Virtuales (En inglés Virutal Desktop Infrastructure (VDI) de código abierto basado en virtualización de linux KVM y dockers. Se puede instalar de forma independiente en unos minutos pero también puede crecer a medida que necesitáis con múltiples hipervisors.

## Funcionalidades

Estas son las principales características:

- Interfaz básica para gestionar los escritorios.
- Registro automático de OAUTH2 e inicio de sesión con Google y Github.
- Configuración multitenant por categorías y nuevo rol de gestor multitenant.
- Se pueden establecer límites por grupo y categoría:
- Número máximo de escritorios creados, escritorios concurrentes, plantillas, vCPUS, memoria, ISOS cargados, ...
- Escritorios efímeros con un uso limitado del tiempo.
- Creación de escritorio automático persistente al iniciar la sesión del usuario.
- Generación automática y renovación de certificados Letsencrypt.
- Solo se usan los puertos 80 y 443 para el acceso web y el acceso a los visores.
- Estadísticas de sistema y uso de usuarios con grafana.

## Downloads

Podéis bajar inmediatamente las imágenes preinstaladas y optimizadas del sistema operativo desde el menú **Downloads**.

- Iniciáis **escritorios de demostración** y conectaos utilizando vuestro navegador y protocolo spice o vnc. No hay que instalar nada y será asegurado con certificados.
- Instalad virt-*viewer y conectáis utilizando el cliente de spice. El conector transparente de **sonido y USB** estará disponible.

Creáis vuestro propio escritorio utilizando ISOs bajados desde actualizaciones o podéis subir vuestra opción desde el menú **Media**. Cuando acabáis de instalar el sistema operativo y las aplicaciones creáis una **plantilla** y decidís qué usuarios o categorías pueden crear un escritorio idéntico en esta plantilla. Gracias a la **creación incremental de disco** todo esto se puede hacer en minutos.

<iframe src="https://drive.google.com/file/d/1tPL12yw3MEV5IEPL5by7z76zVVSNnAng/preview" width="640" height="480"></iframe>

No estáis ligados a una instalación «autónoma» en un servidor. Podéis añadir más hipervisors al **pool** y dejar que IsardVDI decida donde iniciar cada escritorio. Cada hipervisor solo necesita el docker-compongo de hipervisor IsardVDI. Tened en cuenta que tendríais que mantener el almacenamiento compartido entre estos hipervisors.

## Casos de uso (conocidos)

### Educativo

Actualmente gestionamos una *gran infraestructura de IsardVDI** en la Escuela del Trabajo de Barcelona. 3000 estudiantes y profesores tienen IsardVDI disponible seis hipervisors y un cluster NARIZ dual hecha a medida, que van desde placas base dual core de servidor intel de alta gama a gigabyte de gaming.

Tenemos experiencia en diferentes clientes delgados que utilizamos para reducir los costes de renovación y consumo en las aulas.

## Administración Pública

La administración pública local en Cataluña tiene una instalación de tres hipervisors reformados que hicimos en tiempo récord haciendo también una integración sin fisuras en su autenticación SASL2. Hicimos algunas pruebas de rendimiento con IsardVDI en esta infraestructura consiguiendo un resultado impresionante de +2000 escritorios VDI concurrentes.

## Por qué elegir IsardVDI?

- Licencia AGPLv3 de código abierto.
- Grandes implementaciones de pruebas de concepto.
- Optimizaciones de infraestructuras y reducciones de costes.
- Sin licencias.
- BYOD: HTML5 o visores SPICE nativos.
- Instalaciones para múltiples clientes.
- OAUTHv2, SASL2, autenticación a base de datos con código de registro automático.
- Sistema de control con APIO RISTRA.
- Creación inmediata de escritorios y plantillas.
- Escritorios persistentes y no persistentes.
- Dockeritzat. Es fácil instalar y mantener en cualquier distribución de Linux.
- Aplicáis límites para los clientes y cuotas por categorías, grupos y/o usuarios.
- ...

## Autores de IsardVDI

- Josep Maria Viñolas Auquer
- Alberto Larraz Dalmases
- Néfix Estrada Campañà

## Apoyo / Contacto

Por favor enviadnos un correo electrónico a si <mailto:info@isardvdi.com> tenéis ninguno pregunta o [creáis un ticket](https://gitlab.com/isard/isardvdi/-/issues/new).